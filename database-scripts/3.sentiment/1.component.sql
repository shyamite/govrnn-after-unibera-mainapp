/*
-- Query: select *  from component
LIMIT 0, 1000

-- Date: 2016-07-27 14:07
*/
INSERT INTO `component` (`id`,`componentType`) VALUES (21,'Sentiment');
INSERT INTO `component` (`id`,`componentType`) VALUES (22,'Sentiment');
INSERT INTO `component` (`id`,`componentType`) VALUES (23,'Opinion');
INSERT INTO `component` (`id`,`componentType`) VALUES (24,'Response');
INSERT INTO `component` (`id`,`componentType`) VALUES (5,'Link');
INSERT INTO `component` (`id`,`componentType`) VALUES (6,'GPI');
INSERT INTO `component` (`id`,`componentType`) VALUES (7,'GA');
INSERT INTO `component` (`id`,`componentType`) VALUES (8,'LocalVerdict');
INSERT INTO `component` (`id`,`componentType`) VALUES (9,'GlobalVerdict');
INSERT INTO `component` (`id`,`componentType`) VALUES (10,'Sentiment');
INSERT INTO `component` (`id`,`componentType`) VALUES (11,'Opinion');
INSERT INTO `component` (`id`,`componentType`) VALUES (12,'Response');
INSERT INTO `component` (`id`,`componentType`) VALUES (13,'Link');
INSERT INTO `component` (`id`,`componentType`) VALUES (14,'GPI');
INSERT INTO `component` (`id`,`componentType`) VALUES (15,'GA');
INSERT INTO `component` (`id`,`componentType`) VALUES (16,'GlobalVerdict');
INSERT INTO `component` (`id`,`componentType`) VALUES (17,'LocalVerdict');
INSERT INTO `component` (`id`,`componentType`) VALUES (18,'Sentiment');
INSERT INTO `component` (`id`,`componentType`) VALUES (19,'GA');
INSERT INTO `component` (`id`,`componentType`) VALUES (20,'GPI');
