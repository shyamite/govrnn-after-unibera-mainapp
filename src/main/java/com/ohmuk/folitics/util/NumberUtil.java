/**
 * 
 */
package com.ohmuk.folitics.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Kalpana
 *
 */
public class NumberUtil {
	
	protected static final Logger logger = LoggerFactory.getLogger(NumberUtil.class);

	public static final double formatNumberDecimal(double value, int places){
		 if (places < 0) {
			 logger.info("NumberUtil - Decimal places cannot be less than 0");
			 throw new IllegalArgumentException();
		 }
	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	private static final Random RANDOM = new Random();

	public static final int getRandomNumber(int max){
		return RANDOM.nextInt(max);
	}
}
