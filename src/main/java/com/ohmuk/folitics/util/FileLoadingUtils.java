package com.ohmuk.folitics.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.catalina.core.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.ohmuk.folitics.cache.SingletonCache;
import com.ohmuk.folitics.hibernate.entity.Sentiment;

public class FileLoadingUtils {

	private static final String STANARD_OPINION_IMAGE_FOLDER= "Standard_800_350";
	private static final String THUMB_OPINION_IMAGE_FOLDER= "Thumb_170_100";
    
    public static final String DailyNewsDirectory = "." + File.separator + "testdata" + File.separator + "dailyNews";

    public static final String SentimentImagesDiectory = File.separator + "static" + File.separator + "images"
            + File.separator + "sentiments";
    public static final String BASE_IMAGE_PATH = "."+File.separator +"src"+File.separator +"main"+File.separator +"resources"+File.separator +"static";
	public static final String BASE_IMAGE_PATH_SENTIMENT_LIST = File.separator + "images"
            + File.separator + "sentiments"+File.separator +"SentimentListImages";

    public static final String IMAGE_PATH = File.separator +"src"+File.separator +"main"+File.separator +"resources"+File.separator +"static"+File.separator +"images"+File.separator +"sentiments"+File.separator;

    protected static final Logger LOGGER = LoggerFactory.getLogger(FileLoadingUtils.class);

    @Autowired
    private ApplicationContext context;

    public static final List<File> loadDailyNews() {
        LOGGER.info("Enter FileLoadingUtils loadDailyNews method");
        try {
            File directory = new File(DailyNewsDirectory);
            // get all the files from a directory
            File[] fList = directory.listFiles();
            LOGGER.info("Exit FileLoadingUtils loadDailyNews method");
            if (fList != null && fList.length != 0) {

                return Arrays.asList(fList);
            } else {
                return null;
            }

        } catch (Exception ex) {
            LOGGER.error("Excetpion in reading daily news" + ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }

    public static final Map<String, List<String>> loadSentimentImageUrl() {
        LOGGER.info("Enter FileLoadingUtils loadSentimentImageUrl method");
        try {
            String separator = "/";//File.separator;
            String relativeFolderPath = separator + "images" + separator + "sentiments";
            String relativeFolderPathStatic = separator + "static" + relativeFolderPath;
            LOGGER.info("relativeFolderPathStatic : " + relativeFolderPathStatic);
            String folder = (FileLoadingUtils.class).getResource(relativeFolderPathStatic).getFile();
            folder = StringUtils.replace(folder, "%20", " ");
            LOGGER.info("Folder:" + folder);
            File directory = new File(folder);
            // get all the directories under /static/images/sentiments
            File[] dList = directory.listFiles(File::isDirectory);
            Map<String, List<String>> sentimentImagesMap = new HashMap<String, List<String>>();
            if (dList != null && dList.length > 0) {
                for (File dir : dList) {
                    if (dir.isDirectory()) {
                        File[] subFoldesList = dir.listFiles(File::isDirectory);
                        if (dList != null && dList.length > 0) {
                            for (File subDir : subFoldesList) {
                                if (subDir.isDirectory() && subDir.getName().equals(THUMB_OPINION_IMAGE_FOLDER)) {
                                	File[] imageFiles = subDir.listFiles();
                                    if (imageFiles != null && imageFiles.length > 0) {
                                        List<String> imageUrls = new ArrayList<String>();
                                        for (File image : imageFiles) {
                                        	String imagePath =relativeFolderPath + separator
                                                    + dir.getName()+ separator+ subDir.getName() + separator + image.getName();
                                            imageUrls.add(imagePath);
                                        }
                                        LOGGER.info("Sentiment Name :" + dir.getName());
                                        LOGGER.info("Sentiment Images Urls:" + imageUrls);
                                        sentimentImagesMap.put(dir.getName(), imageUrls);
                                    }        
                                }
                            }             
                        }
                    }
                }
            }
            LOGGER.info("Enter FileLoadingUtils loadSentimentImageUrl method");
            return sentimentImagesMap;

        } catch (Exception ex) {
        	
            LOGGER.error("Excetpion in loading images" + ex.getMessage());
            LOGGER.error(ex.getStackTrace().toString());
            return null;
        }
    }

    public static final String returnStandardImagePath(String thumbImagePath){
    	String returnStandardPath = thumbImagePath;
    	String thumb_suffix =  "_thumb";
    	if(!StringUtils.isEmpty(thumbImagePath) && thumbImagePath.contains(THUMB_OPINION_IMAGE_FOLDER)){
    		returnStandardPath = thumbImagePath.replace(THUMB_OPINION_IMAGE_FOLDER, STANARD_OPINION_IMAGE_FOLDER);
    		if(returnStandardPath.contains(thumb_suffix))
    			returnStandardPath = returnStandardPath.replace(thumb_suffix, "");
    	}
    	return returnStandardPath;
    }
    public static final Map<String, List<String>> addSentimentImageUrlsToCache() {
        SingletonCache singletonCache = SingletonCache.getInstance();
        Map<String, List<String>> sentimentIamgeUrls = FileLoadingUtils.loadSentimentImageUrl();
        if (sentimentIamgeUrls != null && !sentimentIamgeUrls.isEmpty()) {
            for (Entry<String, List<String>> entry : sentimentIamgeUrls.entrySet()) {
                singletonCache.put(entry.getKey(), entry.getValue());
            }
        }
        return sentimentIamgeUrls;
    }

    public static List<String> searchSentimentImage(String searchkeyword, Sentiment sentiment) throws Exception {
        SingletonCache singletonCache = SingletonCache.getInstance();
        List<String> sentimentIamgeUrls = FileLoadingUtils.searchSentimentImageUrl(searchkeyword, sentiment);
        if (sentimentIamgeUrls != null && !sentimentIamgeUrls.isEmpty()) {
            /*
             * for (Entry<String, List<String>> entry :
             * sentimentIamgeUrls.entrySet()) {
             * 
             * System.out.println("value ::"+entry.getValue());
             * singletonCache.put(entry.getKey(), entry.getValue()); }
             */
        }
        return sentimentIamgeUrls;
    }

    private static List<String> searchSentimentImageUrl(String searchkeyword, Sentiment sentiment) throws Exception {

        List<String> sentimentImagesList = new ArrayList<String>();
        if (sentiment != null) {
            try {
                String separator = "/";
                String relativeFolderPath = separator + "images" + separator + "sentiments" + separator
                        + sentiment.getSubject();
                String relativeFolderPathStatic = separator + "static" + relativeFolderPath;
                System.out.print("relativeFolderPathStatic : " + relativeFolderPathStatic);

                String imagePath = System.getProperty("user.dir") + "/src/main/resources/" + relativeFolderPathStatic;

                String folder = imagePath;
                folder = StringUtils.replace(folder, "%20", " ");
                System.out.println("Folder:" + folder);
                File directory = new File(folder);
                // get all the directories under /static/images/sentiments/<
                // sentiment folder>
                File[] dList = directory.listFiles();

                if (dList != null && dList.length > 0) {
                    List<String> imageUrls = new ArrayList<String>();
                    for (File image : dList) {

                        if (org.apache.commons.lang.StringUtils.containsAny(image.getName().toLowerCase(),
                                searchkeyword.toLowerCase())) {
                            System.out.println("image.getanme ::" + image.getName());
                            /*
                             * if (!image.getName().matches("\\.")) {
                             * 
                             * } else { imageUrls.add(relativeFolderPath +
                             * separator + image.getName()); }
                             */

                            if (image.getName().contains(".")) {
                                imageUrls.add(relativeFolderPath + separator + image.getName());
                            }
                        }
                    }
                    sentimentImagesList.addAll(imageUrls);

                }
                LOGGER.info("Enter FileLoadingUtils loadSentimentImageUrl method");
                return sentimentImagesList;

            } catch (Exception ex) {
                LOGGER.error("Excetpion in reading daily news" + ex.getMessage());
                ex.printStackTrace();
                return sentimentImagesList;
            }
        }
        return sentimentImagesList;
    }
}
