package com.ohmuk.folitics.enums;

public enum NewsFeedType {
	SENTIMENT("Sentiment"), BREAKING_NEWS("BreakingNews"), BOTH("Both");
	private String value;

	private NewsFeedType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public static final NewsFeedType getNewsFeedType(String value) {
		if (SENTIMENT.getValue().equals(value)) {
			return SENTIMENT;
		}
		if (BREAKING_NEWS.getValue().equals(value)) {
			return BREAKING_NEWS;
		}
		if (BOTH.getValue().equals(value)) {
			return BOTH;
		}
		return null;
	}
}
