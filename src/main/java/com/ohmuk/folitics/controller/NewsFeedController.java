package com.ohmuk.folitics.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ohmuk.folitics.dto.RSSChannelLink;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.dto.SentimentNewsFeed;
import com.ohmuk.folitics.enums.NewsFeedType;
import com.ohmuk.folitics.hibernate.entity.NewsFeed;
import com.ohmuk.folitics.service.interfaces.NewsFeedService;
import com.ohmuk.folitics.xml.dto.RSS;

@Controller

@RequestMapping("/admin/news")
public class NewsFeedController {
	private static Logger logger = LoggerFactory.getLogger(NewsFeedController.class);

	@Autowired
	private NewsFeedService newsFeedService;

	/*
	 * This method is loading rss feeds from single channel
	 */
	@RequestMapping(value = "/rss", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<RSS> getNews(@RequestParam(value = "rssLink", required = true) String rssLink) {

		logger.info("Inside getNews method");
		RSS rss = null;
		try {
			rss = newsFeedService.getNewsByRSSLink(rssLink);
		} catch (Exception exception) {
			logger.error("Exception: " + exception);
			return new ResponseDto<RSS>(false);
		}
		if (rss != null) {
			logger.debug("getNews Success");
			return new ResponseDto<RSS>(true, rss);
		}
		logger.debug("getNews Un-Successfully");
		return new ResponseDto<RSS>(false);
	}
	
	/*
	 * Automatic mode method will load feeds from all channels and load data, search for keywords
	 * and enter data to related sentiments and breaking news
	 */
	@RequestMapping(value = "/rssAuto", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<RSS> autoModeFeed() {

		logger.info("Inside getNews method");
		RSS rss = null;
		try {
			// loop though all links; linke ndtv, zeetv
			//for()
			//rss = newsFeedService.getNewsByRSSLink(rssLink);
		} catch (Exception exception) {
			logger.error("Exception: " + exception);
			return new ResponseDto<RSS>(false);
		}
		if (rss != null) {
			logger.debug("getNews Success");
			return new ResponseDto<RSS>(true, rss);
		}
		logger.debug("getNews Un-Successfully");
		return new ResponseDto<RSS>(false);
	}

	@RequestMapping(value = "/preLoadNewsData", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<List<RSSChannelLink>> loadChannelLinks() {
		logger.info("Inside loadChannelLinks method");
		List<RSSChannelLink> channelList = null;
		try {
			channelList = newsFeedService.loadChannelLinks();
		} catch (Exception e) {
			logger.error("Exception: " + e);
		}

		if (channelList != null) {
			logger.debug("loadChannelLinks list Success");
			return new ResponseDto<List<RSSChannelLink>>(true, channelList);
		}
		return new ResponseDto<List<RSSChannelLink>>(false, channelList);
	}
	
	@RequestMapping(value = "/getAllNewsSource", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<List<NewsFeed>> getAllNewsSource() {
		logger.info("Inside loadChannelLinks method");
		List<NewsFeed> channelList = null;
		try {
			channelList = newsFeedService.getAllNewsSource();
		} catch (Exception e) {
			logger.error("Exception: " + e);
		}

		if (channelList != null) {
			logger.debug("loadChannelLinks list Success");
			return new ResponseDto<List<NewsFeed>>(true, channelList);
		}
		return new ResponseDto<List<NewsFeed>>(false, channelList);
	}

	@RequestMapping(value = "/addSentimentNews", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Boolean> addSentimentNews(@RequestBody SentimentNewsFeed sentimentNewsFeed) {
		logger.info("Inside addNewsFeed method");
		Boolean successMessage = null;
		try {
			logger.info("Inside addSentimentNews" + sentimentNewsFeed);
			// newsFeedService.addBreakingNews(sentimentNewsFeed.getBreakingNews());
			successMessage = newsFeedService.addSentimentNews(sentimentNewsFeed.getSentimentNews());
		} catch (Exception e) {
			logger.error("Exception: " + e);
		}

		if (successMessage != null && successMessage == true) {
			logger.info("addSentimentNews Success");
			return new ResponseDto<Boolean>(true, successMessage);
		}
		return new ResponseDto<Boolean>(false, successMessage);
	}
	
	@RequestMapping(value = "/addNewsSource", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Boolean> addNewsSource(@RequestBody NewsFeed newsfeed) {
		logger.info("Inside addNewsFeed method");
		Boolean successMessage = null;
		try {
			logger.info("Inside addNewsfeed" + newsfeed);
			// newsFeedService.addBreakingNews(sentimentNewsFeed.getBreakingNews());
			newsFeedService.addNewsFeed(newsfeed);
			successMessage = true;
		} catch (Exception e) {
			logger.error("Exception: " + e);
			successMessage = false;
		}

		if (successMessage != null && successMessage == true) {
			logger.info("addSentimentNews Success");
			return new ResponseDto<Boolean>(true, successMessage);
		}
		return new ResponseDto<Boolean>(false, successMessage);
	}
	
	@RequestMapping(value = "/updateNewsSource", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Boolean> updateNewsSource(@RequestBody NewsFeed newsfeed) {
		logger.info("Inside updateNewsFeed method");
		Boolean successMessage = null;
		try {
			logger.info("Inside updateNewsfeed" + newsfeed);
			// newsFeedService.addBreakingNews(sentimentNewsFeed.getBreakingNews());
			newsFeedService.updateNewsFeed(newsfeed);
			successMessage = true;
		} catch (Exception e) {
			logger.error("Exception: " + e);
			successMessage = false;
		}

		if (successMessage != null && successMessage == true) {
			logger.info("addSentimentNews Success");
			return new ResponseDto<Boolean>(true, successMessage);
		}
		return new ResponseDto<Boolean>(false, successMessage);
	}
	
	@RequestMapping(value = "/deleteNewsSource", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Boolean> deleteNewsSource(@RequestBody NewsFeed newsfeed) {
		logger.info("Inside updateNewsFeed method");
		Boolean successMessage = null;
		try {
			logger.info("Inside updateNewsfeed" + newsfeed);
			// newsFeedService.addBreakingNews(sentimentNewsFeed.getBreakingNews());
			newsFeedService.deleteNewsFeed(newsfeed);
			successMessage = true;
		} catch (Exception e) {
			logger.error("Exception: " + e);
			successMessage = false;
		}

		if (successMessage != null && successMessage == true) {
			logger.info("addSentimentNews Success");
			return new ResponseDto<Boolean>(true, successMessage);
		}
		return new ResponseDto<Boolean>(false, successMessage);
	}
	
	@RequestMapping(value = "/addSentimentNewsAndBreakingNews", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Boolean> addSentimentNewsAndBreaking(
			@RequestBody SentimentNewsFeed sentimentNewsFeed
			) {
		logger.info("Inside addNewsFeed, addBreakingNews method");
		Boolean successMessage = null;
		Boolean smsg = null;
		try {
			logger.info("Inside addSentimentNews and addBreakingNews" + sentimentNewsFeed);
			if(sentimentNewsFeed.getBreakingNews().size() > 0) {
				smsg = newsFeedService.addBreakingNews(sentimentNewsFeed.getBreakingNews());
			}
			if(sentimentNewsFeed.getSentimentNews().size() > 0) {
				successMessage = newsFeedService.addSentimentNews(sentimentNewsFeed.getSentimentNews());
			}
		} catch (Exception e) {
			logger.error("Exception: " + e);
		}

		if ((successMessage != null && successMessage == true) || (smsg !=null && smsg == true)) {
			logger.info("addSentimentNews, addBreakingNews Success");
			return new ResponseDto<Boolean>(true, successMessage);
		}
		return new ResponseDto<Boolean>(false, successMessage);
	}
	
	@RequestMapping(value = "/addBreakingNews", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Boolean> addBreakingNews(
			@RequestBody SentimentNewsFeed sentimentNewsFeed
			) {
		logger.info("Inside addBreakingNews method");
		Boolean successMessage = null;
		Boolean smsg = null;
		try {
			logger.info("Inside addBreakingNews" + sentimentNewsFeed);
			smsg = newsFeedService.addBreakingNews(sentimentNewsFeed.getBreakingNews());
		} catch (Exception e) {
			logger.error("Exception: " + e);
		}

		if (smsg !=null && smsg == true) {
			logger.info("addBreakingNews Success");
			return new ResponseDto<Boolean>(true, successMessage);
		}
		return new ResponseDto<Boolean>(false, successMessage);
	}

	@RequestMapping(value = "/updatedSentimentNews", method = RequestMethod.PUT)
	public @ResponseBody ResponseDto<Boolean> updatedSentimentNews(@RequestBody SentimentNewsFeed sentimentNewsFeed) {
		logger.info("Inside updatedSentimentNews method");
		Boolean successMessage = null;
		try {
			logger.info("Inside updatedSentimentNews" + sentimentNewsFeed);
			successMessage = newsFeedService.updatedSentimentNews(sentimentNewsFeed.getSentimentNews());
		} catch (Exception e) {
			logger.error("Exception: " + e);
		}

		if (successMessage != null && successMessage == true) {
			logger.info("updatedSentimentNews Success");
			return new ResponseDto<Boolean>(true, successMessage);
		}
		return new ResponseDto<Boolean>(false, successMessage);
	}
}
