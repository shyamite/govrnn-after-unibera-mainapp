package com.ohmuk.folitics.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ohmuk.folitics.component.newsfeed.RSSFeedAggegrator;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.hibernate.entity.newsfeed.FeedChannel;
import com.ohmuk.folitics.hibernate.entity.newsfeed.FeedSource;
import com.ohmuk.folitics.mongodb.entity.NewsFeed;
import com.ohmuk.folitics.service.newsfeed.IFeedChannelService;
import com.ohmuk.folitics.service.newsfeed.IFeedSourceService;

/**
 * @author Jahid
 *
 */
@Controller
@RequestMapping("/rssfeed")
public class RSSFeedController {

	final static Logger logger = LoggerFactory
			.getLogger(RSSFeedController.class);

	@Autowired
	private volatile RSSFeedAggegrator rssFeedAggegrator;
	
	@Autowired 
	private volatile IFeedSourceService feedSourceService;
	
	@Autowired
	private volatile IFeedChannelService feedChannelService;
	
	@RequestMapping(value = "/addFeedSource", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<FeedSource> addFeedSource(@RequestBody FeedSource feedSource) {
		FeedSource fd;
		try {
			fd = feedSourceService.create(feedSource);
			if (fd != null)
				return new ResponseDto<FeedSource>(true, fd);
		} catch (Exception e) {
			logger.error("Something went wrong while adding sources " + e);
			e.printStackTrace();
		}
		return new ResponseDto<FeedSource>(false);
	}
	
	@RequestMapping(value = "/updateFeedSource", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<FeedSource> updateFeedSource(@RequestBody FeedSource feedSource) {
		FeedSource fd;
		try {
			fd = feedSourceService.update(feedSource);
			if (fd != null)
				return new ResponseDto<FeedSource>(true, fd);
		} catch (Exception e) {
			logger.error("Something went wrong while adding sources " + e);
			e.printStackTrace();
		}
		return new ResponseDto<FeedSource>(false);
	}
	
	@RequestMapping(value = "/getFeedSource", method = RequestMethod.GET, consumes = "application/json")
	public @ResponseBody ResponseDto<FeedSource> getFeedSource(@RequestParam long id) {
		FeedSource fd;
		try {
			fd = feedSourceService.read(id);
			if (fd != null)
				return new ResponseDto<FeedSource>(true, fd);
		} catch (Exception e) {
			logger.error("Something went wrong while adding sources " + e);
			e.printStackTrace();
		}
		return new ResponseDto<FeedSource>(false);
	}
	
	@RequestMapping(value = "/deleteSingleFeedSource", method = RequestMethod.GET, consumes = "application/json")
	public @ResponseBody ResponseDto<FeedSource> deleteFeedSource(@RequestParam long id) {
		FeedSource fd;
		try {
			fd = feedSourceService.delete(id);
			if (fd != null)
				return new ResponseDto<FeedSource>(true, fd);
		} catch (Exception e) {
			logger.error("Something went wrong while adding sources " + e);
			e.printStackTrace();
		}
		return new ResponseDto<FeedSource>(false);
	}
	
	@RequestMapping(value = "/disableFeedsource", method = RequestMethod.GET, consumes = "application/json")
	public @ResponseBody ResponseDto<String> disableFeedSource(Long id, boolean enable) {
		try {
			if (feedSourceService.disableFeedSource(id, enable))
				return new ResponseDto<String>(true,
						"Feed source disbale with id: " + id);
		} catch (Exception e) {
			logger.error("Something went wrong while adding sources " + e);
			e.printStackTrace();
		}
		return new ResponseDto<String>(false,
				"Feed source could not be disabled : " + id);
	}
	
	@RequestMapping(value = "/getAllFeedSources", method = RequestMethod.GET, consumes = "application/json")
	public @ResponseBody ResponseDto<List<FeedSource>> getAllFeedSources() {
		List<FeedSource> fds;
		try {
			fds = feedSourceService.readAll();
			if (fds != null) {
				return new ResponseDto<List<FeedSource>>(true, fds);
			}

		} catch (Exception e) {
			logger.error("Something went wrong while adding sources " + e);
			e.printStackTrace();
		}
		return new ResponseDto<List<FeedSource>>(false);
	}

	@RequestMapping(value = "/addFeedChannel", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<FeedChannel> addFeedChannel(@RequestBody FeedChannel feedChannel) {
		FeedChannel fd;
		try {
			fd = feedChannelService.create(feedChannel);
			if (fd != null)
				return new ResponseDto<FeedChannel>(true, fd);
		} catch (Exception e) {
			logger.error("Something went wrong while adding sources " + e);
			e.printStackTrace();
		}
		return new ResponseDto<FeedChannel>(false);
	}
	
	@RequestMapping(value = "/updateFeedChannel", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<FeedChannel> updateFeedChannel(@RequestBody FeedChannel feedChannel) {
		FeedChannel fd;
		try {
			fd = feedChannelService.update(feedChannel);
			if (fd != null)
				return new ResponseDto<FeedChannel>(true, fd);
		} catch (Exception e) {
			logger.error("Something went wrong while adding sources " + e);
			e.printStackTrace();
		}
		return new ResponseDto<FeedChannel>(false);
	}
	
	@RequestMapping(value = "/getFeedChannel", method = RequestMethod.GET, consumes = "application/json")
	public @ResponseBody ResponseDto<FeedChannel> getFeedChannel(@RequestParam long id) {
		FeedChannel fd;
		try {
			fd = feedChannelService.read(id);
			if (fd != null)
				return new ResponseDto<FeedChannel>(true, fd);
		} catch (Exception e) {
			logger.error("Something went wrong while adding sources " + e);
			e.printStackTrace();
		}
		return new ResponseDto<FeedChannel>(false);
	}
	
	@RequestMapping(value = "/deleteSingleFeedChannel", method = RequestMethod.GET, consumes = "application/json")
	public @ResponseBody ResponseDto<FeedChannel> deleteFeedChannel(@RequestParam long id) {
		FeedChannel fd;
		try {
			fd = feedChannelService.delete(id);
			if (fd != null)
				return new ResponseDto<FeedChannel>(true, fd);
		} catch (Exception e) {
			logger.error("Something went wrong while adding sources " + e);
			e.printStackTrace();
		}
		return new ResponseDto<FeedChannel>(false);
	}
		
	@RequestMapping(value = "/getAllFeedChannels", method = RequestMethod.GET, consumes = "application/json")
	public @ResponseBody ResponseDto<List<FeedChannel>> getAllFeedChannels() {
		List<FeedChannel> fds;
		try {
			fds = feedChannelService.readAll();
			if (fds != null) {
				return new ResponseDto<List<FeedChannel>>(true, fds);
			}

		} catch (Exception e) {
			logger.error("Something went wrong while adding sources " + e);
			e.printStackTrace();
		}
		return new ResponseDto<List<FeedChannel>>(false);
	}

	@RequestMapping(value = "/deleteFeedSource", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<List<FeedSource>> deleteFeedSource(
			String feedSourceName) {
		List<FeedSource> fds;
		try {
			fds = feedSourceService.readByName(feedSourceName, false);
			if (fds != null) {
				for (FeedSource fd : fds) {
					feedSourceService.delete(fd.getId());
				}
				return new ResponseDto<List<FeedSource>>(true, fds);
			}

		} catch (Exception e) {
			logger.error("Something went wrong while adding sources " + e);
			e.printStackTrace();
		}
		return new ResponseDto<List<FeedSource>>(false);
	}
	
	@RequestMapping(value = "/addsource", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<FeedSource> addsource(String sourceName,
			String sourceURL) {
		FeedSource fd;
		try {
			fd = rssFeedAggegrator.addFeedSource(sourceName, sourceURL);
			if (fd != null)
				return new ResponseDto<FeedSource>(true, fd);
		} catch (Exception e) {
			logger.error("Something went wrong while adding sources " + e);
			e.printStackTrace();
		}
		return new ResponseDto<FeedSource>(false);
	}

	
	@RequestMapping(value = "/disablesource", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<String> disableSource(Long id) {
		try {
			if (rssFeedAggegrator.disableFeedSource(id))
				return new ResponseDto<String>(true,
						"Feed source disbale with id: " + id);
		} catch (Exception e) {
			logger.error("Something went wrong while adding sources " + e);
			e.printStackTrace();
		}
		return new ResponseDto<String>(false,
				"Feed source could not be disabled : " + id);
	}

	@RequestMapping(value = "/loadfeed", method = RequestMethod.GET, consumes = "application/json")
	public @ResponseBody ResponseDto<String> loadFeed(String sourceName) {
		List<String> messages = new ArrayList<String>();
		if (rssFeedAggegrator.aggregateFeed(sourceName, messages)) {
			return new ResponseDto<String>(true, messages);
		}
		return new ResponseDto<String>(false, messages);
	}

	@RequestMapping(value = "/clearfeed", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<String> clearfeed(Long id) {
		try {
			if (rssFeedAggegrator.clearFeed(id)) {
				return new ResponseDto<String>(true,
						"All feed has been cleared for feed source id : " + id);
			}
		} catch (EmptyResultDataAccessException ed) {
			return new ResponseDto<String>(false,
					"Error while clearing the feed for feed source id: " + id);
		} catch (Exception e) {
			logger.error("Something went wrong while adding sources " + e);
			e.printStackTrace();
		}
		return new ResponseDto<String>(false);
	}

	@RequestMapping(value = "/getallsources", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<List<FeedSource>> getAllFeedSource() {
		try {
			List<FeedSource> fds = rssFeedAggegrator.fidAllFeedSources();
			if (fds != null && !fds.isEmpty())
				return new ResponseDto<List<FeedSource>>(true, fds);
		} catch (Exception e) {
			logger.error("Something went wrong while adding sources " + e);
			e.printStackTrace();
		}
		return new ResponseDto<List<FeedSource>>(false);
	}

	@RequestMapping(value = "/findsource", method = RequestMethod.GET, consumes = "application/json")
	public @ResponseBody ResponseDto<List<FeedSource>> findFeedSource(
			String sourceName) {
		try {
			List<FeedSource> fds = rssFeedAggegrator.findFeedSoruce(sourceName);
			if (fds != null && !fds.isEmpty())
				return new ResponseDto<List<FeedSource>>(true, fds);
		} catch (Exception e) {
			logger.error("Something went wrong while adding sources " + e);
			e.printStackTrace();
		}
		return new ResponseDto<List<FeedSource>>(false);
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET, consumes = "application/json")
	public @ResponseBody ResponseDto<List<NewsFeed>> searchNewsFeed(
			String categories) {
		List<String> keywords= Arrays.asList(categories.split(","));
		List<NewsFeed> nfs = rssFeedAggegrator.searchText(keywords);
		if (nfs != null && !nfs.isEmpty()) {
			return new ResponseDto<List<NewsFeed>>(true, nfs);
		}
		return new ResponseDto<List<NewsFeed>>(false);
	}
}
