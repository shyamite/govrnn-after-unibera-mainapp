package com.ohmuk.folitics.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ohmuk.folitics.businessDelegate.interfaces.IOpinionBusinessDelegate;
import com.ohmuk.folitics.businessDelegate.interfaces.IPollBusinessDelegate;
import com.ohmuk.folitics.businessDelegate.interfaces.IBreakingNewsBusinessDelegate;
import com.ohmuk.folitics.component.newsfeed.ImageSizeEnum;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.enums.FileType;
import com.ohmuk.folitics.enums.BreakingNewsState;
import com.ohmuk.folitics.hibernate.entity.BreakingNewsNews;
import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.Link;
import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.BreakingNews;
import com.ohmuk.folitics.hibernate.entity.attachment.Attachment;
import com.ohmuk.folitics.ouput.model.BreakingNewsOutputModel;
import com.ohmuk.folitics.ouput.model.BreakingNewsOutputModel;
import com.ohmuk.folitics.ouput.model.BreakingNewsOutputModel;
//import com.ohmuk.folitics.service.newsfeed.IBreakingNewsNewsLoaderService;
import com.ohmuk.folitics.util.AttachmentUtil;
import com.ohmuk.folitics.util.ThumbnailUtil;

/**
 * @author Asif
 * Has to work on it
 */
@Controller
@RequestMapping("/breakingnews")
public class BreakingNewsController {

	private static Logger LOGGER = LoggerFactory
			.getLogger(BreakingNewsController.class);

	@Autowired
	private volatile IBreakingNewsBusinessDelegate breakingnewsBusinessDelegate;
	
	@Autowired
	private volatile IPollBusinessDelegate pollBusinessDelegate;
	
	@Autowired
	private IOpinionBusinessDelegate opinionBusinessDelegate;
	
//	@Autowired
//	private IBreakingNewsNewsLoaderService breakingnewsNewsLoaderService;

	@RequestMapping
	public String getBreakingNewsPage() {

		return "breakingnews-page";
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<BreakingNews> getAdd() {

		List<BreakingNews> breakingnewss = new ArrayList<>();
		breakingnewss.add(getTestBreakingNews());
		return new ResponseDto<BreakingNews>(true, breakingnewss);
	}

	/**
	 * This method is used to add breakingnews. This is a multipart spring web
	 * service that requires image and json for breakingnews.
	 * 
	 * @author gautam.yadav
	 * @param image
	 * @param breakingnews
	 * @return ResponseDto<BreakingNews>
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<BreakingNews> add(
			@RequestPart(value = "file") MultipartFile image,
			@RequestPart(value = "breakingnews") BreakingNews breakingnews) {

		LOGGER.info("Inside BreakingNewsController add method");
		if (image == null) {
			LOGGER.error("Image is null", new Exception(
					"Image is required to add breakingnews"));
			return new ResponseDto<BreakingNews>(false);
		}
		if (breakingnews == null) {
			LOGGER.error("BreakingNews is null",
					new Exception("BreakingNews is null"));
			return new ResponseDto<BreakingNews>(false);
		}
		try {
			LOGGER.debug("Image " + image.getOriginalFilename() + "Received "
					+ "Image size " + image.getSize());
			LOGGER.debug("BreakingNews to be saved " + breakingnews.getSubject());

			breakingnews.setId(null);
			breakingnews.setState(BreakingNewsState.ALIVE.getValue());

			breakingnews.setImage(image.getBytes());
			breakingnews.setImageType(FileType.getFileType(
					image.getOriginalFilename().split("\\.")[1]).getValue());
			/*
			 * List<Poll> polls = new ArrayList<Poll>(); if
			 * (breakingnews.getPolls() != null) { for (Poll poll :
			 * breakingnews.getPolls()) { // poll.setBreakingNews(breakingnews);
			 * 
			 * Poll sessionPoll = pollBusinessDelegate.getPollById(poll
			 * .getId()); //sessionPoll.setBreakingNews(breakingnews);
			 * polls.add(sessionPoll);
			 * 
			 * // pollBusinessDelegate.saveAndFlush(sessionPoll);
			 * 
			 * logger.debug("Poll :" + sessionPoll.getQuestion() + " saved"); }
			 * breakingnews.setPolls(polls); }
			 */
			breakingnews = breakingnewsBusinessDelegate.save(breakingnews);
			LOGGER.debug("BreakingNews and polls saved/updated");

			return new ResponseDto<BreakingNews>(true, breakingnews);

		} catch (IOException ioException) {
			LOGGER.error("Error saving breakingnews/poll", ioException);
			LOGGER.error("Exiting add breakingnews");

			return new ResponseDto<BreakingNews>(false);
		} catch (Exception exception) {
			LOGGER.error("Error saving breakingnews/poll", exception);
			LOGGER.info("Exiting from BreakingNewsController add method");
			return new ResponseDto<BreakingNews>(false);
		}
	}

	/**
	 * This method is used to add breakingnews. This is a multipart spring web
	 * service that requires image and json for breakingnews.
	 * 
	 * @author gautam.yadav
	 * @param image
	 * @param breakingnews
	 * @return ResponseDto<BreakingNews>
	 */
	@RequestMapping(value = "/addBreakingNews", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<BreakingNews> addBreakingNews(
			@RequestPart(value = "file") MultipartFile image,
			@RequestPart(value = "attachments") List<MultipartFile> files,
			@RequestPart(value = "breakingnews") BreakingNews breakingnews) {

		LOGGER.info("Inside BreakingNewsController add method");
		if (image == null) {
			LOGGER.error("Image is null", new Exception(
					"Image is required to add breakingnews"));
			return new ResponseDto<BreakingNews>(false);
		}
		if (breakingnews == null) {
			LOGGER.error("BreakingNews is null",
					new Exception("BreakingNews is null"));
			return new ResponseDto<BreakingNews>(false);
		}
		try {
			LOGGER.debug("Image " + image.getOriginalFilename() + "Received "
					+ "Image size " + image.getSize());
			LOGGER.debug("BreakingNews to be saved " + breakingnews.getSubject());

			breakingnews.setId(null);
			breakingnews.setState(BreakingNewsState.ALIVE.getValue());

			breakingnews.setImage(image.getBytes());
			breakingnews.setImageType(FileType.getFileType(
					image.getOriginalFilename().split("\\.")[1]).getValue());

			List<Attachment> attachments = new ArrayList<Attachment>();
			for (MultipartFile file : files) {
				Attachment attachment = new Attachment();
				attachments.add(AttachmentUtil.prepareAttachment(file,
						attachment));
			}
			breakingnews = breakingnewsBusinessDelegate.save(breakingnews);
			LOGGER.debug("BreakingNews and polls saved/updated");

			return new ResponseDto<BreakingNews>(true, breakingnews);

		} catch (IOException ioException) {
			LOGGER.error("Error saving breakingnews/poll", ioException);
			LOGGER.error("Exiting add breakingnews");

			return new ResponseDto<BreakingNews>(false);
		} catch (Exception exception) {
			LOGGER.error("Error saving breakingnews/poll", exception);
			LOGGER.info("Exiting from BreakingNewsController add method");
			return new ResponseDto<BreakingNews>(false);
		}
	}

	/**
	 * Web service is to edit {@link BreakingNews}
	 * 
	 * @param breakingnews
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<BreakingNews> edit(
			@RequestBody BreakingNews breakingnews) {
		LOGGER.info("Inside BreakingNewsController edit method");
		try {
			breakingnews = breakingnewsBusinessDelegate.update(breakingnews);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in updating BreakingNews");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from BreakingNewsController edit method");

			return new ResponseDto<BreakingNews>(false);
		}
		if (breakingnews != null) {
			LOGGER.debug("BreakingNews with id: " + breakingnews.getId()
					+ " is update");
			LOGGER.info("Exiting from BreakingNewsController edit method");
			return new ResponseDto<BreakingNews>(true, breakingnews);
		}
		LOGGER.debug("BreakingNews is not update");
		LOGGER.info("Exiting from BreakingNewsController edit method");
		return new ResponseDto<BreakingNews>(false);
	}

	/**
	 * Web service to clone {@link BreakingNews}
	 * 
	 * @param breakingnews
	 * @return
	 */
	@RequestMapping(value = "/clone", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<BreakingNews> clone(
			@RequestBody BreakingNews breakingnews) {
		LOGGER.info("Inside BreakingNewsController clone method");
		if (null != breakingnews) {
			try {
				breakingnews = breakingnewsBusinessDelegate.clone(breakingnews);
			} catch (Exception exception) {
				LOGGER.error("Exception in cloning BreakingNews");
				LOGGER.error("Exception: " + exception);

				return new ResponseDto<BreakingNews>(false);
			}
			LOGGER.debug("BreakingNews clone with id :" + breakingnews.getId()
					+ " is saved");
			/*
			 * try { for (Poll poll : breakingnews.getPolls()) {
			 * poll.setBreakingNews(breakingnews);
			 * 
			 * Poll sessionPoll = pollBusinessDelegate.getPollById(poll
			 * .getId()); sessionPoll.setBreakingNews(breakingnews);
			 * 
			 * pollBusinessDelegate.saveAndFlush(sessionPoll);
			 * 
			 * logger.debug("Poll :" + sessionPoll.getQuestion() + " saved"); }
			 * } catch (Exception exception) {
			 * logger.error("Error saving breakingnews/poll", exception);
			 * logger.error("Exiting add breakingnews");
			 * logger.info("Exiting from BreakingNewsController clone method");
			 * 
			 * return new ResponseDto<BreakingNews>(false); }
			 */
			LOGGER.info("Exiting from BreakingNewsController clone method");
			return new ResponseDto<BreakingNews>(true, breakingnews);
		}
		LOGGER.info("Exiting from BreakingNewsController clone method");
		return new ResponseDto<BreakingNews>(false);

	}

	/**
	 * Web service is to soft delete {@link BreakingNews} by id
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/deleteById", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<BreakingNews> delete(Long id) {
		LOGGER.info("Inside BreakingNewsController deleteById method");
		try {
			if (breakingnewsBusinessDelegate.delete(id)) {
				LOGGER.debug("BreakingNews with id: " + id + " is soft delete");
				LOGGER.info("Exiting from BreakingNewsController deleteById method");
				return new ResponseDto<BreakingNews>(true);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in deleting BreakingNews");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from BreakingNewsController deleteById method");

			return new ResponseDto<BreakingNews>(false);
		}
		LOGGER.debug("BreakingNews with id: " + id + " is not delete");
		LOGGER.info("Exiting from BreakingNewsController deleteById method");
		return new ResponseDto<BreakingNews>(false);
	}

	/**
	 * Web service is to soft delete {@link BreakingNews}
	 * 
	 * @param breakingnews
	 * @return
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<BreakingNews> delete(
			@RequestBody BreakingNews breakingnews) {
		LOGGER.info("Inside BreakingNewsController delete method");
		try {
			if (breakingnewsBusinessDelegate.delete(breakingnews)) {
				LOGGER.debug("BreakingNews with id: " + breakingnews.getId()
						+ " is soft delete");
				LOGGER.info("Exiting from BreakingNewsController delete method");
				return new ResponseDto<BreakingNews>(true);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in updating BreakingNews");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from BreakingNewsController delete method");

			return new ResponseDto<BreakingNews>(false);
		}
		LOGGER.debug("BreakingNews is not delete");
		LOGGER.info("Exiting from BreakingNewsController delete method");
		return new ResponseDto<BreakingNews>(false);
	}

	/**
	 * Web service is to hard delete {@link BreakingNews} by id
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/deleteFromDbById", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<BreakingNews> deleteFromDB(
			@RequestParam Long id) {
		LOGGER.info("Inside BreakingNewsController deleteFromDbById method");
		try {
			if (breakingnewsBusinessDelegate.deleteFromDB(id)) {
				LOGGER.debug("BreakingNews with id: " + id + " is deleted from DB");
				LOGGER.info("Exiting from BreakingNewsController deleteFromDbById method");
				return new ResponseDto<BreakingNews>(true);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in deleteFromDBbyId BreakingNews");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from BreakingNewsController deleteFromDbById method");

			return new ResponseDto<BreakingNews>(false);
		}
		LOGGER.debug("BreakingNews with id: " + id + " is not deleted");
		LOGGER.info("Exiting from BreakingNewsController deleteFromDbById method");
		return new ResponseDto<BreakingNews>(false);
	}

	/**
	 * Web service is to delete {@link BreakingNews}
	 * 
	 * @param breakingnews
	 * @return
	 */
	@RequestMapping(value = "/deleteFromDb", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<BreakingNews> deleteFromDB(
			@RequestBody BreakingNews breakingnews) {
		LOGGER.info("Inside BreakingNewsController deleteFromDb method");
		try {
			if (breakingnewsBusinessDelegate.deleteFromDB(breakingnews)) {
				LOGGER.debug("BreakingNews of id: " + breakingnews.getId()
						+ " is deleted form DB");
				return new ResponseDto<BreakingNews>(true);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in deleteFromdb BreakingNews : "
					+ breakingnews.getId());
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from BreakingNewsController deleteFromDb method");

			return new ResponseDto<BreakingNews>(false);
		}
		LOGGER.debug("BreakingNews is not deleted");
		LOGGER.info("Exiting from BreakingNewsController deleteFromDb method");
		return new ResponseDto<BreakingNews>(false);
	}

	/**
	 * Web service is to get all source ( {@link Link} ) by {@link BreakingNews} id
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/getAllSources", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Link> getAllSources(Long id) {
		LOGGER.info("Inside BreakingNewsController getAllSources method");
		BreakingNews breakingnews = null;
		try {
			breakingnews = breakingnewsBusinessDelegate.read(id);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in getAllSources BreakingNews");
			LOGGER.error("Exception: " + exception);

			return new ResponseDto<Link>(false);
		}
		if (breakingnews != null) {
			List<Link> linkSet = null;
			try {
				linkSet = breakingnewsBusinessDelegate
						.getAllSourcesForBreakingNews(breakingnews);
			} catch (Exception exception) {
				LOGGER.error("Exception in getAllSources 2 BreakingNews");
				LOGGER.error("Exception: " + exception);
				LOGGER.info("Exiting from BreakingNewsController getAllSources method");

				return new ResponseDto<Link>(false);
			}
			if (linkSet != null) {
				LOGGER.debug(linkSet.size()
						+ " source is found for breakingnews with id: " + id);
				LOGGER.info("Exiting from BreakingNewsController getAllSources method");
				return new ResponseDto<Link>(true, linkSet);
			}
		}
		LOGGER.debug("No source is found for breakingnews with id: " + id);
		LOGGER.info("Exiting from BreakingNewsController getAllSources method");
		return new ResponseDto<Link>(false);
	}

	/**
	 * Web service is to get {@link BreakingNews} by id
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/find", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<BreakingNews> find(Long id) {
		LOGGER.info("Inside BreakingNewsController find method");
		BreakingNews breakingnews = null;
		try {
			breakingnews = breakingnewsBusinessDelegate.read(id);
			if (breakingnews != null) {
//				try {
//					breakingnews.setImage( ThumbnailUtil.getBreakingNewsImageThumbnail(breakingnews.getImage(),breakingnews.getImageType()));
//				} catch (IOException e) {
//				}
				LOGGER.debug("BreakingNews is found for id: " + id);
				LOGGER.info("Exiting from BreakingNewsController find method");
				return new ResponseDto<BreakingNews>(true, breakingnews);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in finding BreakingNews");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from BreakingNewsController find method");

			return new ResponseDto<BreakingNews>(false);
		}
		LOGGER.debug("BreakingNews is not found for id: " + id);
		LOGGER.info("Exiting from BreakingNewsController find method");
		return new ResponseDto<BreakingNews>(false);
	}


	/**
	 * Web service is to get all {@link BreakingNews}
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<BreakingNewsNews> getall() {
		LOGGER.info("Inside BreakingNewsController getAll method");
		List<BreakingNewsNews> breakingnewss = null;
		try {
			breakingnewss = breakingnewsBusinessDelegate.readAll();
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in getAll BreakingNews");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from BreakingNewsController getAll method");

			return new ResponseDto<BreakingNewsNews>(false);
		}
		if (breakingnewss != null) {
			LOGGER.debug(breakingnewss.size() + " breakingnews is found");
			LOGGER.info("Exiting from BreakingNewsController getAll method");
			return new ResponseDto<BreakingNewsNews>(true, breakingnewss);
		}
		LOGGER.debug("No breakingnews is found");
		LOGGER.info("Exiting from BreakingNewsController getAll method");
		return new ResponseDto<BreakingNewsNews>(false);
	}

	@RequestMapping(value = "/findByType", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<BreakingNews> findByType(String type) {
		LOGGER.info("Inside BreakingNewsController findByType method");
		List<BreakingNews> breakingnewss = null;
		try {
			breakingnewss = breakingnewsBusinessDelegate.findByType(type);
		} catch (Exception exception) {
			LOGGER.error("Exception in findByType BreakingNews");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from BreakingNewsController findbyType method");

			return new ResponseDto<BreakingNews>(false);
		}
		if (breakingnewss != null) {
			LOGGER.debug(breakingnewss.size() + " breakingnews is found");
			LOGGER.info("Exiting from BreakingNewsController find by type method");
			return new ResponseDto<BreakingNews>(true, breakingnewss);
		}
		LOGGER.debug("No breakingnews is found");
		LOGGER.info("Exiting from BreakingNewsController findByType method");
		return new ResponseDto<BreakingNews>(false);
	}

	/**
	 * Web service is to get all {@link BreakingNews} not having breakingnews in ids
	 * 
	 * @author Asif Iqubal
	 * @return new ResponseDto<BreakingNews>
	 */
	@RequestMapping(value = "/getAllBreakingNewsNotIn", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<BreakingNews> getAllBreakingNewsNotIn(
			@RequestBody Set<Long> ids) {
		LOGGER.info("Inside BreakingNewsController getAllBreakingNewsNotIn method");
		List<BreakingNews> breakingnewss = null;
		try {
			breakingnewss = breakingnewsBusinessDelegate.getAllBreakingNewsNotIn(ids);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in getAllBreakingNewsNotIn BreakingNews");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from BreakingNewsController getAllBreakingNewsNotIn method");

			return new ResponseDto<BreakingNews>(false);
		}
		if (breakingnewss != null) {
			LOGGER.debug(breakingnewss.size() + " BreakingNews found");
			LOGGER.info("Exiting from BreakingNewsController getAllBreakingNewsNotIn method");
			return new ResponseDto<BreakingNews>(true, breakingnewss);
		}
		LOGGER.debug("No breakingnews found");
		LOGGER.info("Exiting from BreakingNewsController getAllBreakingNewsNotIn method");
		return new ResponseDto<BreakingNews>(false);
	}

	/**
	 * Web service is to update status of breakingnews by id
	 * 
	 * @author Asif Iqubal
	 * @param allRequestParams
	 * @return
	 */
	@RequestMapping(value = "/updateStatus", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<BreakingNews> deleteFromDB(
			@RequestParam Map<String, Object> allRequestParams) {
		LOGGER.info("Inside BreakingNewsController updateStatus method");
		Long id = Long.parseLong((String) allRequestParams.get("id"));
		String status = (String) allRequestParams.get("status");
		if (id != 0 && status != null) {
			try {
				breakingnewsBusinessDelegate.updateBreakingNewsStatus(id, status);
			} catch (Exception exception) {
				exception.printStackTrace();
				LOGGER.error("Exception in updateStatus BreakingNews");
				LOGGER.error("Exception: " + exception);
				LOGGER.info("Exiting from BreakingNewsController updateStatus method");

				return new ResponseDto<BreakingNews>(false);
			}
			LOGGER.debug("breakingnews with id: " + id + " status is changed to "
					+ status);
			LOGGER.info("Exiting from BreakingNewsController updateStatus method");
			return new ResponseDto<BreakingNews>(true);
		}
		LOGGER.debug("breakingnews with id: " + id + " status is not changed to "
				+ status);
		LOGGER.info("Exiting from BreakingNewsController updateStatus method");
		return new ResponseDto<BreakingNews>(false);

	}

	/**
	 * Web service is to get list of indicator attached with breakingnews
	 * 
	 * @param id
	 * @return ResponseDto<Category>
	 * @author Asif Iqubal
	 */
	@RequestMapping(value = "/getAllBreakingNewsIndicator", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Set<Category>> getAllBreakingNewsIndicator(
			Long id) {
		LOGGER.info("Inside BreakingNewsController getAllBreakingNewsIndicator method");
		Set<Category> indicadtors = null;
		try {
			indicadtors = breakingnewsBusinessDelegate.getAllIndicator(id);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in getAllBreakingNewsIndicator BreakingNews");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from BreakingNewsController getAllBreakingNewsIndicator method");

			return new ResponseDto<Set<Category>>(false);
		}
		if (null != indicadtors) {
			LOGGER.debug(indicadtors.size() + " indicators is found");
			LOGGER.info("Exiting from BreakingNewsController getAllBreakingNewsIndicator method");
			return new ResponseDto<Set<Category>>(true, indicadtors);
		} else {
			LOGGER.debug("No indicator is found");
			LOGGER.info("Exiting from BreakingNewsController getAllBreakingNewsIndicator method");
			return new ResponseDto<Set<Category>>(false);
		}
	}

	@RequestMapping(value = "/getBreakingNewsList", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<BreakingNewsOutputModel> getBreakingNewsList(
			String type) {
		LOGGER.info("Inside BreakingNewsController getBreakingNewsList method");
		List<BreakingNewsOutputModel> breakingnewsModels = null;
		try {
			breakingnewsModels = breakingnewsBusinessDelegate
					.getAllBreakingNewsByType(type);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in updating BreakingNews");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from BreakingNewsController getBreakingNewsList method");

			return new ResponseDto<BreakingNewsOutputModel>(false);
		}
		if (breakingnewsModels != null) {
			LOGGER.debug(breakingnewsModels.size() + " breakingnews is found");
			LOGGER.info("Exiting from BreakingNewsController getBreakingNewsList method");
			return new ResponseDto<BreakingNewsOutputModel>(true, breakingnewsModels);
		}
		LOGGER.debug("No breakingnews is found");
		LOGGER.info("Exiting from BreakingNewsController getBreakingNewsList method");
		return new ResponseDto<BreakingNewsOutputModel>(false);
	}

	@RequestMapping(value = "/getTestBreakingNews", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody BreakingNews getTestBreakingNews() {

		return getDummyBreakingNews();
	}

	@RequestMapping(value = "/saveNewsFeed", method = RequestMethod.POST)
	public @ResponseBody void saveNewsFeed() {

		breakingnewsBusinessDelegate.saveNewsFeed();

	}

	
	
	@RequestMapping(value = "/downloadBreakingNewsImage", method = RequestMethod.GET)
	public @ResponseBody byte[] downloadUserImage(
			@RequestParam("breakingnewsId") Long breakingnewsId,
			@RequestParam("imageType") String imageType,
			@RequestParam("imageSize") String imageSize) {
		LOGGER.info("Inside UserController downloadUserImage method");
		BreakingNews breakingnews = null;
		try {
			// Need to add delegate method
			breakingnews = breakingnewsBusinessDelegate.read(breakingnewsId);
			if (breakingnews == null) {
				LOGGER.debug("BreakingNews not found breakingnews id: " + breakingnewsId);
				LOGGER.info("Exiting from BreakingNewsController downloadBreakingNewsImage method");
				return null;
			}
			byte[] bytes = null;
			String fileType = null;
			fileType = "jpg";
			bytes = breakingnews.getImage();

			if (imageSize.equals(ImageSizeEnum.THUMBNAIL.getImageSize())) {
				return ThumbnailUtil.getImageThumbnail(bytes, fileType);
			} else if (imageSize.equals(ImageSizeEnum.STANDARD.getImageSize())) {
				return ThumbnailUtil.getImageOriginal(bytes, fileType);
			} else if (imageSize.equals(ImageSizeEnum.SNAPSHOT.getImageSize())) {
				return ThumbnailUtil.getImageSnapshot(bytes, fileType);
			}

		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in downloadBreakingNewsImage with user id: "
					+ breakingnewsId);
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from BreakingNewsController downloadBreakingNewsImage method");
			return null;
		}
		LOGGER.info("Exiting from BreakingNewsController downloadBreakingNewsImage method");
		return null;
	}

	private BreakingNews getDummyBreakingNews() {

		BreakingNews breakingnews = new BreakingNews();
		breakingnews.setSubject("Subject for test BreakingNews");
		breakingnews.setDescription("Description for test BreakingNews");
		return breakingnews;

	}

	@RequestMapping(value = "/loadBreakingNewsNewsRSS", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Boolean> loadBreakingNews() {
		// breakingnewsNewsLoaderService.loadNewsForAllBreakingNewss();
		LOGGER.info("loadBreakingNewsNewsRSS ----");
		return new ResponseDto<Boolean>(true);
		
	}
	
	@RequestMapping(value = "/getPagedBreakingNewsList", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<BreakingNewsOutputModel> getPagedBreakingNewsList(
			String type,int from,int to) {
		LOGGER.info("Inside BreakingNewsController getPagedBreakingNewsList method");
		List<BreakingNewsOutputModel> breakingnewsModels = null;
		try {
			breakingnewsModels = breakingnewsBusinessDelegate
					.getPaginatedBreakingNewsByType(type, from, to);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in getPagedBreakingNewsList BreakingNews");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from BreakingNewsController getPagedBreakingNewsList method");

			return new ResponseDto<BreakingNewsOutputModel>(false);
		}
		if (breakingnewsModels != null) {
			LOGGER.debug(breakingnewsModels.size() + " breakingnews is found");
			LOGGER.info("Exiting from BreakingNewsController getPagedBreakingNewsList method");
			return new ResponseDto<BreakingNewsOutputModel>(true, breakingnewsModels);
		}
		LOGGER.debug("No breakingnews is found");
		LOGGER.info("Exiting from BreakingNewsController getPagedBreakingNewsList method");
		return new ResponseDto<BreakingNewsOutputModel>(false);
	}
	
	@RequestMapping(value = "/getPagedBreakingNewsListByStatus", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<BreakingNewsOutputModel> getPagedBreakingNewsListByStatus(
			String type, String status, int from,int to) {
		LOGGER.info("Inside BreakingNewsController getPagedBreakingNewsListBystatus method");
		List<BreakingNewsOutputModel> breakingnewsModels = null;
		try {
			breakingnewsModels = breakingnewsBusinessDelegate
					.getPaginatedBreakingNewsByStatus(type, status, from, to);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in getPagedBreakingNewsList BreakingNews");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from BreakingNewsController getPagedBreakingNewsListByStatus method");

			return new ResponseDto<BreakingNewsOutputModel>(false);
		}
		if (breakingnewsModels != null) {
			LOGGER.debug(breakingnewsModels.size() + " breakingnews is found");
			LOGGER.info("Exiting from BreakingNewsController getPagedBreakingNewsListByStatus method");
			return new ResponseDto<BreakingNewsOutputModel>(true, breakingnewsModels);
		}
		LOGGER.debug("No breakingnews is found");
		LOGGER.info("Exiting from BreakingNewsController getPagedBreakingNewsListBystatus method");
		return null;
	}	
		
	@RequestMapping(value = "/getBreakingNewsListSizeByType", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Integer> getBreakingNewsListSizeByType(
			String type) {
		LOGGER.info("Inside BreakingNewsController getBreakingNewsListSizeByType method");
		int breakingnewsListSize;
		try {
			breakingnewsListSize =  (breakingnewsBusinessDelegate.findByType(type) != null ? breakingnewsBusinessDelegate.findByType(type).size():0 );
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in getPagedBreakingNewsList BreakingNews");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from BreakingNewsController getBreakingNewsListSizeByType method");

			return new ResponseDto<Integer>(false);
		}
		if (breakingnewsListSize > 0 ) {
			LOGGER.debug(breakingnewsListSize + " breakingnews is found");
			LOGGER.info("Exiting from BreakingNewsController getBreakingNewsListSizeByType method");
			return new ResponseDto<Integer>(true, breakingnewsListSize);
		}
		LOGGER.debug("No breakingnews is found");
		LOGGER.info("Exiting from BreakingNewsController getBreakingNewsListSizeByType method");
		return new ResponseDto<Integer>(false);
	}
	
	@RequestMapping(value = "/getBreakingNewsNameList", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<BreakingNewsOutputModel> getBreakingNewsNameList(
			String type) {
		LOGGER.info("Inside BreakingNewsController getBreakingNewsNameList method");
		List<BreakingNewsOutputModel> breakingnewsModels = null;
		try {
			breakingnewsModels = breakingnewsBusinessDelegate
					.getAllBreakingNewsByType(type);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in updating BreakingNews");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from BreakingNewsController getBreakingNewsNameList method");

			return new ResponseDto<BreakingNewsOutputModel>(false);
		}
		if (breakingnewsModels != null) {
			LOGGER.debug(breakingnewsModels.size() + " breakingnews is found");
			LOGGER.info("Exiting from BreakingNewsController getBreakingNewsNameList method");
			return new ResponseDto<BreakingNewsOutputModel>(true, breakingnewsModels);
		}
		LOGGER.debug("No breakingnews is found");
		LOGGER.info("Exiting from BreakingNewsController getBreakingNewsNameList method");
		return new ResponseDto<BreakingNewsOutputModel>(false);
	}
	
	@RequestMapping(value = "/getBreakingNewsListByStatus", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<BreakingNewsOutputModel> getBreakingNewsListByStatus(
			String type, String status) {
		LOGGER.info("Inside BreakingNewsController getBreakingNewsNameListbystatus method");
		List<BreakingNewsOutputModel> breakingnewsModels = null;
		try {
			breakingnewsModels = breakingnewsBusinessDelegate
					.getBreakingNewsListByStatus(type, status);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in updating BreakingNews");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from BreakingNewsController getBreakingNewsNameListbystatus method");

			return new ResponseDto<BreakingNewsOutputModel>(false);
		}
		if (breakingnewsModels != null) {
			LOGGER.debug(breakingnewsModels.size() + " breakingnews is found");
			LOGGER.info("Exiting from BreakingNewsController getBreakingNewsNameListbystatus method");
			return new ResponseDto<BreakingNewsOutputModel>(true, breakingnewsModels);
		}
		LOGGER.debug("No breakingnews is found");
		LOGGER.info("Exiting from BreakingNewsController getBreakingNewsNameListbystatus method");
		return new ResponseDto<BreakingNewsOutputModel>(false);
	}
	
	
	/**
	 * Web service is to hide the breakingnews
	 * 
	 * @author Asif Iqubal
	 * @param allRequestParams
	 * @return
	 */
	@RequestMapping(value = "/hideBreakingNews", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Object> hideBreakingNews(String breakingnewsName, String breakingnewsType) {
		LOGGER.info("Inside BreakingNewsController hideBreakingNews method");
		boolean result  = false;
		try {
			result  = breakingnewsBusinessDelegate.changeBreakingNewsState(breakingnewsName, breakingnewsType, BreakingNewsState.HIDDEN);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in hideBreakingNews BreakingNews");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from BreakingNewsController updateStatus method");
				return new ResponseDto<Object>(false);
		}
		LOGGER.info("Exiting from BreakingNewsController hideBreakingNews method");
		return new ResponseDto<Object>(result);
	}
	
	@RequestMapping(value = "/disableEnableBreakingNews", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Object> disableEnableBreakingNews(@RequestParam("enabled") boolean enabled,
			@RequestParam("id") Long id) {
		LOGGER.info("Inside BreakingNewsController enableBreakingNews method");
		boolean result  = false;
		try {
			result  = breakingnewsBusinessDelegate.enableDisableBreakingNews(enabled, id);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in hideBreakingNews BreakingNews");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from BreakingNewsController updateStatus method");
				return new ResponseDto<Object>(false);
		}
		LOGGER.info("Exiting from BreakingNewsController hideBreakingNews method");
		return new ResponseDto<Object>(result);
	}

	/**
	 * Web service is to hide the breakingnews
	 * 
	 * @author Asif Iqubal
	 * @param allRequestParams
	 * @return
	 */
	@RequestMapping(value = "/deleteBreakingNews", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Object> deleteBreakingNews(String breakingnewsName, String breakingnewsType) {
		LOGGER.info("Inside BreakingNewsController hideBreakingNews method");
		boolean result  = false;
		try {
			result  = breakingnewsBusinessDelegate.changeBreakingNewsState(breakingnewsName, breakingnewsType, BreakingNewsState.HIDDEN);
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in hideBreakingNews BreakingNews");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from BreakingNewsController updateStatus method");
				return new ResponseDto<Object>(false);
		}
		LOGGER.info("Exiting from BreakingNewsController hideBreakingNews method");
		return new ResponseDto<Object>(result);
	}

}
