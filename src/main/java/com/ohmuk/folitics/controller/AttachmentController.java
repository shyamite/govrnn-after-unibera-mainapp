package com.ohmuk.folitics.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ohmuk.folitics.businessDelegate.interfaces.IAttachmentBusinessDeligate;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.enums.AttachmentType;
import com.ohmuk.folitics.enums.FileType;
import com.ohmuk.folitics.hibernate.entity.attachment.Attachment;
import com.ohmuk.folitics.hibernate.entity.attachment.AttachmentFile;

/**
 * @author soumya
 * 
 */
@Controller
@Transactional
@RequestMapping("/attachment")
public class AttachmentController {

	@Autowired
	private volatile IAttachmentBusinessDeligate attachmentBusinessDeligate;

	private static Logger logger = LoggerFactory.getLogger(AttachmentController.class);

	/**
	 * This method is used to add attachment. This is a multipart spring web
	 * service that requires image.
	 * 
	 * @author soumya
	 * @param image
	 * @param sentiment
	 * @return ResponseDto<String>
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Long> add(@RequestPart(value = "file") MultipartFile image) {

		logger.info("Inside AttachmentController add method");
		Long attachmentId = null;
		if (image == null) {
			logger.error("Image is null", new Exception("Image is required to add sentiment"));
			return new ResponseDto<Long>(false, attachmentId, "No Attachemnt");
		}
		try {

			logger.info("Image " + image.getOriginalFilename() + "Received " + "Image size " + image.getSize());

			AttachmentFile file = new AttachmentFile();
			file.setData(image.getBytes());
			file.setEdited(new Timestamp(System.currentTimeMillis()));
			file.setTitle(image.getOriginalFilename());

			String fileType = FileType.getFileType(image.getOriginalFilename().split("\\.")[1]).getValue();
			file.setType(fileType);
			file.setTimestamp(new Timestamp(System.currentTimeMillis()));

			Attachment attachment = new Attachment();
			attachment.setAttachmentFile(file);
			attachment.setAttachmentType(AttachmentType.IMAGE.getValue());
			attachment.setDescription(image.getOriginalFilename());
			attachment.setFilePath(image.getOriginalFilename());		
			attachment.setFileType(fileType);
			attachment.setTitle(image.getName());
			

			attachmentId =  attachmentBusinessDeligate.create(attachment).getId();

			logger.debug("Attachment is saved");
			return new ResponseDto<Long>(true,attachmentId, "Attachment Saved");

		} catch (IOException ioException) {
			logger.error("Error saving Attachment", ioException);
			logger.error("Exiting add Attachment");

			return new ResponseDto<Long>(false, attachmentId, "Attachemnt Not Saved");
		} catch (Exception exception) {
			logger.error("Error saving Attachment", exception);
			logger.info("Exiting from AttachmentController add method");
			return new ResponseDto<Long>(false,attachmentId, "Attachemnt Not Saved");
		}
	}
	
	/**
	 * This method is used to delete attachment. 
	 * 
	 * @author Jahid
	 * @param id
	 * @return ResponseDto<String>
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<String> delete(Long id) {
		logger.info("Inside AttachmentController delete method");
		try {
			attachmentBusinessDeligate.delete(id);
			logger.debug("Attachment is deleted");
			return new ResponseDto<String>(true, "Attachment deleted");
		} catch (Exception exception) {
			logger.error("Error deleting Attachment", exception);
			logger.info("Exiting from AttachmentController delete method!");
			return new ResponseDto<String>(false, "Attachemnt delete falied!");
		}
	}
	
	/**
	 * This method is used to delete attachment. 
	 * 
	 * @author soumya
	 * @param id
	 * @return ResponseDto<String>
	 */
	@RequestMapping(value = "/download", method = RequestMethod.GET, produces="image/jpg")
	public ResponseEntity<InputStreamResource> download(Long id) {
		logger.info("Inside AttachmentController download method");
		try {
			Attachment attachment = attachmentBusinessDeligate.read(id);
			if (null != attachment) {
				if (attachment.getAttachmentFile().getType().equalsIgnoreCase("jpg")) {
					HttpHeaders headers = new HttpHeaders();
				    headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
				    headers.add("Pragma", "no-cache");
				    headers.add("Expires", "0");
				    headers.add("Content-Disposition", "attachment; filename=" + attachment.getAttachmentFile().getTitle());			
				    InputStream inputStream = new ByteArrayInputStream(attachment.getAttachmentFile().getData()); 
				    
				    return ResponseEntity
				            .ok()
				            .headers(headers)
				            .contentType(MediaType.parseMediaType("application/octet-stream"))
				            .body(new InputStreamResource(inputStream));
				}
				logger.debug("Attachment is saved/downloaded");
				return null;

			}			
			return null;
			
		} catch (Exception exception) {
			logger.error("Error downloading Attachment", exception);
			logger.info("Exiting from AttachmentController download method!");
			return null;
		}
	}
	
	/**
	 * This method is used to delete attachment. 
	 * 
	 * @author soumya
	 * @param id
	 * @return ResponseDto<String>
	 */
	@RequestMapping(value = "/show", method = RequestMethod.GET, produces="image/jpg")
	public @ResponseBody byte[] show(Long id) {
		logger.info("Inside AttachmentController show method");
		try {
			Attachment attachment = attachmentBusinessDeligate.read(id);			
			if (null != attachment) {
				if (attachment.getAttachmentFile().getType().equalsIgnoreCase("jpg")) {
					
					return attachment.getAttachmentFile().getData();
				}
				logger.debug("Attachment is download");
				return null;

			}
			return null;
			
		} catch (Exception exception) {
			logger.error("Error saving Attachment", exception);
			logger.info("Exiting from AttachmentController show method!");
			return null;
		}
	}

}
