package com.ohmuk.folitics.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ohmuk.folitics.businessDelegate.interfaces.IUserBusinessDelegate;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.UserUINotification;
import com.ohmuk.folitics.mongodb.entity.NotificationMongo;
import com.ohmuk.folitics.mongodb.service.INotificationMongoService;
import com.ohmuk.folitics.ouput.model.GeneralNotificationOutputModel;
import com.ohmuk.folitics.ouput.model.OpinionNotificationOutputModel;
import com.ohmuk.folitics.service.IUserService;
import com.ohmuk.folitics.service.IUserUINotificationService;

/**
 * @author Soumya
 * 
 */
@Controller
@RequestMapping("/notification")
public class NotificationController {

    @Autowired
    private volatile IUserBusinessDelegate businessDelegate;

    @Autowired
    INotificationMongoService notificationMongoService;
    
    @Autowired
  private IUserUINotificationService userUINotificationService;
    
    @Autowired
    private IUserService userService;
    
    @Autowired 

    private static Logger logger = LoggerFactory.getLogger(NotificationController.class);

    @RequestMapping(value = "/getOpinionNotifications", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<OpinionNotificationOutputModel> getOpinionNotifications(Long userId,
            String componentType) throws Exception {
        logger.info("Inside NotificationController getOpinionNotifications method");
        List<OpinionNotificationOutputModel> opinionNotificationOutputModels;
        // Currently it is getting the top notification for the newly created
        // opinion
        opinionNotificationOutputModels = getAllOpinionNotifications(userId, componentType);
        logger.info("Exiting NotificationController getOpinionNotifications method");
        return new ResponseDto<OpinionNotificationOutputModel>(true, opinionNotificationOutputModels);
    }

    @RequestMapping(value = "/getGeneralNotifications", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<GeneralNotificationOutputModel> getGeneralNotifications(Long userId,
            String componentType) throws Exception {
        logger.info("Inside NotificationController getGeneralNotifications method");
        List<GeneralNotificationOutputModel> generalNotificationOutputModels;
        // TO DO : this will late changed accordingly to get appropriate values
        generalNotificationOutputModels = getGeneralNotificationsForUser(userId, componentType);
        logger.info("Exiting NotificationController getGeneralNotifications method");
        return new ResponseDto<GeneralNotificationOutputModel>(true, generalNotificationOutputModels);
    }

    @RequestMapping(value = "/getUnreadNotificationsCount", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<Integer> getUnreadNotifications(Long userId, String notificationType) {
        logger.info("Inside NotificationControl.getUnreadNotificationsCount method");
        try {
            if (null != userId) {
                User dbUser = userService.findUserById(userId);
                
                UserUINotification userUINotification = userUINotificationService.read(dbUser, notificationType);
                if(!userUINotification.getEnabled()){
                    return new ResponseDto<Integer>(true, 0);
                }
                else{
                    List<NotificationMongo> unreadNotifications = notificationMongoService.getUnreadNotificationForUser(
                            userId, notificationType);
                    if (unreadNotifications != null && !unreadNotifications.isEmpty()) {
                        return new ResponseDto<Integer>(true, unreadNotifications.size());
                    }
                }
            }
        } catch (Exception exception) {
            logger.error("Exception in reading all unread notifications count in NotificationControl.getUnreadNotifications with userId:"
                    + userId);
            logger.error("Exception: " + exception);
            logger.info("Exiting from NotificationControl.getUnreadNotificationsCount method");
            return new ResponseDto<Integer>(false);
        }
        return new ResponseDto<Integer>(false, 0);
    }

    @RequestMapping(value = "/updateNotificationreadStatus", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<NotificationMongo> updateNotificationreadStatus(String notificationMongoId) {
        logger.info("Inside NotificationControl.updateNotificationreadStatus method");
        // BigInteger bigInteger = new
        // BigDecimal(notificationMongoId).toBigInteger();
        try {
            if (null != notificationMongoId) {
                return new ResponseDto<NotificationMongo>(notificationMongoService.update(notificationMongoId));
            } else {
                return new ResponseDto<NotificationMongo>(false);
            }
        } catch (Exception exception) {
            logger.error("Exception in updating notification mongo db object in NotificationControl.updateNotificationreadStatus with id:"
                    + notificationMongoId);
            logger.error("Exception: " + exception);
            logger.info("Exiting from NotificationControl.updateNotificationreadStatus method");
            return new ResponseDto<NotificationMongo>(false);
        }
    }

    private List<OpinionNotificationOutputModel> getAllOpinionNotifications(Long userId, String componentType)
            throws Exception {
        List<OpinionNotificationOutputModel> opinionNotificationOutputModels = new ArrayList<>();
        opinionNotificationOutputModels = notificationMongoService.getTopOpinionNotificationForUser(userId,
                componentType);
        /* Collections.reverse(opinionNotificationOutputModels); */

        return opinionNotificationOutputModels;
    }

    private List<GeneralNotificationOutputModel> getGeneralNotificationsForUser(Long userId, String componentType)
            throws Exception {

        List<GeneralNotificationOutputModel> generalNotificationOutputModels = new ArrayList<>();

        generalNotificationOutputModels = notificationMongoService.getAllGeneralNotificationForUser(userId,
                componentType);

        return generalNotificationOutputModels;
    }

}
