package com.ohmuk.folitics.cvs.reader.input;

import java.io.IOException;

import au.com.bytecode.opencsv.CSVReader;

public interface CSVFileReader {
	CSVReader getReader(String filePath) throws  IOException ;
}
