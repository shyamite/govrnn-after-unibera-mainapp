package com.ohmuk.folitics.beans;

import java.sql.Timestamp;
import java.util.Map;

import com.ohmuk.folitics.hibernate.entity.rating.ReferenceRating;
import com.ohmuk.folitics.hibernate.entity.rating.SentimentNewsRating;

public class RatingDataBean {

	private Long componentId;

	private String componentType;

	private Long userId;
	
	private Long parentId;
	
	private Long rating;
	
	private String feelType;
	
	private Timestamp editTime;


	private Map rateAggregaionMap;
	
	private Map feelAggregation;

	private Map rateTotalMap;
	
	
	
	/**
	 * @return the rateTotalMap
	 */
	public Map getRateTotalMap() {
		return rateTotalMap;
	}

	/**
	 * @param rateTotalMap the rateTotalMap to set
	 */
	public void setRateTotalMap(Map rateTotalMap) {
		this.rateTotalMap = rateTotalMap;
	}

	/**
	 * @return the feelAggregation
	 */
	public Map getFeelAggregation() {
		return feelAggregation;
	}

	/**
	 * @param feelAggregation the feelAggregation to set
	 */
	public void setFeelAggregation(Map feelAggregation) {
		this.feelAggregation = feelAggregation;
	}

	/**
	 * @return the rateAggregaionMap
	 */
	public Map getRateAggregaionMap() {
		return rateAggregaionMap;
	}
	
	/**
	 * @return the editTime
	 */
	public Timestamp getEditTime() {
		return editTime;
	}

	/**
	 * @param editTime the editTime to set
	 */
	public void setEditTime(Timestamp editTime) {
		this.editTime = editTime;
	}

	/**
	 * @param rateAggregaionMap the rateAggregaionMap to set
	 */
	public void setRateAggregaionMap(Map rateAggregaionMap) {
		this.rateAggregaionMap = rateAggregaionMap;
	}
	/**
	 * @return the componentId
	 */
	public Long getComponentId() {
		return componentId;
	}
	/**
	 * @return the feelType
	 */
	public String getFeelType() {
		return feelType;
	}
	/**
	 * @param feelType the feelType to set
	 */
	public void setFeelType(String feelType) {
		this.feelType = feelType;
	}
	/**
	 * @param componentId
	 *            the componentId to set
	 */
	public void setComponentId(Long componentId) {
		this.componentId = componentId;
	}

	/**
	 * @return the componentType
	 */
	public String getComponentType() {
		return componentType;
	}

	/**
	 * @param componentType
	 *            the componentType to set
	 */
	public void setComponentType(String componentType) {
		this.componentType = componentType;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the rating
	 */
	public Long getRating() {
		return rating;
	}

	/**
	 * @param rating the rating to set
	 */
	public void setRating(Long rating) {
		this.rating = rating;
	}

	/**
	 * @return the parentId
	 */
	public Long getParentId() {
		return parentId;
	}

	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public static final RatingDataBean getRatingDataBean(ReferenceRating rating,
			String componentType) {
		if(rating!=null){
			RatingDataBean bean = new RatingDataBean();
			bean.setComponentType(componentType);
			bean.setParentId(rating.getRatingId().getParentId());
			bean.setUserId(rating.getRatingId().getUserId());
			bean.setComponentId(rating.getRatingId().getComponentId());
			bean.setRating(rating.getRating());
			return bean;
		}
		return null;
	}

	public static final RatingDataBean getRatingDataBean(SentimentNewsRating rating,
			String componentType) {
		if(rating!=null){
			RatingDataBean bean = new RatingDataBean();
			bean.setComponentType(componentType);
			bean.setParentId(rating.getRatingId().getParentId());
			bean.setUserId(rating.getRatingId().getUserId());
			bean.setComponentId(rating.getRatingId().getComponentId());
			bean.setRating(rating.getRating());
			bean.setFeelType(rating.getRatingId().getFeelType());
			bean.setEditTime(rating.getEditTime());
			return bean;
		}
		return null;
	}
}
