package com.ohmuk.folitics.mongodb.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ohmuk.folitics.businessDelegate.interfaces.IOpinionBusinessDelegate;
import com.ohmuk.folitics.businessDelegate.interfaces.IResponseBusinessDelegate;
import com.ohmuk.folitics.businessDelegate.interfaces.ISearchBusinessDeligate;
import com.ohmuk.folitics.businessDelegate.interfaces.IUserBusinessDelegate;
import com.ohmuk.folitics.component.newsfeed.ImageSizeEnum;
import com.ohmuk.folitics.component.newsfeed.ImageTypeEnum;
import com.ohmuk.folitics.constants.Constants;
import com.ohmuk.folitics.enums.NotificationReadStatus;
import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.Response;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.UserImage;
import com.ohmuk.folitics.mongodb.dao.NotificationMongodbDAO;
import com.ohmuk.folitics.mongodb.entity.NotificationMongo;
import com.ohmuk.folitics.mongodb.repository.INotificationMongoRepository;
import com.ohmuk.folitics.ouput.model.GeneralNotificationOutputModel;
import com.ohmuk.folitics.ouput.model.OpinionNotificationOutputModel;
import com.ohmuk.folitics.ouput.model.UserOutputModel;
import com.ohmuk.folitics.util.DateUtils;

@Service
@Transactional
public class NotificationMongoService implements INotificationMongoService {

    @Autowired
    INotificationMongoRepository notificationMongoRepository;

    @Autowired
    NotificationMongodbDAO notificationMongodbDAO;

    @Autowired
    private volatile IUserBusinessDelegate businessDelegate;

    @Autowired
    private IOpinionBusinessDelegate opinionBusinessDelegate;

    @Autowired
    private IResponseBusinessDelegate responseBuisnessDelegate;

    @Autowired
    private ISearchBusinessDeligate searchBusinessdelegate;

    @Value("${spring.profiles.active}")
    private String serverPath;

    @Override
    public NotificationMongo save(NotificationMongo notification) {

        notification = notificationMongoRepository.save(notification);
        return notification;
    }

    @Override
    public NotificationMongo read(String id) {

        NotificationMongo notification = notificationMongoRepository.findOne(id);
        return notification;
    }

    @Override
    public List<NotificationMongo> getUnreadNotificationForUser(Long userId, String notificationType) throws Exception {

        List<NotificationMongo> unreadNotifications = notificationMongodbDAO.findUnreadNotificationsForUser(userId,
                notificationType);

        return unreadNotifications;
    }

    @Override
    public List<NotificationMongo> getTopNotificationForUser(Long userId, String notificationType) throws Exception {

        List<NotificationMongo> unreadNotifications = notificationMongodbDAO.findTopNotificationsForUser(userId,
                notificationType);

        return unreadNotifications;
    }

    @Override
    public List<NotificationMongo> getAll() {

        List<NotificationMongo> notifications = notificationMongoRepository.findAll();
        return notifications;
    }

    @Override
    public Boolean update(String notificationId) {

        NotificationMongo originalNotification = notificationMongoRepository.findOne(notificationId);

        if (originalNotification == null) {
            return null;
        }
        originalNotification.setReadStatus(NotificationReadStatus.READ.getValue());

        if (notificationMongoRepository.save(originalNotification) != null) {
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(NotificationMongo notification) {

        String id = notification.getId();
        notificationMongoRepository.delete(notification);

        /*
         * if (notificationMongoRepository.findOne(id) == null) { return true; }
         * else { return false; }
         */
        return true;
    }

    @Override
    public List<OpinionNotificationOutputModel> getTopOpinionNotificationForUser(Long userId, String notificationType)
            throws Exception {

        List<OpinionNotificationOutputModel> opinionOutputModelList = new ArrayList<OpinionNotificationOutputModel>();

        List<NotificationMongo> unreadNotifications = notificationMongodbDAO.findTopNotificationsForUser(userId,
                notificationType);

        if (unreadNotifications == null) {

        } else {
            for (NotificationMongo notificationMongo : unreadNotifications) {

                Opinion opinion = new Opinion();

                opinion = opinionBusinessDelegate.read(notificationMongo.getComponentId());
                if (opinion != null) {
                    OpinionNotificationOutputModel model = new OpinionNotificationOutputModel();
                    model.setTitle(opinion.getSubject());
                    model.setCategory(opinion.getCategory().getName());
                    model.setType(opinion.getType());
                    model.setId(opinion.getId());
                    model.setNotificationId(notificationMongo.getId());
                    model.setStatus(notificationMongo.getReadStatus());
                    model.setUrl(notificationMongo.getUrl());

                    getTimestamp(notificationMongo.getCreatedAt());
                    model.setFormattedAge(DateUtils.getDateOrTimeAgo(getTimestamp(notificationMongo.getCreatedAt())));

                    try {
                        User user = businessDelegate.findUserById(notificationMongo.getUserId());

                        List<UserImage> userImages = user.getUserImages();
                        if (userImages != null && !userImages.isEmpty()) {
                            for (UserImage image : userImages) {

                                if ((image.getImageType()).equalsIgnoreCase(ImageTypeEnum.USERIMAGE.getImageType())) {
                                    UserOutputModel userModel = new UserOutputModel(user.getId(), user.getName(),
                                            "User", null, null, null);
                                    // userModel.setImage(image.getImage());
                                    // String url =
                                    // searchBusinessdelegate.getBaseUrl();
                                    model.setImageUrl(serverPath + "/user/downloadUserImage?userId=" + user.getId()
                                            + "&imageType=" + ImageTypeEnum.USERIMAGE.getImageType() + "&imageSize="
                                            + ImageSizeEnum.THUMBNAIL.getImageSize());
                                    model.setUser(userModel);
                                }

                            }
                        } else {
                            UserOutputModel userModel = new UserOutputModel(user.getId(), user.getName(), "User", null,
                                    null, null);
                            model.setUser(userModel);
                        }
                        opinionOutputModelList.add(model);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return opinionOutputModelList;
    }

    public Timestamp getTimestamp(java.util.Date date) {
        return date == null ? null : new java.sql.Timestamp(date.getTime());
    }

    @Override
    public List<GeneralNotificationOutputModel> getAllGeneralNotificationForUser(Long userId, String componentType)
            throws Exception {

        List<GeneralNotificationOutputModel> generalNotificationModelList = new ArrayList<GeneralNotificationOutputModel>();
        List<NotificationMongo> unreadNotifications = notificationMongodbDAO.findTopNotificationsForUser(userId,
                componentType);

        if (unreadNotifications == null) {

        } else {
            for (NotificationMongo notificationMongo : unreadNotifications) {
                Opinion opinion = new Opinion();
                Response response = new Response();
                Long opinionId = null;
                if (notificationMongo.getComponentType().equals(Constants.OPINION)) {
                    opinion = opinionBusinessDelegate.read(notificationMongo.getComponentId());
                 // TODO : Need to handle the exceptions
                    if(opinion != null){
                        opinionId = opinion.getId();
                    }
                }
                if (notificationMongo.getComponentType().equals(Constants.RESPONSE)) {
                    response = responseBuisnessDelegate.getResponseById(notificationMongo.getComponentId());
                    if (response != null) {                    	
                    	if(response.getOpinion() != null){
                            opinionId = response.getOpinion().getId();
                    	}
                    }
                }

                setRespone(generalNotificationModelList, notificationMongo, opinionId);
            }
        }
        return generalNotificationModelList;
    }

    private void setRespone(List<GeneralNotificationOutputModel> generalNotificationModelList,
            NotificationMongo notificationMongo, Long opinionId) {
        GeneralNotificationOutputModel generalNotificationOutputModel = new GeneralNotificationOutputModel();
        generalNotificationOutputModel.setComponetType(notificationMongo.getComponentType());

        getTimestamp(notificationMongo.getCreatedAt());
        generalNotificationOutputModel.setFormattedAge(DateUtils.getDateOrTimeAgo(getTimestamp(notificationMongo
                .getCreatedAt())));

        generalNotificationOutputModel.setId(opinionId);
        generalNotificationOutputModel.setType(notificationMongo.getNotificationType());
        generalNotificationOutputModel.setTitle(notificationMongo.getMessage());
        generalNotificationOutputModel.setNotificationId(notificationMongo.getId());
        generalNotificationOutputModel.setStatus(notificationMongo.getReadStatus());
        generalNotificationOutputModel.setUrl(notificationMongo.getUrl());

        try {
            User user = businessDelegate.findUserById(notificationMongo.getUserId());
            List<UserImage> userImages = user.getUserImages();
            if (userImages != null && !userImages.isEmpty()) {
                for (UserImage image : userImages) {

                    if ((image.getImageType()).equalsIgnoreCase(ImageTypeEnum.USERIMAGE.getImageType())) {
                        UserOutputModel userModel = new UserOutputModel(user.getId(), user.getName(), "User", null,
                                null, null);
                        // userModel.setImage(image.getImage());
                        // generalNotificationOutputModel.setImageUrl(imageUrl);
                        /* String url = searchBusinessdelegate.getBaseUrl(); */
                        generalNotificationOutputModel.setImageUrl(serverPath + "/user/downloadUserImage?userId="
                                + user.getId() + "&imageType=" + ImageTypeEnum.USERIMAGE.getImageType() + "&imageSize="
                                + ImageSizeEnum.THUMBNAIL.getImageSize());
                        generalNotificationOutputModel.setUser(userModel);
                    }
                }
            } else {
                UserOutputModel userModel = new UserOutputModel(user.getId(), user.getName(), "User", null, null, null);
                generalNotificationOutputModel.setUser(userModel);
            }
            generalNotificationModelList.add(generalNotificationOutputModel);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public boolean deleteByUserId(Long userId) {

        try {
            return notificationMongodbDAO.deleteByUserId(userId);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            // e.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean deleteByComponentIdAndType(Long componentId, String componentType) {

        try {
            return notificationMongodbDAO.deleteByComponentIdAndType(componentId, componentType);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            // e.printStackTrace();
            return false;
        }

    }

}
