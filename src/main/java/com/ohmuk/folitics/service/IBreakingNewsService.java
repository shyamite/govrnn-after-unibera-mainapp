package com.ohmuk.folitics.service;


import java.util.List;
import java.util.Set;

import com.ohmuk.folitics.hibernate.entity.Link;
import com.ohmuk.folitics.hibernate.entity.BreakingNews;
import com.ohmuk.folitics.hibernate.entity.BreakingNewsNews;
//import com.ohmuk.folitics.hibernate.entity.BreakingNewsOpinionStat;
import com.ohmuk.folitics.ouput.model.BreakingNewsOutputModel;

/**
 * @author Asif
 *
 */
public interface IBreakingNewsService extends IBaseService {

	/**
	 * Method is to save {@link BreakingNews}
	 * 
	 * @param breakingnews
	 * @return BreakingNews
	 * @throws Exception
	 */
	BreakingNews save(BreakingNews breakingnews) throws Exception;

	/**
	 * Method is to get {@link BreakingNews} by id
	 * 
	 * @param id
	 * @return BreakingNews
	 * @throws Exception
	 */
	BreakingNews read(Long id) throws Exception;

	/**
	 * Method is to get all {@link BreakingNews}
	 * 
	 * @return List<BreakingNews>
	 * @throws Exception
	 */
	List<BreakingNewsNews> readAll() throws Exception;

	/**
	 * Method is to update breakingnews
	 * 
	 * @param breakingnews
	 * @return BreakingNews
	 * @throws Exception
	 */
	BreakingNews update(BreakingNews breakingnews) throws Exception;

	/**
	 * Method is to soft delete {@link BreakingNews}
	 * 
	 * @param id
	 * @return boolean
	 * @throws Exception
	 */
	boolean delete(Long id) throws Exception;

	/**
	 * Method is to soft delete {@link BreakingNews} by id
	 * 
	 * @param breakingnews
	 * @return boolean
	 * @throws Exception
	 */
	boolean delete(BreakingNews breakingnews) throws Exception;

	/**
	 * Method is to hard delete {@link BreakingNews}
	 * 
	 * @param BreakingNews
	 * @return boolean
	 * @throws Exception
	 */
	boolean deleteFromDB(BreakingNews breakingnews) throws Exception;

	/**
	 * Method is to update {@link BreakingNews} status by id
	 * 
	 * @param id
	 * @param status
	 * @return boolean
	 * @throws Exception
	 */
	boolean updateBreakingNewsStatus(Long id, String status)
			throws Exception;

	/**
	 * Method is to clone {@link BreakingNews}
	 * 
	 * @param breakingnews
	 * @return BreakingNews
	 * @throws Exception
	 */
	BreakingNews clone(BreakingNews breakingnews) throws Exception;

	/**
	 * Method is to get all {@link BreakingNews} not id breakingnewsIds
	 * 
	 * @author Mayank Sharma
	 * @param breakingnewsIds
	 * @return List<BreakingNews>
	 * @throws Exception
	 */
	List<BreakingNews> getAllBreakingNewsNotIn(Set<Long> breakingnewsIds)
			throws Exception;

	/**
	 * Method is to get all sources for {@link BreakingNews}
	 * 
	 * @param breakingnews
	 * @return List<Link>
	 * @throws Exception
	 */
	List<Link> getAllSourcesForBreakingNews(BreakingNews breakingnews)
			throws Exception;

	List<BreakingNews> findByType(String type)throws Exception;
	
	List<BreakingNewsOutputModel> getAllBreakingNewsByMatch(String title) throws Exception;

	List<BreakingNewsOutputModel> getAllBreakingNewsByType(String type) throws Exception;
	
	List<BreakingNewsOutputModel> getAllBreakingNewssByType(String type) throws Exception;

    List<BreakingNews> searchBreakingNews(String searchKeyword);

	List<BreakingNews> findByName(String subject) throws Exception;
	
	BreakingNewsOutputModel findById(String id) throws Exception;

	List<BreakingNews> getAllBreakingNewsByExactMatch(String subject) throws Exception;

	List<BreakingNewsOutputModel> getPaginatedBreakingNewsByType(String type, int firstResult, int maxResults)
			throws Exception;

	List<BreakingNewsOutputModel> getBreakingNewsNameList(String type) throws Exception;
	
	List<BreakingNewsOutputModel> getPaginatedBreakingNewsByStatus(String type, String status,int firstResult, int maxResults)
			throws Exception;

	List<BreakingNewsOutputModel> getBreakingNewsListByStatus(String type, String status) throws Exception;
	
	BreakingNews updateBreakingNewsNews(Long id, List<BreakingNewsNews> breakingnewsNews) throws Exception;
	
	boolean enableDisableBreakingNews(boolean enable, long id) throws Exception;
}

