package com.ohmuk.folitics.service.interfaces;

import com.ohmuk.folitics.hibernate.entity.User;

public interface UserAdminAuthenticateService {

	public User authenticateUser(String userName,String password);
}
