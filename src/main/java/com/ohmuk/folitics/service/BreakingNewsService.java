package com.ohmuk.folitics.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ohmuk.folitics.constants.Constants;
import com.ohmuk.folitics.elasticsearch.service.IESService;
import com.ohmuk.folitics.enums.ComponentState;
import com.ohmuk.folitics.enums.BreakingNewsState;
import com.ohmuk.folitics.hibernate.entity.Link;
import com.ohmuk.folitics.hibernate.entity.BreakingNews;
import com.ohmuk.folitics.hibernate.entity.BreakingNewsNews;
import com.ohmuk.folitics.hibernate.entity.poll.Poll;
import com.ohmuk.folitics.model.ImageModel;
import com.ohmuk.folitics.ouput.model.BreakingNewsOutputModel;
import com.ohmuk.folitics.util.DateUtils;
import com.ohmuk.folitics.util.ElasticSearchUtils;
import com.ohmuk.folitics.util.Serialize_JSON;

/**
 * @author Asif
 *
 */
@Service
@Transactional
public class BreakingNewsService implements IBreakingNewsService {

	@Autowired
	IESService esService;

	private static Logger logger = LoggerFactory.getLogger(BreakingNewsService.class);

	@Autowired
	private SessionFactory _sessionFactory;

	private Session getSession() {
		return _sessionFactory.getCurrentSession();
	}

	@Override
	public BreakingNews save(BreakingNews breakingnews) throws Exception {
		logger.info("Inside BreakingNewsService save method");
		Long id = (Long) getSession().save(breakingnews);
		if (Constants.useElasticSearch)
			esService.save(ElasticSearchUtils.INDEX, ElasticSearchUtils.TYPE_SENTIMENT, String.valueOf(id),
					Serialize_JSON.getJSONString(breakingnews));
		logger.info("Existing from BreakingNewsService save method");
		return read(id);
	}

	@Override
	public BreakingNews read(Long id) throws Exception {
		logger.info("Inside BreakingNewsService read method");
		BreakingNews breakingnews = (BreakingNews) getSession().get(BreakingNews.class, id);
		if (breakingnews != null) {
			
			Hibernate.initialize(breakingnews.getCategories());
			//Hibernate.initialize(breakingnews.getBreakingNewsNews());
		}
		logger.info("Existing from BreakingNewsService read method");
		return breakingnews;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BreakingNewsNews> readAll() throws Exception {
		logger.info("Inside BreakingNewsService readAll method");
		List<BreakingNewsNews> breakingnewss = getSession().createCriteria(BreakingNewsNews.class)
				.add(Restrictions.eq("enabled", true)).addOrder(Order.desc("createTime"))
				.list();
		logger.info("Existing from BreakingNewsService readAll method");
		return breakingnewss;
	}

	@Override
	public BreakingNews updateBreakingNewsNews(Long id, List<BreakingNewsNews> breakingnewsNews) throws Exception {
		logger.info("Inside BreakingNewsService update method");
		BreakingNews breakingnews = read(id);
		if (breakingnews != null) {
			logger.info("Inside BreakingNewsService update method : " + breakingnews.getSubject());

			for (BreakingNewsNews sn : breakingnewsNews) {
				// sn.setBreakingNews(breakingnews);
				logger.info("HTML TextLenth : " + sn.getHtmlText().length() + " Plain text Length :  "
						+ sn.getPlainText().length());
			}
			//breakingnews.getBreakingNewsNews().addAll(breakingnewsNews);
			breakingnews.setEditTime(DateUtils.getSqlTimeStamp());
			getSession().update(breakingnews);
		}
		logger.info("Existing from BreakingNewsService update method");
		return breakingnews;
	}

	@Override
	public BreakingNews update(BreakingNews breakingnews) throws Exception {
		logger.info("Inside BreakingNewsService update method");
		breakingnews.setEditTime(DateUtils.getSqlTimeStamp());
		getSession().update(breakingnews);
		logger.info("Existing from BreakingNewsService update method");
		return breakingnews;
	}

	@Override
	public boolean delete(Long id) throws Exception {
		logger.info("Inside BreakingNewsService delete method");
		BreakingNews breakingnews = read(id);
		if (breakingnews != null) {
			breakingnews.setState(BreakingNewsState.DELETED.getValue());
			getSession().update(breakingnews);
			logger.info("Existing from BreakingNewsService delete method");

			return true;
		} else
			return false;
	}

	@Override
	public boolean delete(BreakingNews breakingnews) throws Exception {
		logger.info("Inside BreakingNewsService delete method");
		if (read(breakingnews.getId()) != null) {
			breakingnews.setState(ComponentState.DELETED.getValue());
			getSession().update(breakingnews);
			logger.info("Existing from BreakingNewsService delete method");
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteFromDB(BreakingNews breakingnews) throws Exception {
		if (breakingnews != null) {
			logger.info("Inside BreakingNewsService deleteFromDB method : " + breakingnews.getId());
			getSession().delete(breakingnews);
			logger.info("Existing from BreakingNewsService deleteFromDB method : ");
			return true;
		} else
			return false;
	}

	@Override
	public boolean updateBreakingNewsStatus(Long id, String status) throws Exception {
		logger.info("Inside BreakingNewsService updateBreakingNewsStatus method");
		BreakingNews breakingnews = (BreakingNews) getSession().get(BreakingNews.class, id);
		logger.debug("breakingnews id: " + breakingnews.getId() + " breakingnews State: " + breakingnews.getState());
		breakingnews.setState(status);
		logger.debug("breakingnews id: " + breakingnews.getId() + " breakingnews State: " + breakingnews.getState());
		getSession().update(breakingnews);
		logger.info("Existing from BreakingNewsService updateBreakingNewsStatus method");
		return true;
	}

	@Override
	public BreakingNews clone(BreakingNews breakingnews) throws Exception {
		logger.info("Inside BreakingNewsService clone method");
		breakingnews = (BreakingNews) getSession().save(breakingnews);
		logger.info("Existing from BreakingNewsService clone method");
		return breakingnews;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<BreakingNews> getAllBreakingNewsNotIn(Set<Long> sId) throws Exception {
		logger.info("Inside BreakingNewsService getAllBreakingNewsNotIn method");
		Criteria criteria = getSession().createCriteria(BreakingNews.class).add(Restrictions.and(
				Restrictions.not(Restrictions.in("id", sId)), Restrictions.eq("state", BreakingNewsState.ALIVE.getValue()))

		);
		List<BreakingNews> breakingnewss = criteria.list();
		logger.info("Existing from BreakingNewsService getAllBreakingNewsNotIn method");
		return breakingnewss;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Link> getAllSourcesForBreakingNews(BreakingNews breakingnews) throws Exception {
		logger.info("Inside BreakingNewsService getAllSourcesForBreakingNews method");
		Criteria criteria = getSession().createCriteria(Link.class)
				.add(Restrictions.eq("state", BreakingNewsState.ALIVE.getValue()));
		List<Link> links = criteria.list();
		logger.info("Existing from BreakingNewsService getAllSourcesForBreakingNews method");
		return links;
	}

	@Override
	public List<BreakingNews> findByType(String type) throws Exception {
		logger.info("Inside BreakingNewsService findbytype method");
		Criteria criteria = getSession().createCriteria(BreakingNews.class);

		criteria.add(Restrictions.and(Restrictions.eq("type", type),
				Restrictions.eq("state", BreakingNewsState.ALIVE.getValue()))).addOrder(Order.desc("createTime"));
		List<BreakingNews> breakingnewss = criteria.list();
		return breakingnewss;

	}

	@Override
	public ImageModel getImageModel(Long entityId, boolean isThumbnail) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ImageModel> getImageModels(String entityIds, boolean isThumbnail) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<BreakingNewsOutputModel> getAllBreakingNewsByMatch(String title) throws Exception {

		logger.info("Inside BreakingNewsSerivce getAllBreakingNewsByMatch method");
		List<BreakingNews> breakingnewss = null;
		List<BreakingNewsOutputModel> models = null;
		try {
			Criteria criteria = getSession().createCriteria(BreakingNews.class)
					.add(Restrictions.and(Restrictions.like("subject", title, MatchMode.START),
							Restrictions.eq("state", BreakingNewsState.ALIVE.getValue())));

			breakingnewss = criteria.list();
			if (null != breakingnewss) {
				models = new ArrayList<BreakingNewsOutputModel>();
				for (BreakingNews breakingnews : breakingnewss) {
					BreakingNewsOutputModel breakingnewsOutputModel = BreakingNewsOutputModel.getModel(breakingnews);
					models.add(breakingnewsOutputModel);
				}
			}

		} catch (Exception exception) {
			logger.error("Exception in getAllBreakingNewsByMatch for title : " + title);
			logger.info("Exiting BreakingNewsSerivce getAllBreakingNewsByMatch method");
		}

		if (null != breakingnewss) {
			logger.debug("Matching breakingnews list with : " + title);
			logger.info("Exiting BreakingNewsSerivce getAllBreakingNewsByMatch method");
			return models;
		}
		logger.info("Exiting BreakingNewsSerivce getAllBreakingNewsByMatch method");
		return null;

	}

	@Override
	public List<BreakingNews> getAllBreakingNewsByExactMatch(String subject) throws Exception {

		logger.info("Inside BreakingNewsSerivce getAllBreakingNewsByExactMatch method");
		List<BreakingNews> breakingnewss = null;
		try {
			Criteria criteria = getSession().createCriteria(BreakingNews.class).add(Restrictions.and(
					Restrictions.eq("subject", subject), Restrictions.eq("state", BreakingNewsState.ALIVE.getValue())));

			breakingnewss = criteria.list();

		} catch (Exception exception) {
			logger.error("Exception in getAllBreakingNewsByExactMatch for subject : " + subject);
			logger.info("Exiting BreakingNewsSerivce getAllBreakingNewsByExactMatch method");
		}
		logger.info("Exiting BreakingNewsSerivce getAllBreakingNewsByExactMatch method");
		return breakingnewss;

	}

	@Override
	public List<BreakingNewsOutputModel> getAllBreakingNewsByType(String type) throws Exception {

		logger.info("Inside BreakingNewsSerivce getAllBreakingNewsByType method");
		List<BreakingNewsOutputModel> models = null;
		List<BreakingNews> breakingnewss = null;
		try {
			Criteria criteria = getSession().createCriteria(BreakingNews.class)
					.add(Restrictions.and(Restrictions.eq("type", type),
							Restrictions.eq("state", BreakingNewsState.ALIVE.getValue())))
					.addOrder(Order.desc("createTime"));
			breakingnewss = criteria.list();
			if (null != breakingnewss) {
				models = new ArrayList<BreakingNewsOutputModel>();
				for (BreakingNews breakingnews : breakingnewss) {
					models.add(BreakingNewsOutputModel.getModel(breakingnews));
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
			logger.error("Exception in getAllBreakingNewsByMatch for title type : " + exception);
			logger.info("Exiting BreakingNewsSerivce getAllBreakingNewsByType method");
		}

		if (null != models) {
			logger.debug("Matching breakingnews list with type : " + type);
			logger.info("Exiting BreakingNewsSerivce getAllBreakingNewsByType method");
			return models;
		}
		logger.info("Exiting BreakingNewsSerivce getAllBreakingNewsByType method");
		return null;

	}
	@Override
	public List<BreakingNewsOutputModel> getAllBreakingNewssByType(String type) throws Exception {

		logger.info("Inside BreakingNewsSerivce getAllBreakingNewsByType method");
		List<BreakingNewsOutputModel> models = null;
		List<BreakingNews> breakingnewss = null;
		try {
			Criteria criteria = getSession().createCriteria(BreakingNews.class)
					.add(Restrictions.and(Restrictions.eq("type", type)))
					.addOrder(Order.desc("createTime"));
			breakingnewss = criteria.list();
			if (null != breakingnewss) {
				models = new ArrayList<BreakingNewsOutputModel>();
				for (BreakingNews breakingnews : breakingnewss) {
					models.add(BreakingNewsOutputModel.getModel(breakingnews));
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
			logger.error("Exception in getAllBreakingNewsByMatch for title type : " + exception);
			logger.info("Exiting BreakingNewsSerivce getAllBreakingNewsByType method");
		}

		if (null != models) {
			logger.debug("Matching breakingnews list with type : " + type);
			logger.info("Exiting BreakingNewsSerivce getAllBreakingNewsByType method");
			return models;
		}
		logger.info("Exiting BreakingNewsSerivce getAllBreakingNewsByType method");
		return null;

	}

	@Override
	public List<BreakingNews> searchBreakingNews(String searchKeyword) {
		logger.info("Inside UserService getUserByMatch method");
		List<BreakingNews> breakingnewsList = null;
		try {
			searchKeyword = "%" + searchKeyword + "%";
			Criteria criteria = getSession().createCriteria(BreakingNews.class)
					.add(Restrictions.and(
							Restrictions.or(Restrictions.like("subject", searchKeyword),
									Restrictions.like("description", searchKeyword)),
							Restrictions.eq("state", BreakingNewsState.ALIVE.getValue())));
			criteria.addOrder(Order.desc("createTime"));
			breakingnewsList = criteria.list();
		} catch (Exception exception) {
			logger.error("Exception in getUserByMatch for match name : " + searchKeyword);
		}

		if (null != breakingnewsList) {
			logger.debug("User list found starting with match name : " + searchKeyword);
			return breakingnewsList;
		}
		return null;
	}

	@Override
	public List<BreakingNews> findByName(String subject) throws Exception {
		logger.info("Inside BreakingNewsService findbytype method");
		List<BreakingNews> breakingnewss = null;
		Criteria criteria = getSession().createCriteria(BreakingNews.class);
		criteria.add(Restrictions.and(Restrictions.eq("subject", subject),
				Restrictions.eq("state", BreakingNewsState.ALIVE.getValue())));
		criteria.addOrder(Order.desc("createTime"));
		breakingnewss = criteria.list();
		return breakingnewss;

	}

	@Override
	public BreakingNewsOutputModel findById(String id) throws Exception {
		logger.info("Inside BreakingNewsService findbytype method");
		BreakingNews breakingnews = null;
		logger.info("Inside BreakingNewsSerivce findById method");
		BreakingNewsOutputModel model = null;
		try {
			breakingnews = getSession().get(BreakingNews.class, Long.parseLong(id));
			if (null != breakingnews) {
				model = BreakingNewsOutputModel.getModel(breakingnews);
			}

		} catch (Exception exception) {
			logger.error("Exception in findById for id : " + id);
			logger.info("Exiting BreakingNewsSerivce findById method");
		}

		if (null != model) {
			logger.info("Exiting BreakingNewsSerivce findById method");
			return model;
		}
		logger.info("Exiting BreakingNewsSerivce findById method");
		return null;

	}

	@Override
	public List<BreakingNewsOutputModel> getPaginatedBreakingNewsByType(String type, int firstResult, int maxResults)
			throws Exception {

		logger.info("Inside BreakingNewsSerivce getPaginatedBreakingNewsByType method");
		List<BreakingNewsOutputModel> models = null;
		List<BreakingNews> breakingnewss = null;
		try {
			Criteria criteria = getSession().createCriteria(BreakingNews.class)
					.add(Restrictions.and(Restrictions.eq("type", type),
							Restrictions.eq("state", BreakingNewsState.ALIVE.getValue())))
					.addOrder(Order.desc("createTime")).setFirstResult(firstResult).setMaxResults(8);
			breakingnewss = criteria.list();
			if (null != breakingnewss) {
				models = new ArrayList<BreakingNewsOutputModel>();
				for (BreakingNews breakingnews : breakingnewss) {
					models.add(BreakingNewsOutputModel.getModel(breakingnews));
				}
			}

		} catch (Exception exception) {
			logger.error("Exception in getPaginatedBreakingNewsByType for title type : " + type);
			logger.info("Exiting BreakingNewsSerivce getPaginatedBreakingNewsByType method");
		}

		if (null != models) {
			logger.debug("Matching breakingnews list with type : " + type);
			logger.info("Exiting BreakingNewsSerivce getPaginatedBreakingNewsByType method");
			return models;
		}
		logger.info("Exiting BreakingNewsSerivce getPaginatedBreakingNewsByType method");
		return null;
	}

	@Override
	public List<BreakingNewsOutputModel> getBreakingNewsNameList(String type) throws Exception {

		logger.info("Inside BreakingNewsSerivce getBreakingNewsNameList method");
		List<BreakingNewsOutputModel> models = null;
		List<BreakingNews> breakingnewss = null;
		try {
			Criteria criteria = getSession().createCriteria(BreakingNews.class).add(Restrictions
					.and(Restrictions.eq("type", type), Restrictions.eq("state", BreakingNewsState.ALIVE.getValue())));
			criteria.addOrder(Order.desc("createTime"));
			breakingnewss = criteria.list();
			if (null != breakingnewss) {
				models = new ArrayList<BreakingNewsOutputModel>();
				for (BreakingNews breakingnews : breakingnewss) {
					models.add(BreakingNewsOutputModel.getIdAndNameModel(breakingnews));
				}
			}

		} catch (Exception exception) {
			logger.error("Exception in getBreakingNewsNameList for title type : " + type);
			logger.info("Exiting BreakingNewsSerivce getBreakingNewsNameList method");
		}

		if (null != models) {
			logger.debug("Matching breakingnews list with type : " + type);
			logger.info("Exiting BreakingNewsSerivce getBreakingNewsNameList method");
			return models;
		}
		logger.info("Exiting BreakingNewsSerivce getBreakingNewsNameList method");
		return null;

	}

	@Override
	public List<BreakingNewsOutputModel> getPaginatedBreakingNewsByStatus(
			String type, String status, int firstResult, int maxResults)
			throws Exception {
		// TODO Auto-generated method stub
		logger.info("Inside BreakingNewsSerivce getPaginatedBreakingNewsByStatus method");
		List<BreakingNewsOutputModel> models = null;
		List<BreakingNews> breakingnewss = null;
		try {
			Criteria criteria = getSession().createCriteria(BreakingNews.class)
					.add(Restrictions.and(Restrictions.eq("type", type),
							Restrictions.eq("state", status)))
					.addOrder(Order.desc("createTime")).setFirstResult(firstResult).setMaxResults(8);
			breakingnewss = criteria.list();
			if (null != breakingnewss) {
				models = new ArrayList<BreakingNewsOutputModel>();
				for (BreakingNews breakingnews : breakingnewss) {
					models.add(BreakingNewsOutputModel.getModel(breakingnews));
				}
			}

		} catch (Exception exception) {
			logger.error("Exception in getPaginatedBreakingNewsByType for title type : " + type);
			logger.info("Exiting BreakingNewsSerivce getPaginatedBreakingNewsByStatus method");
		}

		if (null != models) {
			logger.debug("Matching breakingnews list with type : " + type);
			logger.info("Exiting BreakingNewsSerivce getPaginatedBreakingNewsByStatus method");
			return models;
		}
		logger.info("Exiting BreakingNewsSerivce getPaginatedBreakingNewsByStatus method");
		return null;
	}

	@Override
	public List<BreakingNewsOutputModel> getBreakingNewsListByStatus(
			String type, String status) throws Exception {
		// TODO Auto-generated method stub
		logger.info("Inside BreakingNewsSerivce getBreakingNewsNameListBystatus method");
		List<BreakingNewsOutputModel> models = null;
		List<BreakingNews> breakingnewss = null;
		try {
			Criteria criteria = getSession().createCriteria(BreakingNews.class).add(Restrictions
					.and(Restrictions.eq("type", type), Restrictions.eq("state", status)));
			criteria.addOrder(Order.desc("createTime"));
			breakingnewss = criteria.list();
			if (null != breakingnewss) {
				models = new ArrayList<BreakingNewsOutputModel>();
				for (BreakingNews breakingnews : breakingnewss) {
					models.add(BreakingNewsOutputModel.getIdAndNameModel(breakingnews));
				}
			}

		} catch (Exception exception) {
			logger.error("Exception in getBreakingNewsNameList for title type : " + type);
			logger.info("Exiting BreakingNewsSerivce getBreakingNewsNameListbystatus method");
		}

		if (null != models) {
			logger.debug("Matching breakingnews list with type : " + type);
			logger.info("Exiting BreakingNewsSerivce getBreakingNewsNameListbystatus method");
			return models;
		}
		logger.info("Exiting BreakingNewsSerivce getBreakingNewsNameListbystatus method");
		return null;
	}

	@Override
	public boolean enableDisableBreakingNews(boolean enable, long id)
			throws Exception {
		// TODO Auto-generated method stub
		try {
			BreakingNewsNews breakingNews = getSession().get(BreakingNewsNews.class, id);
			breakingNews.setEnabled(enable);
			getSession().update(breakingNews);
			return true;
		} catch(Exception exception) {
			return false;
		}	
	}

}

