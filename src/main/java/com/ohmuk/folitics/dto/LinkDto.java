package com.ohmuk.folitics.dto;

import java.io.Serializable;

import com.ohmuk.folitics.hibernate.entity.attachment.Attachment;

public class LinkDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private String titleText;
	private String linkText;
	private Attachment attachment;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitleText() {
		return titleText;
	}

	public void setTitleText(String titleText) {
		this.titleText = titleText;
	}

	public String getLinkText() {
		return linkText;
	}

	public void setLinkText(String linkText) {
		this.linkText = linkText;
	}
	
	public Attachment getAttachment() {
		return attachment;
	}

	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}

	@Override
	public String toString() {
		return "LinkDto [titleText=" + titleText + ", linkText=" + linkText + "]";
	}

}
