package com.ohmuk.folitics.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ohmuk.folitics.hibernate.entity.BreakingNewsNews;
import com.ohmuk.folitics.hibernate.entity.SentimentNews;

public class SentimentNewsFeed implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private ArrayList<SentimentNews> sentimentNews;
	
	private ArrayList<BreakingNewsNews> breakingNews;


	public ArrayList<SentimentNews> getSentimentNews() {
		return sentimentNews;
	}


	public void setSentimentNews(ArrayList<SentimentNews> sentimentNews) {
		this.sentimentNews = sentimentNews;
	}


	public ArrayList<BreakingNewsNews> getBreakingNews() {
		return breakingNews;
	}


	public void setBreakingNews(ArrayList<BreakingNewsNews> breakingNews) {
		this.breakingNews = breakingNews;
	}



	@Override
	public String toString() {
		return "SentimentNewsFeed [sentimentNews=" + sentimentNews + "] [breakingNews=" + breakingNews + "]";
	}

}
