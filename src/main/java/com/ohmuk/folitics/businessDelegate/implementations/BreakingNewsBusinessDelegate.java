package com.ohmuk.folitics.businessDelegate.implementations;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ohmuk.folitics.businessDelegate.interfaces.IBreakingNewsBusinessDelegate;
import com.ohmuk.folitics.elasticsearch.service.IESService;
import com.ohmuk.folitics.enums.BreakingNewsState;
import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.Link;
import com.ohmuk.folitics.hibernate.entity.BreakingNews;
import com.ohmuk.folitics.hibernate.entity.BreakingNewsNews;
import com.ohmuk.folitics.mongodb.service.INewsFeedMongodbService;
import com.ohmuk.folitics.ouput.model.BreakingNewsOutputModel;
import com.ohmuk.folitics.service.IPollService;
import com.ohmuk.folitics.util.ElasticSearchUtils;
import com.ohmuk.folitics.service.IBreakingNewsService;
@Component
@Transactional
public class BreakingNewsBusinessDelegate implements IBreakingNewsBusinessDelegate {

	private static Logger logger = LoggerFactory
			.getLogger(BreakingNewsBusinessDelegate.class);

	@Autowired
	private volatile IBreakingNewsService breakingnewsService;

	@Autowired
	private volatile IESService eSService;

	@Autowired
	IPollService pollService;

	@Autowired
	private INewsFeedMongodbService newsFeedService;

	@Override
	public BreakingNews save(BreakingNews breakingnews) throws Exception {
		logger.info("Inside save method in business delegate");
		// List<BreakingNewsNews> list = breakingnews.getBreakingNewsNews();
		//if (list != null) {
			//for (BreakingNewsNews options : list) {
				//options.setBreakingNews(breakingnews);
	//		}
	//	}
		BreakingNews breakingnewsData = breakingnewsService.save(breakingnews);
		logger.info("Exiting save method in business delegate");
		return breakingnewsData;
	}

	@Override
	public BreakingNews read(Long id) throws Exception {
		logger.info("Inside read method in business delegate");
		BreakingNews breakingnewsData = breakingnewsService.read(id);
		logger.info("Exiting read method in business delegate");
		return breakingnewsData;
	}

	@Override
	public List<BreakingNewsNews> readAll() throws Exception {
		logger.info("Inside readAll method in business delegate");
		List<BreakingNewsNews> breakingnewsData = breakingnewsService.readAll();
		logger.info("Exiting readAll method in business delegate");
		return breakingnewsData;
	}

	@Override
	public boolean changeBreakingNewsState(String subject, String breakingnewsType, BreakingNewsState state )
			throws Exception {
		logger.info("Inside changeBreakingNewsState method in business delegate");
		List<BreakingNews> breakingnewss = breakingnewsService.findByName(subject);
		try {
			for (BreakingNews breakingnews : breakingnewss) {				
					breakingnews.setState(state.getValue());
					breakingnewsService.update(breakingnews);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		logger.info("Exiting changeBreakingNewsState method in business delegate");
		return true;
	}

	@Override
	public BreakingNews update(BreakingNews breakingnews) throws Exception {
		logger.info("Inside update method in business delegate");
		BreakingNews breakingnewsData = breakingnewsService.update(breakingnews);
		logger.info("Exiting update method in business delegate");
		return breakingnewsData;
	}

	@Override
	public boolean delete(Long id) throws Exception {
		logger.info("Inside delete method in business delegate");
		boolean sucess = breakingnewsService.delete(id);
		logger.info("Exiting delete method in business delegate");
		return sucess;
	}

	@Override
	public boolean delete(BreakingNews breakingnews) throws Exception {
		logger.info("Inside delete method in business delegate");
		boolean sucess = breakingnewsService.delete(breakingnews);
		logger.info("Exiting delete method in business delegate");
		return sucess;
	}

	@Override
	public boolean deleteFromDB(Long id) throws Exception {
		logger.info("Inside deleteFromDBById method in business delegate");
		BreakingNews breakingnews = read(id);
		/*
		 * for (Poll poll : breakingnews.getPolls()) { poll.setBreakingNews(null);
		 * pollService.update(poll); }
		 */
		boolean sucess = breakingnewsService.deleteFromDB(breakingnews);
		logger.info("Exiting deleteFromDBById method in business delegate");
		return sucess;
	}

	@Override
	public boolean deleteFromDB(BreakingNews breakingnews) throws Exception {
		logger.info("Inside deleteFromDB method in business delegate");
		if (breakingnewsService.read(breakingnews.getId()) != null) {
			boolean sucess = breakingnewsService.deleteFromDB(breakingnews);
			logger.info("Exiting deleteFromDB method in business delegate");
			return sucess;
		}
		return true;
	}

	@Override
	public boolean updateBreakingNewsStatus(Long id, String status)
			throws Exception {
		logger.info("Inside updateBreakingNewsStatus method in business delegate");
		boolean sucess = breakingnewsService.updateBreakingNewsStatus(id, status);
		logger.info("Exiting updateBreakingNewsStatus method in business delegate");
		return sucess;
	}

	@Override
	public BreakingNews clone(BreakingNews breakingnews) throws Exception {
		logger.info("Inside clone method in business delegate");
		breakingnews.setId(null);
		breakingnews.setState("Active");
		BreakingNews breakingnewsData = breakingnewsService.clone(breakingnews);
		logger.info("Exiting clone method in business delegate");
		return breakingnewsData;
	}

	@Override
	public List<BreakingNews> getAllBreakingNewsNotIn(Set<Long> breakingnewsIds)
			throws Exception {
		logger.info("Inside getAllBreakingNewsNotIn method in business delegate");
		List<BreakingNews> breakingnewsData = breakingnewsService
				.getAllBreakingNewsNotIn(breakingnewsIds);
		logger.info("Exiting getAllBreakingNewsNotIn method in business delegate");
		return breakingnewsData;
	}

	@Override
	public List<Link> getAllSourcesForBreakingNews(BreakingNews breakingnews)
			throws Exception {
		logger.info("Inside getAllSourcesForBreakingNews method in business delegate");
		List<Link> links = breakingnewsService
				.getAllSourcesForBreakingNews(breakingnews);
		logger.info("Exiting getAllSourcesForBreakingNews method in business delegate");
		return links;
	}

	@Override
	public Set<Category> getAllIndicator(Long breakingnewsId) throws Exception {
		logger.info("Inside BreakingNewsBusinessDelegate getAllIndicator method");
		BreakingNews breakingnews = breakingnewsService.read(breakingnewsId);
		if (null != breakingnews) {
			Set<Category> indicators = new HashSet<Category>();

			for (Category subCategory : breakingnews.getCategories()) {
				for (Category childCategory : subCategory.getChilds()) {
					Hibernate.initialize(subCategory.getChilds());
					indicators.add(childCategory);
				}
			}
			logger.info("Existing from BreakingNewsBusinessDelegate getAllIndicator method");
			return indicators;
		} else {
			logger.info("Existing from BreakingNewsBusinessDelegate getAllIndicator method");
			return null;
		}
	}

	@Override
	public List<BreakingNews> findByType(String type) throws Exception {
		logger.info("Inside find by type method in business delegate");
		List<BreakingNews> breakingnewsData = breakingnewsService.findByType(type);
		logger.info("Exiting findbytype method in business delegate");
		return breakingnewsData;
	}

	@Override
	public void saveNewsFeed() {

		newsFeedService.saveNewsFeed();

	}

	@Override
	public List<BreakingNewsOutputModel> getAllBreakingNewsByMatch(String title)
			throws Exception {
		logger.info("Inside getAllBreakingNewsByMatch method in business delegate");
		List<BreakingNewsOutputModel> breakingnewss = breakingnewsService
				.getAllBreakingNewsByMatch(title);
		logger.info("Exiting getAllBreakingNewsByMatch method in business delegate");
		return breakingnewss;

	}

	@Override
	public List<BreakingNews> getAllBreakingNewsByExactMatch(String subject)
			throws Exception {
		logger.info("Inside getAllBreakingNewsByMatch method in business delegate");
		List<BreakingNews> breakingnewss = breakingnewsService
				.getAllBreakingNewsByExactMatch(subject);
		logger.info("Exiting getAllBreakingNewsByMatch method in business delegate");
		return breakingnewss;

	}

	@Override
	public List<BreakingNewsOutputModel> getAllBreakingNewsByElasticMatch(String title)
			throws Exception {
		logger.info("Inside getAllBreakingNewsByElasticMatch method in business delegate");
		SearchResponse searchResponse = eSService.search(
				ElasticSearchUtils.INDEX, ElasticSearchUtils.TYPE_SENTIMENT,
				title);

		List<BreakingNewsOutputModel> breakingnewsOutputModels = new ArrayList<BreakingNewsOutputModel>();
		for (SearchHit hit : searchResponse.getHits()) {
			// senList.add(new ObjectMapper().readValue(hit.getSourceAsString(),
			// BreakingNews.class));
			BreakingNews breakingnews = new BreakingNews();
			String sourceAsString = hit.getSourceAsString();
			if (sourceAsString != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(
						DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
						false);
				breakingnews = mapper.readValue(sourceAsString, BreakingNews.class);
			}
			breakingnewsOutputModels.add(BreakingNewsOutputModel.getModel(breakingnews));
		}

		logger.info("Exiting getAllBreakingNewsByMatch method in business delegate");
		return breakingnewsOutputModels;

	}

	@Override
	public List<BreakingNewsOutputModel> getAllBreakingNewsByType(String type)
			throws Exception {
		logger.info("Inside getAllBreakingNewsByType method in business delegate");
		List<BreakingNewsOutputModel> breakingnewsModels = breakingnewsService
				.getAllBreakingNewssByType(type);
		logger.info("Exiting getAllBreakingNewsByType method in business delegate");
		return breakingnewsModels;

	}

	@Override
	public List<BreakingNewsOutputModel> getBreakingNewsNameList(String type)
			throws Exception {
		logger.info("Inside getBreakingNewsNameList method in business delegate");
		List<BreakingNewsOutputModel> breakingnewsModels = breakingnewsService
				.getBreakingNewsNameList(type);
		logger.info("Exiting getBreakingNewsNameList method in business delegate");
		return breakingnewsModels;

	}

	@Override
	public List<BreakingNews> searchBreakingNews(String searchKeyword) {
		List<BreakingNews> breakingnewsList = breakingnewsService
				.searchBreakingNews(searchKeyword);
		return breakingnewsList;
	}

	@Override
	public List<BreakingNews> findByName(String subject) throws Exception {
		logger.info("Inside find by name method in business delegate");
		List<BreakingNews> breakingnewss = breakingnewsService.findByName(subject);
		logger.info("Exiting find by name method in business delegate");
		return breakingnewss;
	}
	
	@Override
	public BreakingNews findById(Long id) throws Exception {
		logger.info("Inside find by id method in business delegate");
		BreakingNews breakingnewsOutput = breakingnewsService.read(id);
		logger.info("Exiting find by id method in business delegate");
		return breakingnewsOutput;
	}

	@Override
	public List<BreakingNewsOutputModel> getPaginatedBreakingNewsByType(String type,
			int from, int to) throws Exception {
		logger.info("Inside getBreakingNewsSetByType method in business delegate");
		List<BreakingNewsOutputModel> breakingnewsModels = breakingnewsService
				.getPaginatedBreakingNewsByType(type, from, to);
		logger.info("Exiting getBreakingNewsSetByType method in business delegate");
		return breakingnewsModels;

	}

	@Override
	public List<BreakingNewsOutputModel> getPaginatedBreakingNewsByStatus(
			String type, String status, int from, int to) throws Exception {
		// TODO Auto-generated method stub
		logger.info("Inside getBreakingNewsSetByStatus method in business delegate");
		List<BreakingNewsOutputModel> breakingnewsModels = breakingnewsService
				.getPaginatedBreakingNewsByStatus(type, status,from, to);
		logger.info("Exiting getBreakingNewsSetByStatus method in business delegate");
		return breakingnewsModels;
	}

	@Override
	public List<BreakingNewsOutputModel> getBreakingNewsListByStatus(
			String type, String status) throws Exception {
		// TODO Auto-generated method stub
		logger.info("Inside getBreakingNewsListByStatus method in business delegate");
		List<BreakingNewsOutputModel> breakingnewsModels = breakingnewsService
				.getBreakingNewsListByStatus(type, status);
		logger.info("Exiting getBreakingNewsListByStatus method in business delegate");
		return breakingnewsModels;
	}

	@Override
	public boolean enableDisableBreakingNews(boolean enable, Long id)
			throws Exception {
		// TODO Auto-generated method stub
		return breakingnewsService.enableDisableBreakingNews(enable, id);
	}
}
