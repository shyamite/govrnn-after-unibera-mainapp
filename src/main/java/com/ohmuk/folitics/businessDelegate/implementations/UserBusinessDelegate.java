package com.ohmuk.folitics.businessDelegate.implementations;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.catalina.connector.Connector;
import org.apache.commons.io.FilenameUtils;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainer;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ohmuk.folitics.businessDelegate.interfaces.IUserBusinessDelegate;
import com.ohmuk.folitics.cache.FoliticsCache;
import com.ohmuk.folitics.cache.SingletonCache;
import com.ohmuk.folitics.component.newsfeed.ImageSizeEnum;
import com.ohmuk.folitics.component.newsfeed.ImageTypeEnum;
import com.ohmuk.folitics.enums.ConnectionStatusType;
import com.ohmuk.folitics.enums.UserStatus;
import com.ohmuk.folitics.enums.UserUINotificationTypes;
import com.ohmuk.folitics.exception.MessageException;
import com.ohmuk.folitics.hibernate.entity.Achievement;
import com.ohmuk.folitics.hibernate.entity.ContactDetails;
import com.ohmuk.folitics.hibernate.entity.Leader;
import com.ohmuk.folitics.hibernate.entity.PoliticalView;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.UserAssociation;
import com.ohmuk.folitics.hibernate.entity.UserConnection;
import com.ohmuk.folitics.hibernate.entity.UserEducation;
import com.ohmuk.folitics.hibernate.entity.UserEmailNotificationSettings;
import com.ohmuk.folitics.hibernate.entity.UserImage;
import com.ohmuk.folitics.hibernate.entity.UserPrivacyData;
import com.ohmuk.folitics.hibernate.entity.UserUINotification;
import com.ohmuk.folitics.hibernate.entity.result.lookup.MaritalStatus;
import com.ohmuk.folitics.hibernate.entity.result.lookup.Qualification;
import com.ohmuk.folitics.hibernate.entity.result.lookup.RegionState;
import com.ohmuk.folitics.hibernate.entity.result.lookup.Religion;
import com.ohmuk.folitics.model.ImageModel;
import com.ohmuk.folitics.model.NotificationModel;
import com.ohmuk.folitics.model.UserChangePassword;
import com.ohmuk.folitics.model.UserPersonalDetail;
import com.ohmuk.folitics.model.UserUINotificationModel;
import com.ohmuk.folitics.notification.InterfaceNotificationService;
import com.ohmuk.folitics.notification.NotificationMapping;
import com.ohmuk.folitics.ouput.model.UserOutputModel;
import com.ohmuk.folitics.service.IUserService;
import com.ohmuk.folitics.service.IUserUINotificationService;
import com.ohmuk.folitics.util.DateUtils;
import com.ohmuk.folitics.util.EmailUtil;
import com.ohmuk.folitics.util.EncryptionUtil;
import com.ohmuk.folitics.util.FoliticsUtils;
import com.ohmuk.folitics.util.ImageUtil;
import com.ohmuk.folitics.util.ThumbnailUtil;

@Component
@Transactional
public class UserBusinessDelegate implements IUserBusinessDelegate {

	private static Logger logger = LoggerFactory
			.getLogger(UserBusinessDelegate.class);

	@Autowired
	private volatile IUserService userService;

	@Autowired
	private volatile IUserUINotificationService userUINotificationService;

	@Autowired
	private volatile InterfaceNotificationService notificationService;
	private AtomicInteger ID_GENERATOR = new AtomicInteger(0);
	// protected List<Notification> appMsgs = new CopyOnWriteArrayList<>();

	private Map<Long, List<String>> userSubscribeMessageMap = new HashMap<>();
	public Map<Long, JedisPubSub> userUnsubscribeMap = new HashMap<>();

	@Autowired
	private EmbeddedWebApplicationContext appContext;

	@Override
	public User create(User user) throws Exception {
		logger.info("Inside  create method in business delegate");

		if (null != user.getUserAssociation()) {
			for (UserAssociation userAssociation : user.getUserAssociation()) {
				userAssociation.setUser(user);
			}
		}

		if (null != user.getUserEducation()) {
			for (UserEducation userEducation : user.getUserEducation()) {
				userEducation.setUser(user);
			}
		}

		if (null != user.getUserEmailNotificationSettings()) {
			for (UserEmailNotificationSettings emailNotificationSetting : user
					.getUserEmailNotificationSettings()) {
				emailNotificationSetting.setUser(user);
			}
		}

		if (null != user.getUserUINotification()) {
			for (UserUINotification userUINotification : user
					.getUserUINotification()) {
				userUINotification.setUser(user);
			}
		}
		User userData = userService.create(user);
		logger.info("Exiting  create method in business delegate");
		return userData;

	}

	@Override
	public User findUserById(Long id) throws Exception {
		logger.info("Inside findUserById method in business delegate");
		User userData = userService.findUserById(id);
		
		logger.info("Exiting findUserById method in business delegate");
		return userData;
	}

	@Override
	public List<User> readAll() throws Exception {
		logger.info("Inside readAll method in business delegate");
		List<User> usersList = userService.readAll();
		for (User user : usersList) {
			Hibernate.initialize(user.getUserEducation());
			Hibernate.initialize(user.getUserImages());
			Hibernate.initialize(user.getReligion());
			//user.setPassword("");
		}
		logger.info("Exiting readAll method in business delegate");
		return usersList;
	}

	@Override
	public List<User> getAllRemainingUsers(Long id) throws Exception {
		logger.info("Inside getAllRemainingUsers method in business delegate");
		List<User> usersList = userService.getAllRemainingUsers(id);
		
		for (User user : usersList) {
			Hibernate.initialize(user.getUserEducation());
			Hibernate.initialize(user.getUserImages());
			//user.setPassword("");
		}

		logger.info("Exiting getAllRemainingUsers method in business delegate");
		return usersList;
	}

	@Override
	public User update(User user) throws Exception {
		logger.info("Inside update method in business delegate");
		User userData = userService.update(user);
		logger.info("Exiting update method in business delegate");
		return userData;
	}

	@Override
	public User updatePersonalDetail(UserPersonalDetail userPersonalDetail)
			throws Exception {
		logger.info("Inside update method in business delegate");
		User user = userService.findUserById(userPersonalDetail.getId());
		user.setName(userPersonalDetail.getName());
		user.setEmailId(userPersonalDetail.getEmailId());
		user.setDob(userPersonalDetail.getDob());
		user.setAddress(userPersonalDetail.getAddress());
		user.setMobileNumber(userPersonalDetail.getMobileNumber());
		user.setMaritalStatus(userPersonalDetail.getMaritalStatus());
		user.setGender(userPersonalDetail.getGender());
		user.setState(userPersonalDetail.getState());
		user.setCity(userPersonalDetail.getCity());
		user.setReligion(userPersonalDetail.getReligion());
		user.setCaste(userPersonalDetail.getCaste());
		user.setQualification(userPersonalDetail.getQualification());
		User userData = userService.update(user);
		logger.info("Exiting update method in business delegate");
		return userData;
	}

	@Override
	public boolean changePassword(UserChangePassword userChangePassword)
			throws Exception {
		logger.info("Inside update method in business delegate");
		User user = userService.findUserById(userChangePassword.getId());
		if (!user.getPassword().equals(EncryptionUtil.encryptBase64(userChangePassword.getCurrentPassword()))) {
			return false;
		} else 
		{
			user.setPassword(EncryptionUtil.encryptBase64(userChangePassword.getNewPassword()));
			User userData = userService.update(user);
			if (userData == null)
				return false;
		}
		logger.info("Exiting update method in business delegate");
		return true;
	}

	@Override
	public boolean delete(Long id) throws Exception {
		logger.info("Inside delete method in business delegate");
		boolean sucess = userService.delete(id);
		logger.info("Exiting delete method in business delegate");
		return sucess;
	}

	@Override
	public boolean delete(User user) throws Exception {
		logger.info("Inside delete method in business delegate");
		boolean sucess = userService.delete(user);
		logger.info("Exiting delete method in business delegate");
		return sucess;
	}

	@Override
	public boolean deleteFromDB(Long id) throws Exception {
		logger.info("Inside deleteFromDB method in business delegate");
		boolean sucess = userService.deleteFromDB(id);
		notificationService.deleteNotificationByUserId(id);
		notificationService.deleteNotificationMappingsByUserId(id);
		logger.info("Exiting deleteFromDB method in business delegate");
		return sucess;
	}

	@Override
	public boolean deleteFromDB(User user) throws Exception {
		logger.info("Inside deleteFromDB method in business delegate");
		boolean sucess = userService.deleteFromDB(user);
		logger.info("Exiting deleteFromDB method in business delegate");
		return sucess;
	}

	@Override
	public User resetPassword(Long id) throws Exception {
		logger.info("Inside  resetPassword method in business delegate");
		User user = findUserById(id);
		String newPassword = generateRandomPassword();
		user.setPassword(EncryptionUtil.encryptBase64(newPassword));
		User userWithNewPassword = userService.resetPassword(user);
		String subject = "Your password has been reset for Govrnn";
		if (userWithNewPassword != null) {
			byte[] image = FoliticsUtils.getImageBytes(userWithNewPassword);
			String text = MessageFormat.format(
					EmailUtil.body_for_reset_passowrd, user.getName(),
					user.getUsername(), newPassword);
			EmailUtil.sendEmail(userWithNewPassword.getEmailId(), "", "",
					subject, text, image, "image/jpg");
			logger.info("Exiting  resetPassword method in business delegate");
			return userWithNewPassword;
		}
		logger.info("Exiting from business  delegate resetPassword method");
		return null;
	}

	public String generateRandomPassword() {
		final String characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		final Random rnd = new Random();
		StringBuilder sb = new StringBuilder(8);
		for (int i = 0; i < 8; i++)
			sb.append(characters.charAt(rnd.nextInt(characters.length())));
		return sb.toString();
	}

	@Override
	public ImageModel getImageModel(Long id, boolean isThumbnail)
			throws Exception {
		User u = findUserById(id);
		ImageModel imageModel = getImageModel(u, isThumbnail);
		return imageModel;
	}

	private ImageModel getImageModel(User u, boolean isThumbnail) {
		ImageModel imageModel = new ImageModel();
		if (u != null) {
		}
		return imageModel;
	}

	public List<ImageModel> getImageModels(String entityIds, boolean isThumbnail)
			throws Exception {
		logger.info("Inside getImageModels method in business delegate");
		List<ImageModel> imageModel = userService.getImageModels(entityIds,
				isThumbnail);
		logger.info("Exiting getImageModels method in business delegate");
		return imageModel;
	}

	@Override
	public User uploadImage(MultipartFile file, Long userId, String imageType)
			throws Exception {
		logger.info("Inside uploadImage  method in business delegate");
		User user = findUserById(userId);
		if (user != null) {
			String fileName = null;
			if (!file.isEmpty()) {
				try {
					UserImage userOrAlbumImage = new UserImage();
					String ext = FilenameUtils.getExtension(file
							.getOriginalFilename());
					logger.info("imageType: " + imageType);
					logger.info("File extension : " + ext);
					userOrAlbumImage.setFileType(ext);
					userOrAlbumImage.setUser(user);
					try {
						userOrAlbumImage.setImage(ThumbnailUtil.getUserImage(
								file.getBytes(), ext)); 
					} catch (Exception e) { 
						// if exception then set original
						userOrAlbumImage.setImage(file.getBytes());
					}
					userOrAlbumImage.setImageType(ImageTypeEnum
							.getImageTypeEnum(imageType).getImageType());
					ImageUtil.addUserImage(
							ImageTypeEnum.getImageTypeEnum(imageType)
									.getImageType(), user.getUserImages(),
							userOrAlbumImage);

					User userData = userService.update(user);
					logger.info("Exiting uploadImage  method in business delegate");
					return userData;

				} catch (Exception e) {
					logger.error(
							"You failed to upload " + fileName + ": "
									+ e.getMessage(), e);
				}
			} else {
				logger.debug("Unable to upload. File is empty.");
			}
		}
		return null;
	}

	@Override
	public User findByUsername(String username) throws Exception {
		logger.info("Inside findByUsername method in business delegate");
		User user = userService.findByUsername(username);
		if (user != null) {
			Hibernate.initialize(user.getUserImages());
		}
		logger.info("Exiting findByUsername method in business delegate");
		return user;
	}


	@Override
	public User getUserFromSession(HttpServletRequest request) throws Exception {
		logger.info("Inside userService getUserFromSession method");
		HttpSession httpSession = request.getSession();
		String userName = (String) httpSession.getAttribute("userName");
		System.out.println("username :" + httpSession.getAttribute("userName"));
		if (null != userName) {
			logger.debug("User is found with username: " + userName);
			User user = userService.findByUsername(userName);
			Hibernate.initialize(user.getUserImages());
			logger.info("Exiting from userService getUserFromSession method");
			return user;
		}
		logger.info("Exiting from userService getUserFromSession method");
		return null;
	}

	@Override
	public Long addConnection(Long userId, Long connecionId) throws Exception {
		logger.info("Inside UserBusinessDelegate addConnection method");
		User user = userService.findUserById(userId);
		User connection = userService.findUserById(connecionId);
		if (null != user && null != connection) {
			logger.debug("User and connection found with " + userId + " and"
					+ connecionId);
			UserConnection userConnection = new UserConnection();
			userConnection.setUserId(userId);
			userConnection.setConnectionStatus(ConnectionStatusType.PENDING
					.getValue());
			userConnection.setConnectionId(connecionId);

			Long connectionId = userService.addUserConnection(userConnection);
			if (connectionId != null) {
				NotificationMapping notificationMapping = new NotificationMapping();
				notificationMapping.setAction("connection");
				notificationMapping.setUserId(userId);
				notificationService.connectionNotification(notificationMapping);

			}
			logger.info("Exiting from UserBusinessDelegate addConnection method");
			return connectionId;
		}

		logger.debug("User and connection are not found");
		logger.info("Exiting from UserBusinessDelegate addConnection method");
		return null;
	}

	@Override
	public boolean deleteConnection(Long userId, Long connectionId)
			throws Exception {
		logger.info("Inside UserBusinessDelegate deleteConnection method");
		User user = userService.findUserById(userId);
		if (null != user) {
			logger.debug("User is found with id: " + userId);
			UserConnection userConnection = userService
					.findUserConnectionByUserAndConnectionId(userId,
							connectionId);
			userService.deleteUserConnection(userConnection);
			logger.info("Exiting from UserBusinessDelegate deleteConnection method");
			return true;
		}
		logger.info("Exiting from UserBusinessDelegate deleteConnection method");
		return false;
	}

	@Override
	public boolean updateConnectionStatus(Long userId, Long connectionId,
			String status) throws Exception {
		logger.info("Inside UserBusinessDelegate updateConnectionStatus method");
		if (null != userId & null != connectionId & null != status) {
			userService.updateConnectionStatus(userId, connectionId, status);
			logger.info("Exiting from UserBusinessDelegate updateConnectionStatus method");
			return true;
		}
		logger.info("Exiting from UserBusinessDelegate updateConnectionStatus method");
		return false;
	}

	@Override
	public List<User> getRequestConnection(Long connectionId, String status)
			throws Exception {
		logger.info("Inside UserBusinessDelegategetRequestConnection method");
		List<User> users = userService.getRequestConnection(connectionId,
				status);
		for (User user : users) {

			Hibernate.initialize(user.getUserImages());
		}

		logger.info("Exiting from UserBusinessDelegate getAllConnection method");
		return users;
	}

	@Override
	public List<User> getAllConnection(Long userId) throws Exception {
		logger.info("Inside UserBusinessDelegate getAllConnection method");
		List<User> users = userService.getAllConnection(userId);
		for (User user : users) {

			Hibernate.initialize(user.getUserImages());
		}
		logger.info("Exiting from UserBusinessDelegate getAllConnection method");
		return users;
	}

	@Override
	public Long saveUserUINotification(Long userId,
			UserUINotification userUINotification) throws Exception {
		logger.info("Inside UserBusinessDelegate saveUserUINotification method");
		User user = userService.findUserById(userId);
		userUINotification.setUser(user);
		Long uINotificationId = userService
				.saveUINotification(userUINotification);
		logger.info("Exiting from UserBusinessDelegate saveUserUINotification method");
		return uINotificationId;
	}

	@Override
	public boolean updateUserUINotification(
			UserUINotification userUINotification) throws Exception {
		logger.info("Inside UserBusinessDelegate updateUserUINotification method");
		boolean status = userService.updateUINotification(userUINotification);
		logger.info("Exiting from UserBusinessDelegate updateUserUINotification method");
		return status;
	}

	@Override
	public boolean blockUser(Long userId, Long blockUserId) throws Exception {
		logger.info("Inside UserBusinessDelegate blockUser method");
		boolean status = userService.blockUser(userId, blockUserId);
		logger.info("Exiting from UserBusinessDelegate blockUser method");
		return status;
	}

	@Override
	public boolean unBlockUser(Long userId, Long blockUserId) throws Exception {
		logger.info("Inside UserBusinessDelegate unBlockUser method");
		boolean status = userService.unBlockUser(userId, blockUserId);
		logger.info("Exiting from UserBusinessDelegate unBlockUser method");
		return status;
	}

	@Override
	public Long saveUserEmailNotificationSettings(Long userId,
			UserEmailNotificationSettings userEmailNotificationSettings)
			throws Exception {
		logger.info("Inside UserBusinessDelegate saveUserEmailNotificationSettings method");
		User user = userService.findUserById(userId);
		userEmailNotificationSettings.setUser(user);
		Long userENId = userService
				.saveUserEmailNotificationSettings(userEmailNotificationSettings);
		logger.info("Exiting from UserBusinessDelegate saveUserEmailNotificationSettings method");
		return userENId;
	}

	@Override
	public boolean updateUserEmailNotificationSettings(
			UserEmailNotificationSettings userEmailNotificationSettings)
			throws Exception {
		logger.info("Inside UserBusinessDelegate UpdateUserEmailNotificationSettings method");
		boolean status = userService
				.updateUserEmailNotificationSettings(userEmailNotificationSettings);
		logger.info("Exiting from UserBusinessDelegate UpdateUserEmailNotificationSettings method");
		return status;
	}

	@Override
	public List<UserEmailNotificationSettings> getAllUserEmailNotificationSettings(
			Long userId) throws Exception {
		logger.info("Inside UserBusinessDelegate getAllUserEmailNotificationSettings method");
		List<UserEmailNotificationSettings> users = userService
				.getAllUserEmailNotificationSettings(userId);
		logger.info("Exiting from UserBusinessDelegate getAllUserEmailNotificationSettings method");
		return users;
	}

	@Override
	public List<UserUINotification> getAllUserUINotification(Long userId)
			throws Exception {
		logger.info("Inside UserBusinessDelegate getAllUserUINotification method");
		List<UserUINotification> userUINotifications = userService
				.getAllUserUINotification(userId);
		logger.info("Exiting from UserBusinessDelegate getAllUserUINotification method");
		return userUINotifications;
	}

	@Override
	public Long saveUserPrivacySettings(Long userId,
			UserPrivacyData userPrivacySettings) throws Exception {
		logger.info("Inside UserBusinessDelegate saveUserPrivacySetting method");
		userPrivacySettings.setUserId(userId);
		Long userPSId = userService
				.saveUserPrivacySettings(userPrivacySettings);
		logger.info("Exiting from UserBusinessDelegate saveUserPrivacySetting method");
		return userPSId;
	}

	@Override
	public boolean updateUserPrivacySetting(UserPrivacyData userPrivacySettings)
			throws Exception {
		logger.info("Inside UserBusinessDelegate updateUserPrivacySetting method");
		boolean status = userService
				.updateUserPrivacySetting(userPrivacySettings);
		logger.info("Exiting from UserBusinessDelegate updateUserPrivacySetting method");
		return status;
	}

	@Override
	public List<UserPrivacyData> getAllUserPrivacySettings(Long userId)
			throws Exception {
		logger.info("Inside UserBusinessDelegate getAllUsePrivacySettings method");
		List<UserPrivacyData> userPrivacySettings;
		userPrivacySettings = userService.getAllUsePrivacySettings(userId);
		logger.info("Exiting from UserBusinessDelegate getAllUsePrivacySettings method");
		return userPrivacySettings;
	}

	@SuppressWarnings("unchecked")
	@Override
	public User save(User user) throws Exception {
		System.out.println("Inside deligate");
		user = create(user);
		// Map<String, Object> map = new
		// ObjectMapper().convertValue(user.getUserprofile(), Map.class);
		Map<String, Object> map = new ObjectMapper().convertValue(user,
				Map.class);
		if (null != map) {
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				UserPrivacyData userPrivacySettings = new UserPrivacyData();
				userPrivacySettings.setNotificationType("All");
				userPrivacySettings.setUserDataField(entry.getKey());
				if (null != entry.getValue()) {
					userPrivacySettings.setUserDataValue(entry.getValue()
							.toString());
				} else {
					userPrivacySettings.setUserDataValue("null");
				}
				userPrivacySettings.setUserId(user.getId());
				userService.saveUserPrivacySettings(userPrivacySettings);
			}
		}

		UserUINotification genNotification = new UserUINotification(
				UserUINotificationTypes.GENERAL.getValue(), true);
		genNotification.setUser(user);
		userUINotificationService.save(genNotification);

		UserUINotification opiNotification = new UserUINotification(
				UserUINotificationTypes.OPINION.getValue(), true);
		opiNotification.setUser(user);
		userUINotificationService.save(opiNotification);
		return user;
	}

	@Override
	public List<UserPrivacyData> viewUserPrivacyData(Long viewerId, Long userId)
			throws Exception {
		String viewerType = friendFoeAggrigation(viewerId, userId);
		List<UserPrivacyData> userPrivacyDatas = userService.getUserProfile(
				userId, viewerType);
		return userPrivacyDatas;
	}

	@Override
	public String friendFoeAggrigation(Long viewerId, Long userId)
			throws Exception {
		return "Friends";
	}

	@Override
	public Map<String, List<User>> getAntiProConnection(Long userId)
			throws Exception {
		logger.info("Inside UserBusinessDelegate getantiProConnection method");
		Map<String, List<User>> map = (Map<String, List<User>>) userService
				.getAntiProConnection(userId);

		logger.info("Exiting from UserBusinessDelegate getAntiProConnection method");
		return map;
	}

	@Override
	public JedisPubSub subscribe(Long userId) throws MessageException,
			Exception {

		logger.info("Inside subscribe method in userBusinessDelegate");
		if (userId == null) {
			logger.error("UserId can't be null");
			throw new MessageException("UserId can't be null");

		}
		logger.debug("Redis subscribe for userId:-" + userId);

		JedisPubSub jedisPubSub = setupSubscriber(userId);
		userUnsubscribeMap.put(userId, jedisPubSub);

		logger.info("Exiting subscribe method from userBusinessDelegate");
		return jedisPubSub;

	}

	private JedisPubSub setupSubscriber(Long userId) {

		final JedisPubSub jedisPubSub = new JedisPubSub() {
			@Override
			public void onUnsubscribe(String channel, int subscribedChannels) {

			}

			@Override
			public void onSubscribe(String channel, int subscribedChannels) {

			}

			@Override
			public void onPUnsubscribe(String pattern, int subscribedChannels) {

			}

			@Override
			public void onPSubscribe(String pattern, int subscribedChannels) {

			}

			@Override
			public void onPMessage(String pattern, String channel,
					String message) {
				// messageContainer.add(message);
				// Integer id = ID_GENERATOR.getAndIncrement();
				// Notification notification = new Notification(id, message);
				List<String> messages = new ArrayList<String>();
				messages.add(message);
				userSubscribeMessageMap.put(userId, messages);

				logger.info("Added new message: {}" + message);

				// appMsgs.add(notification);

				log("Message received - " + message);
				// messageReceivedLatch.countDown();

			}

			@Override
			public void onMessage(String channel, String message) {

			}
		};
		new Thread(new Runnable() {
			public void run() {
				try {
					log("Connecting");
					Jedis jedis = new Jedis("localhost", 6379);
					log("subscribing");
					String pattern = "*" + userId + "*";
					jedis.psubscribe(jedisPubSub, pattern);
					log("subscribe returned, closing down");
					jedis.quit();
				} catch (Exception e) {
					log(">>> OH NOES Sub - " + e.getMessage());
					// e.printStackTrace();
				}
			}
		}, "subscriberThread").start();
		return jedisPubSub;
	}

	static final long startMillis = System.currentTimeMillis();

	private static void log(String string, Object... args) {
		long millisSinceStart = System.currentTimeMillis() - startMillis;
		System.out.printf("%20s %6d %s\n", Thread.currentThread().getName(),
				millisSinceStart, String.format(string, args));
	}

	public List<String> readNotifications(Long userId) throws Exception {

		logger.info("Inside readNotifications method in userBusinessDelegate");
		logger.debug("Notifications fetch for userId:-" + userId);

		List<String> messages = userSubscribeMessageMap.get(userId);
		userSubscribeMessageMap.remove(userId);

		logger.info("Exiting readNotifications method from userBusinessDelegate");
		return messages;
		// return userMap;

		/*
		 * int size = this.appMsgs.size(); logger.info(
		 * "Reading messages from {} - {}", startId, size); List<Notification>
		 * temp = Lists.newArrayList(this.appMsgs.subList( startId, size));
		 * Collections.sort(temp); return temp;
		 */
	}

	@Override
	public void unSubscribe(Long userId) throws MessageException, Exception {

		logger.info("Inside unSubscribe method in userBusinessDelegate");
		if (userId == null) {
			logger.error("UserId can't be null");
			throw new MessageException("UserId can't be null");

		}
		logger.debug("Redis unSubscribe for userId:-" + userId);
		JedisPubSub jedisPubSub = userUnsubscribeMap.get(userId);
		jedisPubSub.unsubscribe();
		logger.info("Exiting unSubscribe method in userBusinessDelegate");

	}

	@Override
	public List<Religion> getAllReligion() {
		// TODO Auto-generated method stub
		return userService.getReligion();
	}

	@Override
	public List<RegionState> getAllState() throws Exception {
		// TODO Auto-generated method stub
		return userService.getState();
	}

	@Override
	public List<MaritalStatus> getAllMaritalStatus() throws Exception {
		// TODO Auto-generated method stub
		return userService.getMaritalStatus();
	}

	@Override
	public List<Qualification> getAllQualification() throws Exception {
		// TODO Auto-generated method stub
		return userService.getQualification();
	}

	@Override
	public UserConnection connections(Long userId, Long connecionId)
			throws Exception {
		logger.info("Inside findUserById method in business delegate");
		UserConnection userConnection = userService.connections(userId,
				connecionId);
		logger.info("Exiting findUserById method in business delegate");
		return userConnection;
	}

	@Override
	public boolean verifyIfUsernameExist(String username) throws Exception {
		logger.info("Inside verifyIfUsernameExist method in business delegate");
		boolean result = userService.verifyIfUsernameExist(username);
		logger.info("Exiting verifyIfUsernameExist method in business delegate");
		return result;
	}


	@Override
	public boolean verifyIfUserEmailExist(String email) throws Exception {
		logger.info("Inside verifyIfUserEmailExist method in business delegate");
		boolean result = userService.verifyIfUserEmailExist(email);
		logger.info("Exiting verifyIfUserEmailExist method in business delegate");
		return result;
	}
	@Override
	public Long add(Achievement achievement) throws Exception {
		logger.info("Inside add achievement method in business delegate");
		Long id = userService.add(achievement);
		logger.info("Exiting add achievement method in business delegate");
		return id;
	}

	@Override
	public Long add(Leader leader) throws Exception {
		logger.info("Inside add leader method in business delegate");
		Long id = userService.add(leader);
		logger.info("Exiting add leader method in business delegate");
		return id;
	}

	@Override
	public Long add(PoliticalView politicalView) throws Exception {
		logger.info("Inside add politicalView method in business delegate");
		Long id = userService.add(politicalView);
		logger.info("Exiting add politicalView method in business delegate");
		return id;
	}

	@Override
	public List<Achievement> getAchievement(Long userId) throws Exception {
		logger.info("Inside add achievement method in business delegate");
		List<Achievement> achievement = userService.getAchievement(userId);
		logger.info("Exiting add achievement method in business delegate");
		return achievement;
	}

	@Override
	public List<Leader> getLeader(Long userId) throws Exception {
		logger.info("Inside add leader method in business delegate");
		List<Leader> leader = userService.getLeader(userId);
		logger.info("Exiting add leader method in business delegate");
		return leader;
	}

	@Override
	public List<PoliticalView> getPoliticalView(Long userId) throws Exception {
		logger.info("Inside add politicalView method in business delegate");
		List<PoliticalView> politicalView = userService
				.getPoliticalView(userId);
		logger.info("Exiting add politicalView method in business delegate");
		return politicalView;
	}

	@Override
	public Long saveUserContactDetailsSettings(Long userId,
			ContactDetails contactDetails) throws Exception {
		logger.info("Inside UserBusinessDelegate saveUserContactDetailsSettings method");
		User user = userService.findUserById(userId);
		contactDetails.setUser(user);
		Long userCDId = userService
				.saveUserContactDetailsSettings(contactDetails);
		logger.info("Exiting from UserBusinessDelegate saveUserUINotification method");
		return userCDId;
	}

	@Override
	public boolean updateUserContactDetailsSettings(
			ContactDetails contactDetails) throws Exception {
		logger.info("Inside UserBusinessDelegate updateUserContactDetailsSettings method");
		boolean status = userService
				.updateUserContactDetailsSettings(contactDetails);
		logger.info("Exiting from UserBusinessDelegate updateUserContactDetailsSettings method");
		return status;
	}

	@Override
	public User authenticateUser(String userName, String password)
			throws Exception {
		logger.info("Inside  authenticateUser method in business delegate userName : "+userName+ " password : "+EncryptionUtil.encryptBase64(password));
		List<User> users = findByUserNameOrEmail(userName);
		if (null != users && !users.isEmpty()) {
			for (User user : users) {
				logger.info("password : "+user.getPassword());
				// has to be changed during signup
				//if (user.getPassword().equals(EncryptionUtil.encryptBase64(password))
				if (user.getPassword().equals(EncryptionUtil.encryptBase64(password))
						&& (user.getUsername().equals(userName) || user
								.getEmailId().equals(userName))) {
					Hibernate.initialize(user.getUserImages());
					return user;
				}
			}
		}
		logger.info("Exiting  authenticateUser method in business delegate");
		return null;
	}

	@Override
	public List<User> findByUserNameOrEmail(String userName) throws Exception {
		logger.info("Inside findByUsernameOrEmail method in business delegate");
		List<User> users = userService.findByUserNameorEmail(userName);
		logger.info("Exiting findByUsernameOrEmail method in business delegate");
		return users;
	}

	public boolean passwordMatch() {
		// TODO : once encryption is in place write the functionality
		return true;
	}

	@Override
	public boolean sentWelcomeEmail(User user, String emailValidationUrl)
			throws Exception {
		logger.info("Inside welcomeEmail method in business delegate");
		String subject = "Welcome To Govrnn!";
		if (null != user) {
			if (StringUtils.isEmpty(user.getEmailId())) {
				return false;
			}
			byte[] image = FoliticsUtils.getImageBytes(user);
			String text = MessageFormat.format(EmailUtil.body_welcome_email,
					user.getName(), user.getUsername(), EncryptionUtil.decryptBase64(user.getPassword()),
					emailValidationUrl);
			EmailUtil.sendEmail(user.getEmailId(), "", "", subject, text,
					image, "image/jpg");
			logger.info("Exiting  forgotPassword method in business delegate");
			return true;
		}
		logger.info("Exiting forgotPassword method in business delegate");
		return false;
	}

	@Override
	public boolean forgotPassword(String emailId) throws Exception {
		logger.info("Inside forgotPassword method in business delegate");
		// only one user exists for given email
		List<User> users = userService.findByUserNameorEmail(emailId);
		// User user = (null != users && !users.isEmpty()) ? userService
		// .findByUserNameorEmail(emailId).get(0) : null;
		if (users != null) {
			for (User user : users) {
				if (user.getStatus().equals("Deleted")) {
					continue;
				}
				String subject = "Your login information for Govrnn";
				if (null != user) {
					String newPassword = generateRandomPassword();
					user.setPassword(EncryptionUtil.encryptBase64(newPassword));
					User userWithNewPassword = userService.resetPassword(user);
					if (userWithNewPassword != null) {

						byte[] image = FoliticsUtils
								.getImageBytes(userWithNewPassword);
						// send email
						String text = MessageFormat.format(
								EmailUtil.body_for_forgot_passowrd,
								userWithNewPassword.getName(),
								userWithNewPassword.getUsername(),
								newPassword);

						EmailUtil.sendEmail(userWithNewPassword.getEmailId(),
								"", "", subject, text, image, "image/jpg");
					}
				}
				logger.info("Exiting  forgotPassword method in business delegate");
				return true;
			}

			logger.info("Exiting forgotPassword method in business delegate");
			return false;
		}
		logger.info("Exiting forgotPassword method in business delegate");
		return false;
	}

	@Override
	public boolean forgotUserName(String emailId) throws Exception {
		logger.info("Inside forgotUserName method in business delegate");
		// only one user exists for given email
		List<User> users = userService.findByUserNameorEmail(emailId);
		// User user = (null != users && !users.isEmpty()) ? userService
		// .findByUserNameorEmail(emailId).get(0) : null;
		// if (user==null || user.getStatus().equals("Deleted")) {
		// return false;
		// }
		if (users != null) {
			for (User user : users) {
				if (user.getStatus().equals("Deleted")) {
					continue;
				}
				String subject = "Your login information for Govrnn";
				if (null != user) {
					// send email
					byte[] image = FoliticsUtils.getImageBytes(user);
					String text = MessageFormat.format(
							EmailUtil.body_for_forgot_username, user.getName(),
							user.getUsername());
					EmailUtil.sendEmail(user.getEmailId(), "", "", subject,
							text, image, "image/jpg");
				}
			}
			logger.info("Exiting  forgotUserName method in business delegate");
			return true;
		}
		logger.info("Exiting from business delegate resetPassword forgotUserName");
		return false;
	}

	@Override
	public List<UserOutputModel> getUserByMatch(String match) throws Exception {
		logger.info("Inside getUserAsLike method in business delegate");
		List<UserOutputModel> usersList = userService.getUserByMatch(match);
		logger.info("Exiting getUserAsLike method in business delegate");
		return usersList;
	}

	@Override
	public boolean updateUserUINotificationSettings(
			UserUINotificationModel userUINotificationModel) {
		User user;
		try {
			user = userService
					.findUserById(userUINotificationModel.getUserId());
			for (NotificationModel notification : userUINotificationModel
					.getNotifications()) {
				UserUINotification userUINotification = null;
				userUINotification = userUINotificationService.read(user,
						notification.getNotificationType());
				if (userUINotification != null) {
					userUINotification.setEnabled(notification.getEnabled());
					userUINotificationService.update(userUINotification);
				} else {
					userUINotification = new UserUINotification();
					userUINotification.setEnabled(notification.getEnabled());
					userUINotification.setNotificationType(notification
							.getNotificationType());
					userUINotification.setUser(user);
					userUINotificationService.save(userUINotification);
				}
			}
			logger.info("Exiting  updateUserUINotificationSettings method in business delegate");
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("Exiting updateUserUINotificationSettings method in business delegate with exception "
					+ e.getMessage());
			return false;
		}
	}

	@Override
	public User deactivateUserAccount(Long userId) {
		User user;
		try {
			user = userService.findUserById(userId);
			user.setStatus(UserStatus.DELETED.getValue());
			user = userService.update(user);
			logger.info("Exiting  deactivateUserAccount method in business delegate");
			return user;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("Exiting deactivateUserAccount method in business delegate with exception "
					+ e.getMessage());
			return null;
		}
	}

	@Override
	public List<User> findByUserNameOrName(String searchKeyword) {
		logger.info("Inside findByUsernameOrEmail method in business delegate");
		List<User> users = userService.findByUserNameorName(searchKeyword);
		logger.info("Exiting findByUsernameOrEmail method in business delegate");

		return users;
	}

	@Override
	public List<UserOutputModel> getLatestUsers(int count) {
		logger.info("Inside getLatestUsers method in business delegate");
		List<UserOutputModel> userOutputModels = new ArrayList<UserOutputModel>();
		try {
			List<User> users = userService.getLatestUsers(count);
			for (User user : users) {
				userOutputModels.add(UserOutputModel.getModel(user));
			}
			return userOutputModels;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("Exiting getLatestUsers method in business delegate");

		return null;
	}

	@Override
	public List<SearchOutputModel> setSearchOutputModel(Long userId,
			List<User> users, List<SearchOutputModel> searchOutputModels)
			throws Exception {
		for (User user : users) {
			SearchOutputModel searchOutputModel = new SearchOutputModel();
			searchOutputModel.setComponentType("User");
			searchOutputModel.setId(user.getId());

			String url = getBaseUrl();

			searchOutputModel.setImageUrl(url
					+ "/user/downloadUserImage?userId=" + user.getId()
					+ "&imageType=" + ImageTypeEnum.USERIMAGE.getImageType()
					+ "&imageSize=" + ImageSizeEnum.THUMBNAIL.getImageSize());
			searchOutputModel.setTitle(user.getName());
			UserImage userImage = ImageUtil.getUserImage(
					ImageTypeEnum.USERIMAGE.getImageType(),
					user.getUserImages());
			if (userImage != null)
				searchOutputModel.setImage(userImage.getImage());
			else
				searchOutputModel.setImage((byte[]) SingletonCache
						.getInstance().get(FoliticsCache.KEYS.USER_IMAGE));
			// userImage
			try {
				User dbUser = findUserById(user.getId());
				List<UserImage> userImages = user.getUserImages();
				if (userImages != null && !userImages.isEmpty()) {
					for (UserImage image : userImages) {

						if ((image.getImageType())
								.equalsIgnoreCase(ImageTypeEnum.USERIMAGE
										.getImageType())) {
							UserOutputModel userModel = new UserOutputModel(
									user.getId(), user.getName(), "User", null,
									null, null);
							userModel.setImage(image.getImage());
							searchOutputModel.setImage(image.getImage());
						}
					}
				} else {
					UserOutputModel userModel = new UserOutputModel(
							user.getId(), user.getName(), "User", null, null,
							null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			searchOutputModel.setUrl("#/personality/?id=" + user.getId());
			searchOutputModel.setFormattedAge(DateUtils.getDateOrTimeAgo(user
					.getCreateTime()));
			searchOutputModel.setUser(user);
			searchOutputModel.setAgeString(DateUtils
					.calculateAgeWithString(user.getDob()));
			System.out.println("userId:::" + userId);
			if (userId != null) {
				UserConnection userConnection = userService
						.findUserConnectionByUserAndConnectionId(userId,
								user.getId());
				if (userConnection != null
						&& userConnection.getConnectionStatus() != null) {
					searchOutputModel.setConnectionStatus(userConnection
							.getConnectionStatus());
				} else {
					searchOutputModel.setConnectionStatus("AddConnection");
				}
			} else {
				searchOutputModel.setConnectionStatus("AddConnection");
			}
			if (user.getId().equals(userId)) {

			} else {
				searchOutputModels.add(searchOutputModel);
			}
		}
		return searchOutputModels;
	}

	private String getBaseUrl() throws UnknownHostException {
		Connector connector = ((TomcatEmbeddedServletContainer) appContext
				.getEmbeddedServletContainer()).getTomcat().getConnector();
		String scheme = connector.getScheme();
		String ip = InetAddress.getLocalHost().getHostAddress();
		int port = connector.getPort();
		String contextPath = appContext.getServletContext().getContextPath();
		System.out.println("local url :::" + scheme + "://" + ip + ":" + port
				+ contextPath);
		return scheme + "://" + ip + ":" + port + contextPath;
	}
}
