package com.ohmuk.folitics.businessDelegate.implementations;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ohmuk.folitics.businessDelegate.interfaces.ILinkBusinessDelegate;
import com.ohmuk.folitics.hibernate.entity.Link;
import com.ohmuk.folitics.service.ILinkService;

@Component
@Transactional
public class LinkBusinessDelegate implements ILinkBusinessDelegate {

	private static Logger logger = LoggerFactory.getLogger(LinkBusinessDelegate.class);

	@Autowired
	private volatile ILinkService linkService;

	@Override
	public Link create(Link link) throws Exception {

		logger.info("Inside  create method in business delegate of link");
		Link linkData = linkService.create(link);
		logger.info("Exiting create method in business delegate of link");
		return linkData;
	}

	@Override
	public Link getLinkById(Long id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Link> readAll() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Link> readAllActiveLink() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Link update(Link link) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean delete(Long id) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Link link) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteFromDB(Long id) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteFromDB(Link link) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Link> getLinksForSentiment(Long sentimentId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Link> getIsolatedLinks() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Link save(Link sessionLink) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Link saveAndFlush(Link sessionLink) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
