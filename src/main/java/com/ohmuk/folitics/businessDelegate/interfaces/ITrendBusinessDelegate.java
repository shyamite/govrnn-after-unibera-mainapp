/**
 * 
 */
package com.ohmuk.folitics.businessDelegate.interfaces;

import java.util.List;

import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.Response;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.trend.Trend;
import com.ohmuk.folitics.hibernate.entity.trend.TrendMapping;
import com.ohmuk.folitics.ouput.model.TrendOutputModel;
import com.ohmuk.folitics.ouput.model.UserTrendOutputModel;

/**
 * @author Deewan
 *
 */
public interface ITrendBusinessDelegate {
	List<Trend> displayTrend(Trend trend) throws Exception;
	boolean verifyIfTrendNameExist(Trend trend)throws Exception;
	Trend createTrend(Trend trend) throws Exception;
	List<User> top5User(String name) throws Exception;
	Long saveTrendMapping(Trend trend)throws Exception;
	Long addToTrendMapping(TrendMapping trendMapping);
	List<Trend> searchExactTrend(Trend trend);
	List<Opinion> searchTrendOpinion(Long trendId);
	Trend findTrend(Long id) throws Exception;
	Long createTrendMapping(TrendMapping trendMapping);
	List<TrendOutputModel> matchingTrend(String name);
	List<TrendOutputModel> getTopTrends(int count);
	List<UserTrendOutputModel> getTopTrendUsers(int count);
	Boolean deleteTrendById(Long id) throws Exception;
	Boolean manageTrend(List<String> trendNames, String action);
	Trend getTrendByName(String name) throws Exception;
	Boolean deleteTrendCommentsById(Long trendMappingId) throws Exception;
	List<Response> searchTrendResponse(Long id);
	Trend update(Trend trend) throws Exception;
	List<TrendOutputModel> getTopAdminTrends(int count);
	List<Trend> getAllAdminTrends(); 
}
