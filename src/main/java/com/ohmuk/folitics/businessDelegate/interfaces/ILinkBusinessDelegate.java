package com.ohmuk.folitics.businessDelegate.interfaces;

import java.util.List;

import com.ohmuk.folitics.hibernate.entity.Link;

public interface ILinkBusinessDelegate {

	/**
	 * Method is to add {@link Link}
	 * 
	 * @param link
	 * @return
	 * @throws Exception
	 */
	public Link create(Link link) throws Exception;

	/**
	 * Method is to get {@link Link} by id
	 * 
	 * @param id
	 * @return {@link Link}
	 * @throws Exception
	 */
	public Link getLinkById(Long id) throws Exception;

	/**
	 * Method is to get all {@link Link}
	 * 
	 * @return {@link List < Link >}
	 * @throws Exception
	 */
	public List<Link> readAll() throws Exception;

	/**
	 * Method is to get all active {@link Link}
	 * 
	 * @return {@link Link}
	 * @throws Exception
	 */
	public List<Link> readAllActiveLink() throws Exception;

	/**
	 * Method is to update {@link Link}
	 * 
	 * @param link
	 * @return {@link Link}
	 * @throws Exception
	 */
	public Link update(Link link) throws Exception;

	/**
	 * Method is to delete {@link Link} by id
	 * 
	 * @param id
	 * @return boolean
	 * @throws Exception
	 */
	public boolean delete(Long id) throws Exception;

	/**
	 * Method is to delete {@link Link}
	 * 
	 * @param link
	 * @return boolean
	 * @throws Exception
	 */
	public boolean delete(Link link) throws Exception;

	/**
	 * Method is hard delete {@link Link} by id
	 * 
	 * @param id
	 * @return boolean
	 * @throws Exception
	 */
	public boolean deleteFromDB(Long id) throws Exception;

	/**
	 * Method is to hard delete {@link Link}
	 * 
	 * @param link
	 * @return boolean
	 * @throws Exception
	 */
	public boolean deleteFromDB(Link link) throws Exception;

	/**
	 * Method is to get {@link Link} by sentimentId
	 * 
	 * @param sentimentId
	 * @return {@link List < Link > }
	 */
	public List<Link> getLinksForSentiment(Long sentimentId) throws Exception;

	/**
	 * Method is to get {@link Link} which are not attached with any
	 * {@link com.ohmuk.folitics.hibernate.entity.Sentiment}
	 * 
	 * @return {@link List < Link > }
	 */
	public List<Link> getIsolatedLinks() throws Exception;

	/**
	 * Method is to save {@link Link}
	 * 
	 * @param sessionLink
	 * @return {@link Link}
	 */
	public Link save(Link sessionLink) throws Exception;

	/**
	 * Method is to save and flush {@link Link}
	 * 
	 * @param sessionLink
	 * @return {@link Link}
	 * @throws Exception
	 */
	public Link saveAndFlush(Link sessionLink) throws Exception;

}
