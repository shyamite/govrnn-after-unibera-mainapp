package com.ohmuk.folitics.businessDelegate.interfaces;

import java.util.List;
import java.util.Set;

import com.ohmuk.folitics.enums.BreakingNewsState;
import com.ohmuk.folitics.hibernate.entity.BreakingNewsNews;
import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.Link;
import com.ohmuk.folitics.hibernate.entity.BreakingNews;
import com.ohmuk.folitics.ouput.model.BreakingNewsOutputModel;

public interface IBreakingNewsBusinessDelegate {

	/**
	 * Method is to save {@link BreakingNews}
	 * 
	 * @param breakingnews
	 * @return BreakingNews
	 * @throws Exception
	 */
	public BreakingNews save(BreakingNews breakingnews) throws Exception;

	/**
	 * Method is to get {@link BreakingNews} by id
	 * 
	 * @param id
	 * @return BreakingNews
	 * @throws Exception
	 */
	public BreakingNews read(Long id) throws Exception;

	/**
	 * Method is to get all {@link BreakingNews}
	 * 
	 * @return List<BreakingNews>
	 * @throws Exception
	 */
	public List<BreakingNewsNews> readAll() throws Exception;

	/**
	 * Method is to update breakingnews
	 * 
	 * @param breakingnews
	 * @return BreakingNews
	 * @throws Exception
	 */
	public BreakingNews update(BreakingNews breakingnews) throws Exception;

	/**
	 * Method is to soft delete {@link BreakingNews}
	 * 
	 * @param id
	 * @return boolean
	 * @throws Exception
	 */
	public boolean delete(Long id) throws Exception;

	/**
	 * Method is to soft delete {@link BreakingNews} by id
	 * 
	 * @param breakingnews
	 * @return boolean
	 * @throws Exception
	 */
	public boolean delete(BreakingNews breakingnews) throws Exception;

	/**
	 * Method is to hard delete {@link BreakingNews} by id
	 * 
	 * @param id
	 * @return boolean
	 * @throws Exception
	 */
	public boolean deleteFromDB(Long id) throws Exception;

	/**
	 * Method is to hard delete {@link BreakingNews}
	 * 
	 * @param boolean
	 * @return BreakingNews
	 * @throws Exception
	 */
	public boolean deleteFromDB(BreakingNews breakingnews) throws Exception;

	/**
	 * Method is to update {@link BreakingNews} status by id
	 * 
	 * @param id
	 * @param status
	 * @return boolean
	 * @throws Exception
	 */
	public boolean updateBreakingNewsStatus(Long id, String status)
			throws Exception;

	/**
	 * Method is to clone {@link BreakingNews}
	 * 
	 * @param breakingnews
	 * @return BreakingNews
	 * @throws Exception
	 */
	public BreakingNews clone(BreakingNews breakingnews) throws Exception;

	/**
	 * Method is to get all {@link BreakingNews} not id breakingnewsIds
	 * 
	 * @author Mayank Sharma
	 * @param breakingnewsIds
	 * @return List<BreakingNews>
	 * @throws Exception
	 */
	public List<BreakingNews> getAllBreakingNewsNotIn(Set<Long> breakingnewsIds)
			throws Exception;

	/**
	 * Method is to get all sources for {@link BreakingNews}
	 * 
	 * @param breakingnews
	 * @return List<Link>
	 * @throws Exception
	 */
	public List<Link> getAllSourcesForBreakingNews(BreakingNews breakingnews)
			throws Exception;

	/**
	 * Method is to get all Indicator by breakingnewsId
	 * 
	 * @author Mayank Sharma
	 * @param breakingnewsId
	 * @return Set<Category>
	 * @throws Exception
	 */
	public Set<Category> getAllIndicator(Long breakingnewsId) throws Exception;

	public List<BreakingNews> findByType(String type) throws Exception;

	public void saveNewsFeed();
	
	public List<BreakingNewsOutputModel> getAllBreakingNewsByMatch(String title) throws Exception;

	public List<BreakingNewsOutputModel> getAllBreakingNewsByType(String type) throws Exception;

	/**
	 * @param title
	 * @return
	 * @throws Exception
	 */
	public List<BreakingNewsOutputModel> getAllBreakingNewsByElasticMatch(String title) throws Exception;

    public List<BreakingNews> searchBreakingNews(String searchKeyword);

    List<BreakingNews> findByName(String subject) throws Exception;
    
    BreakingNews findById(Long id) throws Exception;

	List<BreakingNews> getAllBreakingNewsByExactMatch(String subject) throws Exception;

	List<BreakingNewsOutputModel> getPaginatedBreakingNewsByType(String type, int from, int to) throws Exception;

	List<BreakingNewsOutputModel> getBreakingNewsNameList(String type) throws Exception;
	
	List<BreakingNewsOutputModel> getPaginatedBreakingNewsByStatus(String type, String status ,int from, int to) throws Exception;

	List<BreakingNewsOutputModel> getBreakingNewsListByStatus(String type, String status) throws Exception;
	

	boolean changeBreakingNewsState(String subject, String breakingnewsType, BreakingNewsState state) throws Exception;
	boolean enableDisableBreakingNews(boolean enable, Long id) throws Exception;
	
}
