package com.ohmuk.folitics.businessDelegate.interfaces;

import java.util.List;

import com.ohmuk.folitics.beans.RatingDataBean;
import com.ohmuk.folitics.exception.MessageException;

/**
 * Business Delegate interface for Like
 * 
 * @author soumya
 *
 */
public interface IRatingsBusinessDelegate {

	/**
	 * @param ratingDataBean
	 * @return
	 * @throws MessageException
	 * @throws Exception
	 */
	public RatingDataBean rate(RatingDataBean ratingDataBean)
			throws MessageException, Exception;

	/**
	 * @param ratingDataBean
	 * @return
	 * @throws MessageException
	 * @throws Exception
	 */
	public RatingDataBean unrate(RatingDataBean ratingDataBean)
			throws MessageException, Exception;

	public List<RatingDataBean> getAllRatings(RatingDataBean ratingDataBean)
			throws MessageException, Exception;

	public List<RatingDataBean> getRatingForComponentByUser(
			RatingDataBean ratingDataBean) throws MessageException, Exception;

	RatingDataBean getRatingAggregation(RatingDataBean ratingDataBean) throws MessageException, Exception;

}
