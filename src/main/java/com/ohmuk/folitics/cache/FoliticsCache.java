package com.ohmuk.folitics.cache;

public interface FoliticsCache<K, V> {
	interface KEYS {
		String USER_IMAGE = "Default_User_Image";
		String REFERENCE_IMAGE = "Default_Reference_Image";
		String DAILY_NEWS = "Daily_News";
	}

	public void put(K key, V value);

	public V get(K key);
}
