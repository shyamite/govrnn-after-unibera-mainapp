package com.ohmuk.folitics.sentiment.input;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import au.com.bytecode.opencsv.CSVReader;

import com.ohmuk.folitics.businessDelegate.interfaces.ISentimentBusinessDelegate;
import com.ohmuk.folitics.cvs.reader.input.CSVFileReader;
import com.ohmuk.folitics.enums.CategoryType;
import com.ohmuk.folitics.excel.reader.input.ExcelFileReader;
import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.Sentiment;
import com.ohmuk.folitics.hibernate.entity.SentimentNews;
import com.ohmuk.folitics.hibernate.entity.attachment.Attachment;
import com.ohmuk.folitics.hibernate.entity.poll.Poll;
import com.ohmuk.folitics.hibernate.entity.poll.PollOption;

@Service
public class SentimentInputReaderImp implements ISentimentInputReader {

	final static Logger LOGGER = LoggerFactory
			.getLogger(SentimentInputReaderImp.class);

	@Autowired
	CSVFileReader fileReader;

	@Autowired
	ExcelFileReader workbook;

	@Autowired
	private volatile ISentimentBusinessDelegate sentimentBusinessDelegate;

	public static final String POLLS_HEADER = "Polls";
	public static final String SENTIEMNT_HEADER = "Subject";
	public static final String REFERENCES_HEADER = "References";
	public static final String SENTIMENT_NEWS_HEADER = "SentimentNews";

	/*
	 * @Override public SentimentInputModel loadSentiment(String filePath) { try
	 * { CSVReader csvReader = fileReader.getReader(filePath);
	 * SentimentInputModel sentiment = prepareSentiment(csvReader); return
	 * sentiment; } catch (IOException e) { e.printStackTrace(); } return null;
	 * }
	 */

	@Override
	public SentimentInputModel loadSentiment(String filePath) {
		try {
			Workbook excelFileReader = workbook.getReader(filePath);
			SentimentInputModel sentiment = prepareSentiment(excelFileReader);
			return sentiment;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public SentimentInputModel loadFile(InputStream inputStream) {
		try {
			CSVReader csvReader = new CSVReader(new InputStreamReader(
					inputStream), '\t');
			// fileReader.getReader(filePath);
			SentimentInputModel sentiment = prepareSentimentNews(csvReader);
			return sentiment;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private SentimentInputModel prepareSentiment(Workbook workbook)
			throws IOException {
		SentimentInputModel sentiment = new SentimentInputModel();
		// String[] record;
		// int rowCount=0;
		// String currentObject;
		boolean sentimentHeader = false;
		boolean readingPolls = false;
		boolean readingReferences = false;
		boolean readingSentimentNews = false;
		int noOfPollOptions = 0;
		List<PollInputModel> polls = new ArrayList<>();
		List<SentimentNewsInputModel> sentimentNews = new ArrayList<>();
		List<SentimentReferencesInputModel> references = new ArrayList<>();

		Sheet sentimentSheet = workbook.getSheetAt(0);

		Iterator<Row> rows = sentimentSheet.iterator();

		for (int row = 0; row < sentimentSheet.getPhysicalNumberOfRows(); row++) {

			Row currentRow = sentimentSheet.getRow(row);

			for (int j = 0; currentRow != null
					&& j < currentRow.getPhysicalNumberOfCells(); j++) {

				Cell column = currentRow.getCell(j);

				if (column != null
						&& column.getStringCellValue() != null
						&& column.getStringCellValue().equalsIgnoreCase(
								SENTIEMNT_HEADER)) {
					sentimentHeader = true;
					readingPolls = false;
					readingReferences = false;
					readingSentimentNews = false;
					row++;
					break;
				}

				else if (column != null
						&& column.getStringCellValue().equalsIgnoreCase(
								POLLS_HEADER)) {
					sentimentHeader = false;
					readingPolls = true;
					row = row + 2;
					noOfPollOptions = 5;
					readingReferences = false;
					readingSentimentNews = false;
					break;
				}

				else if (column != null
						&& column.getStringCellValue().equalsIgnoreCase(
								REFERENCES_HEADER)) {
					sentimentHeader = false;
					readingPolls = false;
					readingReferences = true;
					readingSentimentNews = false;
					row = row + 2;
					break;
				} else if (column != null
						&& column.getStringCellValue().equalsIgnoreCase(
								SENTIMENT_NEWS_HEADER)) {
					sentimentHeader = false;
					readingPolls = false;
					readingReferences = false;
					readingSentimentNews = true;
					row = row + 2;
					break;
				}
			}
			currentRow = sentimentSheet.getRow(row);
			if (sentimentHeader && currentRow != null) {
				if (currentRow.getCell(0) != null) {
					sentiment.setSubject(StringUtils.trimWhitespace(currentRow
							.getCell(0).getStringCellValue()));
				}
				if (currentRow.getCell(1) != null) {
					sentiment.setDescription(StringUtils
							.trimWhitespace(currentRow.getCell(1)
									.getStringCellValue()));
				}
				if (currentRow.getCell(2) != null) {
					sentiment.setImageUrl(StringUtils.trimWhitespace(currentRow
							.getCell(2).getStringCellValue()));
				}

				if (currentRow.getCell(3) != null) {
					sentiment.setImageType(StringUtils
							.trimWhitespace(currentRow.getCell(3)
									.getStringCellValue()));
				}
				if (currentRow.getCell(4) != null) {
					sentiment.setType(StringUtils.trimWhitespace(currentRow
							.getCell(4).getStringCellValue()));
				}

				if (currentRow.getCell(5) != null) {
					sentiment.setCategories(StringUtils
							.trimWhitespace(currentRow.getCell(5)
									.getStringCellValue()));
				}
				if (currentRow.getCell(6) != null) {
					sentiment.setRelatedSentiments(StringUtils
							.trimWhitespace(currentRow.getCell(6)
									.getStringCellValue()));

				}
				if (currentRow.getCell(7) != null) {
					sentiment.setRelatedSentimentsId(StringUtils
							.trimWhitespace(currentRow.getCell(7)
									.getStringCellValue()));
				}
				if (currentRow.getCell(8) != null) {
					sentiment.setKeywords(StringUtils.trimWhitespace(currentRow
							.getCell(8).getStringCellValue()));
				}
				if (currentRow.getCell(9) != null) {
					sentiment.setFeedSources(StringUtils.trimWhitespace(currentRow
							.getCell(9).getStringCellValue()));
				}
			} else if (readingPolls && currentRow != null) {
				PollInputModel pollInputModel = new PollInputModel();
				if (currentRow.getCell(0) != null) {
					pollInputModel.setImage(StringUtils
							.trimWhitespace(currentRow.getCell(0)
									.getStringCellValue()));
				}
				if (currentRow.getCell(1) != null) {
					pollInputModel.setQuestion(StringUtils
							.trimWhitespace(currentRow.getCell(1)
									.getStringCellValue()));
				}
				// read options until they are empty columns max poll options
				// are set to 5
				List<String> options = new ArrayList<String>();
				for (int i = 2; i < (noOfPollOptions); i++) {
					if (currentRow.getCell(i) != null
							&& !(currentRow.getCell(i).getStringCellValue())
									.isEmpty()) {
						options.add(StringUtils.trimWhitespace(currentRow
								.getCell(i).getStringCellValue()));
					}
				}
				List<PollOption> polloptions = new ArrayList<PollOption>();
				for (String op : options) {
					PollOption pollopp = new PollOption();
					pollopp.setPollOption(op);
					polloptions.add(pollopp);
				}
				pollInputModel.setOptions(polloptions);
				polls.add(pollInputModel);
			}

			else if (readingReferences && currentRow != null) {
				SentimentReferencesInputModel referencesInputModel = new SentimentReferencesInputModel();
				if (currentRow.getCell(0) != null
						&& !currentRow.getCell(0).getStringCellValue()
								.isEmpty()) {
					referencesInputModel.setFilePath(StringUtils
							.trimWhitespace(currentRow.getCell(0)
									.getStringCellValue()));
				}
				if (currentRow.getCell(1) != null
						&& !currentRow.getCell(1).getStringCellValue()
								.isEmpty()) {
					referencesInputModel.setFileType(StringUtils
							.trimWhitespace(currentRow.getCell(1)
									.getStringCellValue()));
				}
				if (currentRow.getCell(2) != null
						&& !currentRow.getCell(2).getStringCellValue()
								.isEmpty()) {
					referencesInputModel.setDescription(StringUtils
							.trimWhitespace(currentRow.getCell(2)
									.getStringCellValue()));
				}

				if (currentRow.getCell(3) != null
						&& !currentRow.getCell(3).getStringCellValue()
								.isEmpty()) {
					referencesInputModel.setAttachmentType(StringUtils
							.trimWhitespace(currentRow.getCell(3)
									.getStringCellValue()));
				}
				if (currentRow.getCell(4) != null
						&& !currentRow.getCell(4).getStringCellValue()
								.isEmpty()) {
					referencesInputModel.setTitle(StringUtils
							.trimWhitespace(currentRow.getCell(4)
									.getStringCellValue()));
				}

				if (currentRow.getCell(5) != null
						&& !currentRow.getCell(5).getStringCellValue()
								.isEmpty()) {
					referencesInputModel.setProvider(StringUtils
							.trimWhitespace(currentRow.getCell(5)
									.getStringCellValue()));
				}
				if (currentRow.getCell(6) != null
						&& !currentRow.getCell(6).getStringCellValue()
								.isEmpty()) {
					referencesInputModel.setAttachmentFilePath(StringUtils
							.trimWhitespace(currentRow.getCell(6)
									.getStringCellValue()));
				}
				references.add(referencesInputModel);
			} else if (readingSentimentNews && currentRow != null) {
				SentimentNewsInputModel newsInputModel = null;
				LOGGER.info("currentRow.getCell(0) : "+currentRow.getCell(0) );
				if (currentRow.getCell(0) != null 
						&& ! currentRow.getCell(0).getStringCellValue().trim()
								.isEmpty()) {
					newsInputModel = new SentimentNewsInputModel();
					newsInputModel.setFeedSource(StringUtils
							.trimWhitespace(currentRow.getCell(0)
									.getStringCellValue()));
				}
				if (currentRow.getCell(1) != null
						&& !currentRow.getCell(1).getStringCellValue().trim()
								.isEmpty()) {
					newsInputModel.setCategory(StringUtils
							.trimWhitespace(currentRow.getCell(1)
									.getStringCellValue()));
				}
				if (currentRow.getCell(2) != null
						&& !currentRow.getCell(2).getStringCellValue().trim()
								.isEmpty()) {
					newsInputModel.setTitle(StringUtils
							.trimWhitespace(currentRow.getCell(2)
									.getStringCellValue()));
				}

				if (currentRow.getCell(3) != null
						&& !currentRow.getCell(3).getStringCellValue().trim()
								.isEmpty()) {
					newsInputModel.setLink(StringUtils
							.trimWhitespace(currentRow.getCell(3)
									.getStringCellValue()));
				}
				if (currentRow.getCell(4) != null
						&& !currentRow.getCell(4).getStringCellValue().trim()
								.isEmpty()) {
					newsInputModel.setPreviewImagePath(StringUtils
							.trimWhitespace(currentRow.getCell(4)
									.getStringCellValue()));
				}

				if (currentRow.getCell(5) != null
						&& !currentRow.getCell(5).getStringCellValue().trim()
								.isEmpty()) {
					newsInputModel.setHtmlText(StringUtils
							.trimWhitespace(currentRow.getCell(5)
									.getStringCellValue()));
				}
				if (currentRow.getCell(6) != null
						&& !currentRow.getCell(6).getStringCellValue().trim()
								.isEmpty()) {
					newsInputModel.setPlainText(StringUtils
							.trimWhitespace(currentRow.getCell(6)
									.getStringCellValue()));
				}
				if (currentRow.getCell(7) != null
						&& !currentRow.getCell(7).getStringCellValue().trim()
								.isEmpty()) {
					newsInputModel.setFeedURL(StringUtils
							.trimWhitespace(currentRow.getCell(7)
									.getStringCellValue()));
				}
				if (currentRow.getCell(8) != null
						&& !currentRow.getCell(8).getStringCellValue().trim()
								.isEmpty()) {
					newsInputModel.setFeedName(StringUtils
							.trimWhitespace(currentRow.getCell(8)
									.getStringCellValue()));
				}
				if (currentRow.getCell(9) != null
						&& !currentRow.getCell(9).getStringCellValue().trim()
								.isEmpty()) {
					newsInputModel.setWriter(StringUtils
							.trimWhitespace(currentRow.getCell(9)
									.getStringCellValue()));
				}
				if(newsInputModel!=null)
					sentimentNews.add(newsInputModel);
				else 
					break;
			}

		}
		sentiment.setPolls(polls);
		sentiment.setReferences(references);
		sentiment.setSentimentNews(sentimentNews);
		return sentiment;
	}

	private SentimentInputModel prepareSentiment(CSVReader reader)
			throws IOException {
		SentimentInputModel sentiment = new SentimentInputModel();
		// String[] record;
		// int rowCount=0;
		// String currentObject;
		boolean readingPolls = false;
		boolean readingReferences = false;
		boolean readingSentimentNews = false;

		List<PollInputModel> polls = new ArrayList<>();
		List<SentimentNewsInputModel> sentimentNews = new ArrayList<>();
		List<SentimentReferencesInputModel> references = new ArrayList<>();

		List<String[]> rows = reader.readAll();

		for (int rowCount = 0; rowCount < rows.size();) {
			String[] record = rows.get(rowCount);
			// String[] record = rowRecord[0].split("\t");
			if (rowCount == 0) {
				rowCount++;
			} else if (rowCount == 1) {
				sentiment.setSubject(StringUtils.trimWhitespace(record[0]));
				sentiment.setDescription(StringUtils.trimWhitespace(record[1]));
				sentiment.setImageUrl(StringUtils.trimWhitespace(record[2]));
				sentiment.setImageType(StringUtils.trimWhitespace(record[3]));
				sentiment.setType(StringUtils.trimWhitespace(record[4]));
				sentiment.setCategories(StringUtils.trimWhitespace(record[5]));
				sentiment.setRelatedSentiments(StringUtils
						.trimWhitespace(record[6]));
				sentiment.setRelatedSentimentsId(StringUtils
						.trimWhitespace(record[7]));
				rowCount++;
			} else {
				// Polls header
				if (POLLS_HEADER.equalsIgnoreCase(StringUtils
						.trimWhitespace(record[0]))) {
					rowCount = rowCount + 2; // eliminate the header polls as
												// well as the next header (that
												// is question option1 to option
												// 5;)
					readingPolls = true;
					readingReferences = false;
					readingSentimentNews = false;
					continue;
				}
				// References header
				if (REFERENCES_HEADER.equalsIgnoreCase(StringUtils
						.trimWhitespace(record[0]))) {
					rowCount = rowCount + 2; // eliminate the header as well as
												// the next header
					readingPolls = false;
					readingReferences = true;
					readingSentimentNews = false;
					continue;

				}
				// SentimentNews header
				if (SENTIMENT_NEWS_HEADER.equalsIgnoreCase(StringUtils
						.trimWhitespace(record[0]))) {
					rowCount = rowCount + 2; // eliminate the header as well as
												// the next header
					readingPolls = false;
					readingReferences = false;
					readingSentimentNews = true;
					continue;

				}
				if (readingPolls) {
					PollInputModel pollInputModel = new PollInputModel();
					pollInputModel.setImage(StringUtils
							.trimWhitespace(record[0]));
					pollInputModel.setQuestion(StringUtils
							.trimWhitespace(record[1]));
					// read options until they are empty columns max poll
					// options are set to 5
					List<String> options = new ArrayList<String>();
					for (int i = 2; i < (record.length - 1); i++) {
						if (!(StringUtils.trimWhitespace(record[i])).isEmpty()) {
							options.add(StringUtils.trimWhitespace(record[i]));
						}
					}
					List<PollOption> polloptions = new ArrayList<PollOption>();
					for (String op : options) {
						PollOption pollopp = new PollOption();
						pollopp.setPollOption(op);
						polloptions.add(pollopp);
					}
					pollInputModel.setOptions(polloptions);
					rowCount++;
					polls.add(pollInputModel);
				}
				if (readingReferences) {
					SentimentReferencesInputModel referencesInputModel = new SentimentReferencesInputModel();
					referencesInputModel.setFilePath(StringUtils
							.trimWhitespace(record[0]));
					referencesInputModel.setFileType(StringUtils
							.trimWhitespace(record[1]));
					referencesInputModel.setDescription(StringUtils
							.trimWhitespace(record[2]));
					referencesInputModel.setAttachmentType(StringUtils
							.trimWhitespace(record[3]));
					referencesInputModel.setTitle(StringUtils
							.trimWhitespace(record[4]));
					referencesInputModel.setProvider(StringUtils
							.trimWhitespace(record[5]));
					referencesInputModel.setAttachmentFilePath(StringUtils
							.trimWhitespace(record[6]));
					references.add(referencesInputModel);
					rowCount++;
				}
				if (readingSentimentNews) {
					SentimentNewsInputModel newsInputModel = new SentimentNewsInputModel();
					newsInputModel.setFeedSource(StringUtils
							.trimWhitespace(record[0]));
					newsInputModel.setCategory(StringUtils
							.trimWhitespace(record[1]));
					newsInputModel.setTitle(StringUtils
							.trimWhitespace(record[2]));
					newsInputModel.setLink(StringUtils
							.trimWhitespace(record[3]));
					newsInputModel.setPreviewImagePath(StringUtils
							.trimWhitespace(record[4]));
					newsInputModel.setHtmlText(StringUtils
							.trimWhitespace(record[5]));
					newsInputModel.setPlainText(StringUtils
							.trimWhitespace(record[6]));
					newsInputModel.setFeedURL(StringUtils
							.trimWhitespace(record[7]));
					newsInputModel.setFeedName(StringUtils
							.trimWhitespace(record[8]));
					newsInputModel.setWriter(StringUtils
							.trimWhitespace(record[9]));
					sentimentNews.add(newsInputModel);
					rowCount++;
				}

			}

		}
		sentiment.setPolls(polls);
		sentiment.setReferences(references);
		sentiment.setSentimentNews(sentimentNews);
		return sentiment;
	}

	private SentimentInputModel prepareSentimentNews(CSVReader reader)
			throws IOException {
		SentimentInputModel sentiment = new SentimentInputModel();

		List<SentimentNewsInputModel> sentimentNews = new ArrayList<>();

		List<String[]> rows = reader.readAll();

		for (int rowCount = 0; rowCount < rows.size();) {
			String[] record = rows.get(rowCount);
			// String[] record = rowRecord[0].split("\t");
			if (rowCount == 0 || rowCount == 1) {
				rowCount++;
			} else {
				SentimentNewsInputModel newsInputModel = new SentimentNewsInputModel();
				sentiment.setSubject(StringUtils.trimWhitespace(record[0]));
				newsInputModel.setCategory(StringUtils
						.trimWhitespace(record[1]));
				newsInputModel.setTitle(StringUtils.trimWhitespace(record[2]));
				newsInputModel.setLink(StringUtils.trimWhitespace(record[3]));
				newsInputModel.setPreviewImagePath(StringUtils
						.trimWhitespace(record[4]));
				newsInputModel.setHtmlText(StringUtils
						.trimWhitespace(record[5]));
				newsInputModel.setPlainText(StringUtils
						.trimWhitespace(record[6]));
				newsInputModel
						.setFeedURL(StringUtils.trimWhitespace(record[7]));
				newsInputModel.setFeedName(StringUtils
						.trimWhitespace(record[8]));
				newsInputModel.setWriter(StringUtils.trimWhitespace(record[9]));
				sentimentNews.add(newsInputModel);
				rowCount++;

			}

		}
		sentiment.setSentimentNews(sentimentNews);
		return sentiment;
	}

	@Override
	/**
	 * 
	 * @param inputModel
	 * @param name
	 * @return
	 */
	public Sentiment getSentimentFromInputModel(SentimentInputModel inputModel,
			String imagesPath) {

		Sentiment sentiment = new Sentiment();

		if (inputModel != null) {

			sentiment.setSubject(inputModel.getSubject());
			sentiment.setKeywords(inputModel.getKeywords());
			sentiment.setFeedSources(inputModel.getFeedSources());
			// sentiment.setDescription(inputModel.getDescription());
			// getting first 1000 charecters at most.
			sentiment.setDescription(inputModel.getDescription().substring(0,
					Math.min(inputModel.getDescription().length(), 1000)));
			LOGGER.info("Sentiment Image Path : " + imagesPath + File.separator
					+ inputModel.getImageUrl());
			Path path = Paths.get(imagesPath + File.separator
					+ inputModel.getImageUrl());
			byte[] data = null;
			try {
				data = Files.readAllBytes(path);
				sentiment.setImage(data);
				// sentiment.setImage(ThumbnailUtil.getImageOriginal(data,
				// inputModel.getImageType()));
			} catch (IOException e1) {
				// sentiment.setImage(data);
				e1.printStackTrace();
			}

			sentiment.setImageType(inputModel.getImageType());
			sentiment.setType(inputModel.getType());
			sentiment.setCreated_By(1l);
			sentiment.setStartTime(new Timestamp(new Date().getTime()));
			sentiment.setEndTime(new Timestamp(new Date().getTime()));
			// categoreis

			if (inputModel.getCategories() == null) {
				Category category = new Category();
				Set<Category> categories = new HashSet<>();
				category = new Category();
				category.setCreatedBy(1l);
				category.setName(inputModel.getSubject());
				category.setType(CategoryType.CATEGORY.getValue());
				category.setStatus("Active");
				category.setDescription(inputModel.getSubject());
				categories.add(category);
				sentiment.setCategories(categories);
			}

			if (inputModel.getCategories() != null
					&& !inputModel.getCategories().trim().isEmpty()) {

				Category category = new Category();
				Set<Category> categories = new HashSet<>();
				String[] inputModelCategories = inputModel.getCategories()
						.split(",");

				if (inputModelCategories.length > 0) {
					for (String catSring : inputModelCategories) {

						category = new Category();
						category.setCreatedBy(1l);
						category.setName(catSring);
						category.setType(CategoryType.CATEGORY.getValue());
						category.setStatus("Active");
						category.setDescription(catSring);
						categories.add(category);
						// sentiment.setCategories(categories);
					}
				} else {

					category = new Category();
					category.setCreatedBy(1l);
					category.setName(inputModel.getCategories());
					category.setType(CategoryType.CATEGORY.getValue());
					category.setStatus("Active");
					category.setDescription(inputModel.getCategories());
					categories.add(category);
				}
				sentiment.setCategories(categories);
			}
			// related Sentiments

			if (inputModel.getRelatedSentiments() != null
					&& !inputModel.getRelatedSentiments().trim().isEmpty()) {

				Sentiment relatedSentiment = null;
				List<Sentiment> matchedSentiments = null;
				Set<Sentiment> relatedSentiments = new HashSet<>();

				String[] inputModelRelSentiments = inputModel
						.getRelatedSentiments().split(",");
				if (inputModelRelSentiments.length > 0) {
					for (String subject : inputModelRelSentiments) {
						try {
							matchedSentiments = sentimentBusinessDelegate
									.getAllSentimentByExactMatch(subject);
							if (matchedSentiments != null
									&& !matchedSentiments.isEmpty()) {
								relatedSentiments.add(matchedSentiments.get(0));
							}
						} catch (Exception e) {
							LOGGER.error("Exception in getting related sentiemt with subject "
									+ subject + " " + e.getMessage());
						}
					}

				} else {

					try {
						relatedSentiment = sentimentBusinessDelegate
								.read(Long.parseLong(inputModel
										.getRelatedSentimentsId()));
						if (relatedSentiment != null) {
							relatedSentiments.add(relatedSentiment);
						}
					} catch (Exception e) {
						LOGGER.error("Exception in getting related sentiemt id "
								+ inputModel.getRelatedSentimentsId()
								+ " "
								+ e.getMessage());
					}

				}

				sentiment.setRelatedSentiments(relatedSentiments);
			}

			// polls
			if (inputModel.getPolls() != null
					&& !inputModel.getPolls().isEmpty()) {

				List<Poll> polls = new ArrayList<>();
				for (PollInputModel poInputModel : inputModel.getPolls()) {
					Poll poll = new Poll();
					poll.setCreatedBy(1l);
					path = Paths.get(imagesPath + File.separator
							+ poInputModel.getImage());
					data = null;
					try {
						data = Files.readAllBytes(path);
						poll.setImage(data);
						// poll.setImage(ThumbnailUtil.getSentimentImageThumbnail(data,
						// inputModel.getImageType()));
					} catch (IOException e1) {

						e1.printStackTrace();
					}

					poll.setQuestion(poInputModel.getQuestion());
					if (poInputModel.getOptions() != null
							&& !poInputModel.getOptions().isEmpty()) {
						List<PollOption> pollOptions = new ArrayList<>();
						for (PollOption option : poInputModel.getOptions()) {

							PollOption pollOption = new PollOption();
							pollOption.setCreatedBy(1l);
							pollOption.setPollOption(option.getPollOption());
							pollOption.setPoll(poll);
							pollOptions.add(pollOption);
						}
						poll.setOptions(pollOptions);

					}
					polls.add(poll);
				}
				sentiment.setPolls(polls);
			}
			// attachemnts
			if (inputModel.getReferences() != null
					&& !inputModel.getReferences().isEmpty()) {

				List<Attachment> attachments = new ArrayList<>();
				for (SentimentReferencesInputModel referencesInputModel : inputModel
						.getReferences()) {
					Attachment attachment = new Attachment();
					attachment.setAttachmentType(referencesInputModel
							.getAttachmentType());
					attachment.setCreated_By(1l);
					attachment.setDescription(referencesInputModel
							.getDescription());
					attachment.setFilePath(referencesInputModel.getFilePath());
					attachment.setFileType(referencesInputModel.getFileType()
							.toLowerCase());
					attachment.setTitle(referencesInputModel.getTitle());
					attachment.setProvider(referencesInputModel.getProvider());
					attachments.add(attachment);
				}

				sentiment.setAttachments(attachments);
			}
			// news
			if (inputModel.getSentimentNews() != null
					&& !inputModel.getSentimentNews().isEmpty()) {

				List<SentimentNews> sentimentNews = new ArrayList<>();
				for (SentimentNewsInputModel newsInputModel : inputModel
						.getSentimentNews()) {
					SentimentNews news = new SentimentNews();
					path = Paths.get(imagesPath + File.separator
							+ newsInputModel.getPreviewImagePath());
					data = null;
					try {
						data = Files.readAllBytes(path);
						// news.setPreviewImagePath(ThumbnailUtil.getSentimentImageThumbnail(data,
						// inputModel.getImageType()));
						// news.setPreviewImagePath(data);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					news.setFeedSource(newsInputModel.getFeedSource());
					news.setFeedName(newsInputModel.getFeedName());
					news.setCategory(newsInputModel.getCategory());
					news.setTitle(newsInputModel.getTitle());
					news.setLink(newsInputModel.getLink());
					news.setHtmlText(org.apache.commons.lang3.StringUtils.substring(newsInputModel.getHtmlText(), 0, 2000) );
					news.setPlainText(org.apache.commons.lang3.StringUtils.substring(newsInputModel.getPlainText(), 0, 2000) );
					news.setFeedURL(newsInputModel.getFeedURL());
					news.setWriter(newsInputModel.getWriter());
					sentimentNews.add(news);
				}
				sentiment.setSentimentNews(sentimentNews);
			}

		}
		return sentiment;
	}

	@Override
	public List<Sentiment> getSentimentFromInputModelForNews(
			SentimentInputModel inputModel) {

		List<Sentiment> sentiments = new ArrayList<Sentiment>();
		List<SentimentNews> sentimentNews = new ArrayList<>();

		if (inputModel != null) {
			if (inputModel.getSubject() != null
					&& !inputModel.getSubject().isEmpty()) {
				try {
					// news
					if (inputModel.getSentimentNews() != null
							&& !inputModel.getSentimentNews().isEmpty()) {
						for (SentimentNewsInputModel newsInputModel : inputModel
								.getSentimentNews()) {
							SentimentNews news = new SentimentNews();
							news.setFeedSource(newsInputModel.getFeedSource());
							news.setFeedName(newsInputModel.getFeedName());
							news.setCategory(newsInputModel.getCategory());
							news.setTitle(newsInputModel.getTitle());
							news.setLink(newsInputModel.getLink());
							news.setHtmlText(org.apache.commons.lang3.StringUtils.substring(newsInputModel.getHtmlText(), 0, 2000) );
							news.setPlainText(org.apache.commons.lang3.StringUtils.substring(newsInputModel.getPlainText(), 0, 2000) );
							news.setWriter(newsInputModel.getWriter());
							sentimentNews.add(news);
						}
					}
					sentiments = sentimentBusinessDelegate
							.findByName(inputModel.getSubject());
					if (sentiments != null && !sentiments.isEmpty()) {
						for (Sentiment sentiment : sentiments) {
							sentiment.setSentimentNews(sentimentNews);
						}
					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return sentiments;
	}

}
