package com.ohmuk.folitics.sentiment.input;

import java.util.List;

import com.ohmuk.folitics.hibernate.entity.poll.PollOption;

public class PollInputModel {
	private String question;
	private long id;
	private List<PollOption> options;
	private String image;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public List<PollOption> getOptions() {
		return options;
	}

	public void setOptions(List<PollOption> options) {
		this.options = options;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
}
