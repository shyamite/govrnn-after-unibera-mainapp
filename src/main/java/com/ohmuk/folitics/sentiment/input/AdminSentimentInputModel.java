package com.ohmuk.folitics.sentiment.input;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.ohmuk.folitics.dto.RelatedSentimentsDto;
import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.attachment.Attachment;
import com.ohmuk.folitics.hibernate.entity.poll.Poll;

public class AdminSentimentInputModel {
	private Long id;
	private String subject;
	private String description;
	private String image;
	private String imageType;
	private String type;
	private ArrayList<Category> categories;
	private Set<String> bingImages = new HashSet<>();
	// private String relatedSentiments;
	private ArrayList<Long> relatedSentimentsId;
	private String keywords;
	private String feedSources;
	private String state;
	private boolean enabled;
	private boolean active;
	private Timestamp endTime;
	public Timestamp getEndTime() {
		return endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	private List<PollInputModel> polls;
	private List<Attachment> links;
	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public List<PollInputModel> getPolls() {
		return polls;
	}

	public void setPolls(List<PollInputModel> polls) {
		this.polls = polls;
	}

	public List<Attachment> getLinks() {
		return links;
	}

	public void setLinks(List<Attachment> links) {
		this.links = links;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageType() {
		return imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ArrayList<Category> getCategories() {
		return categories;
	}

	public void setCategories(ArrayList<Category> categories) {
		this.categories = categories;
	}

//	public String getRelatedSentiments() {
//		return relatedSentiments;
//	}
//
//	public void setRelatedSentiments(String relatedSentiments) {
//		this.relatedSentiments = relatedSentiments;
//	}

	public Set<String> getBingImages() {
		return bingImages;
	}

	public void setBingImages(Set<String> bingImages) {
		this.bingImages = bingImages;
	}
	
	public ArrayList<Long> getRelatedSentimentsId() {
		return relatedSentimentsId;
	}

	public void setRelatedSentimentsId(ArrayList<Long> relatedSentimentsId) {
		this.relatedSentimentsId = relatedSentimentsId;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getFeedSources() {
		return feedSources;
	}

	public void setFeedSources(String feedSources) {
		this.feedSources = feedSources;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "AdminSentimentInputModel [id=" + id + ", subject=" + subject + ", description=" + description
				+ ", imageByte=" + image + ", imageType=" + imageType + ", type=" + type + ", categories="
				+ categories + ", relatedSentimentsId="
				+ relatedSentimentsId + ", keywords=" + keywords + ", feedSources=" + feedSources + ", state="
				+ ", polls=" + polls + "]";
	}
}
