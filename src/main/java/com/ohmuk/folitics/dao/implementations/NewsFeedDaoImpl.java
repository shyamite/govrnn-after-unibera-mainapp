package com.ohmuk.folitics.dao.implementations;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ohmuk.folitics.dao.interfaces.NewsFeedDao;
import com.ohmuk.folitics.hibernate.entity.BreakingNewsNews;
import com.ohmuk.folitics.hibernate.entity.NewsFeed;
import com.ohmuk.folitics.hibernate.entity.Sentiment;
import com.ohmuk.folitics.hibernate.entity.SentimentNews;

@Repository("newsFeedDao")
@Transactional
public class NewsFeedDaoImpl implements NewsFeedDao {

	@Autowired
	private SessionFactory _sessionFactory;

	private Session getSession() {
		return _sessionFactory.getCurrentSession();
	}

	@Override
	public void save(SentimentNews sentimentNews) {
		getSession().createSQLQuery("set names 'UTF8'").executeUpdate();
		getSession().createSQLQuery("set character set 'UTF8'").executeUpdate();
		getSession().save(sentimentNews);
	}

	@Override
	public void update(SentimentNews sentimentNews) {
		getSession().update(sentimentNews);
	}
	
	@Override
	public void delete(SentimentNews sentimentNews) {
		getSession().delete(sentimentNews);
	}

	@Override
	public void saveBreakingNews(BreakingNewsNews breakingNews) {
		// TODO Auto-generated method stub
		getSession().createSQLQuery("set names 'UTF8'").executeUpdate();
		getSession().createSQLQuery("set character set 'UTF8'").executeUpdate();
		getSession().save(breakingNews);
	}

	@Override
	public void updateBreakingNews(BreakingNewsNews breakingNews) {
		// TODO Auto-generated method stub
		getSession().update(breakingNews);
	}

	@Override
	public void deleteBreakingNews(BreakingNewsNews breakingNews) {
		// TODO Auto-generated method stub
		getSession().delete(breakingNews);
	}

	@Override
	public NewsFeed saveNewsFeed(NewsFeed newsfeed) {
		// TODO Auto-generated method stub
		getSession().save(newsfeed);
		return null;
	}

	@Override
	public NewsFeed updateNewsFeed(NewsFeed newsfeed) {
		// TODO Auto-generated method stub
		getSession().update(newsfeed);
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<NewsFeed> getAllNewsFeed() {
		// TODO Auto-generated method stub
		return (List<NewsFeed>)getSession().createCriteria(NewsFeed.class).list();
	}

	@Override
	public NewsFeed deleteNewsFeed(NewsFeed newsfeed) {
		// TODO Auto-generated method stub
		getSession().delete(newsfeed);
		return null;
	}
}
