package com.ohmuk.folitics.dao.interfaces;

import java.util.List;

import com.ohmuk.folitics.hibernate.entity.BreakingNewsNews;
import com.ohmuk.folitics.hibernate.entity.NewsFeed;
import com.ohmuk.folitics.hibernate.entity.SentimentNews;

public interface NewsFeedDao {
	public void save(SentimentNews sentimentNews);
	public NewsFeed saveNewsFeed(NewsFeed newsfeed);
	public NewsFeed updateNewsFeed(NewsFeed newsfeed);
	public List<NewsFeed> getAllNewsFeed();
	public NewsFeed deleteNewsFeed(NewsFeed newsfeed);

	void update(SentimentNews sentimentNews);

	void delete(SentimentNews sentimentNews);
	
	public void saveBreakingNews(BreakingNewsNews breakingNews);

	void updateBreakingNews(BreakingNewsNews breakingNews);

	void deleteBreakingNews(BreakingNewsNews breakingNews);
}
