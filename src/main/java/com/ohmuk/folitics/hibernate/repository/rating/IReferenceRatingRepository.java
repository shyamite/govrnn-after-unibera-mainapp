package com.ohmuk.folitics.hibernate.repository.rating;

import java.util.List;

import com.ohmuk.folitics.hibernate.entity.rating.ReferenceRating;

/**
 * Interface for entity: {@link sentiment reference} repository
 * @author soumya
 *
 */
public interface IReferenceRatingRepository extends IRatingHibernateRepository<ReferenceRating> {
	List<ReferenceRating> findByParentIdAndUserId(Long parentId, Long userId);
	ReferenceRating findByComponentIdAndUserId(Long componentId, Long userId);
}
