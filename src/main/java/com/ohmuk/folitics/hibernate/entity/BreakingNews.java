package com.ohmuk.folitics.hibernate.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.ohmuk.folitics.enums.ComponentType;
import com.ohmuk.folitics.hibernate.entity.attachment.Attachment;
import com.ohmuk.folitics.hibernate.entity.poll.Poll;
import com.ohmuk.folitics.util.DateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

/**
 * @author Asif 
 * @email asif633@gmail.com
 * This class is entity of breaking news section comes on top of home page as scrolling.
 * Code reference has been taken from Sentiment; because both of them has almost same dependencies.
 */
@Entity
@Table(name = "breaking_news")
@PrimaryKeyJoinColumn(name = "id")
@JsonIdentityInfo(generator = JSOGGenerator.class, property = "@id")
public class BreakingNews extends Component implements Serializable {
	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	@Column(nullable = false, length = 512)
	@NotNull(message = "error.breaking_news.subject.notNull")
	@Size(min = 1, max = 512, message = "error.sentiment.subject.size")
	private String subject;

	@Column(nullable = false, length = 1000)
	@NotNull(message = "error.breaking_news.description.notNull")
	@Size(min = 1, max = 1000, message = "error.breaking_news.description.size")
	private String description;

	@Column(nullable = false)
	@NotNull(message = "error.breaking_news.startTime.notNull")
	// @Future(message = "error.sentiment.startTime.future")
	private Timestamp startTime;

	@Column(nullable = false)
	@NotNull(message = "error.breaking_news.endTime.notNull")
	// @Future(message = "error.sentiment.endTime.future")
	private Timestamp endTime;

	@Lob
	@NotNull(message = "error.breaking_news.image.notNull")
	private byte[] image;

	@Column(nullable = false, length = 25, columnDefinition = "enum('png','jpg','jpeg')")
	@NotNull(message = "error.breaking_news.imageType.notNull")
	private String imageType;

	@Column(length = 25, columnDefinition = "enum('New','Alive','Hidden','Expired','Edited','Deleted')")
	private String state;

	@Column(nullable = false, length = 25, columnDefinition = "enum('Temporary','Permanent','Current','Other')")
	@NotNull(message = "error.breaking_news.type.notNull")
	private String type;

	@Column(nullable = true)
	private String keywords;
	
	@Column(nullable = true)
	private String feedSources;

	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToMany
	// (fetch = FetchType.EAGER)
	@Cascade(value = CascadeType.ALL)
	@JoinTable(name = "breaking_newscategory", joinColumns = { @JoinColumn(name = "breaking_newsId", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "categoryId", referencedColumnName = "id") })
	@NotNull(message = "error.breaking_news.categories.notNull")
	private Set<Category> categories;

//	@LazyCollection(LazyCollectionOption.FALSE)
//	@OneToMany(mappedBy = "breaking_news", fetch = FetchType.LAZY)
//	@Cascade(value = CascadeType.ALL)
//	// news list of breaking news entity
//	private List<BreakingNewsNews> breaking_newsNews;

//	public List<BreakingNewsNews> getBreakingNewsNews() {
//		return breaking_newsNews;
//	}
//
//	public void setBreakingNewsNews(List<BreakingNewsNews> breaking_newsNews) {
//		this.breaking_newsNews = breaking_newsNews;
//	}

	public BreakingNews() {
		setComponentType(ComponentType.SENTIMENT.getValue());
		setCreateTime(DateUtils.getSqlTimeStamp());
		setEditTime(DateUtils.getSqlTimeStamp());
	}

	public BreakingNews(String subject) {
		this.subject = subject;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getImageType() {
		return imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public Timestamp getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Set<Category> getCategories() {
		return categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}


	public void addCategory(Category category) {
		if (category != null) {
			categories.add(category);
		}
	}

	public void removeCategory(Category category) {
		if (category != null) {
			categories.remove(category);
		}
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getFeedSources() {
		return feedSources;
	}

	public void setFeedSources(String feedSources) {
		this.feedSources = feedSources;
	}

}
