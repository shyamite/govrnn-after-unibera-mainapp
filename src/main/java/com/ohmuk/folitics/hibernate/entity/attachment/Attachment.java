package com.ohmuk.folitics.hibernate.entity.attachment;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.ohmuk.folitics.hibernate.entity.Sentiment;
import com.ohmuk.folitics.util.DateUtils;

/**
 * The persistent class for the attachment database table.
 * 
 * @author Jahid Ali
 */
@Entity
@Table(name = "attachment")
public class Attachment implements Serializable, Comparable<Attachment> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;

	@Column(length = 25, columnDefinition = "enum('Image','Audio','Video', 'File','Pdf')")
	private String attachmentType;

	@Column(nullable = true, length = 500)
	private String description;

	@Column(length = 200)
	private String filePath;

//	@Column(nullable = false, length = 45, columnDefinition = "enum('png', 'jpg', 'jpeg', 'gif', 'mp3', 'wma', 'wav', 'avi', 'mkv', 'movv', 'mp4', 'divx', 'flv', 'ogg', 'ogv',"
//			+ " 'mpeg-4', 'mpeg', 'vob', 'wmv', '3gp', 'pdf', 'article', 'research paper', 'webpage','discussion paper','press release','website','factsheet','File','file')")
	private String fileType;

	@Column(nullable = true, length = 200)
	private String title;

	@Column(nullable = true, length = 200)
	private String provider;

	// bi-directional many-to-one association to Attachmentfile
	@ManyToOne(fetch = FetchType.EAGER)
	@Cascade(value = CascadeType.ALL)
	@JoinColumn(name = "fileId")
	private AttachmentFile attachmentFile;

	@Column(nullable = false)
	@NotNull(message = "error.attachment.editTime.notNull")
	private Timestamp editTime;

	@Column
	private Long editedBy;

	@Column(nullable = false)
	@NotNull(message = "error.attachment.createTime.notNull")
	private Timestamp createTime;

	@Column
	private Long created_By;
	
//	@LazyCollection(LazyCollectionOption.FALSE)
//	@ManyToOne(fetch = FetchType.EAGER)
//	@JoinColumn(nullable = true, name = "sentimentId", referencedColumnName = "id")
//	private Sentiment sentiment;

	public Attachment() {
		setCreateTime(DateUtils.getSqlTimeStamp());
		setEditTime(DateUtils.getSqlTimeStamp());
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the attachmentType
	 */
	public String getAttachmentType() {
		return attachmentType;
	}

	/**
	 * @param attachmentType
	 *            the attachmentType to set
	 */
	public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @param filePath
	 *            the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * @return the fileType
	 */
	public String getFileType() {
		return fileType;
	}

	/**
	 * @param fileType
	 *            the fileType to set
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the attachmentFile
	 */
	public AttachmentFile getAttachmentFile() {
		return attachmentFile;
	}

	/**
	 * @param attachmentFile
	 *            the attachmentFile to set
	 */
	public void setAttachmentFile(AttachmentFile attachmentFile) {
		this.attachmentFile = attachmentFile;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	/**
	 * @return the editTime
	 */
	public Timestamp getEditTime() {
		return editTime;
	}

	/**
	 * @param editTime
	 *            the editTime to set
	 */
	public void setEditTime(Timestamp editTime) {
		this.editTime = editTime;
	}

	/**
	 * @return the editedBy
	 */
	public Long getEditedBy() {
		return editedBy;
	}

	/**
	 * @param editedBy
	 *            the editedBy to set
	 */
	public void setEditedBy(Long editedBy) {
		this.editedBy = editedBy;
	}

	/**
	 * @return the createTime
	 */
	public Timestamp getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime
	 *            the createTime to set
	 */
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return the created_By
	 */
	public Long getCreated_By() {
		return created_By;
	}

	/**
	 * @param created_By
	 *            the created_By to set
	 */
	public void setCreated_By(Long created_By) {
		this.created_By = created_By;
	}
	
//	public Sentiment getSentiment() {
//		return sentiment;
//	}
//
//	public void setSentiment(Sentiment sentiment) {
//		this.sentiment = sentiment;
//	}

	@Override
	public int compareTo(Attachment o) {
		if (getCreateTime() == null || o.getCreateTime() == null)
			return 0;
		return getCreateTime().compareTo(o.getCreateTime());

	}
}