package com.ohmuk.folitics.ouput.model;

import java.io.File;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.ohmuk.folitics.charting.beans.OpinionAggregation;
import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.Sentiment;
import com.ohmuk.folitics.util.DateUtils;
import com.ohmuk.folitics.util.FileLoadingUtils;
import com.ohmuk.folitics.util.ImageUtil;

public class SentimentOutputModel extends BaseOutputModel<Opinion> implements Serializable {
	protected static final Logger LOGGER = LoggerFactory.getLogger(SentimentOutputModel.class);

	private static final long serialVersionUID = 1L;

	private int sentimentId;

	private String type;
	private String sentimentType;

	private String subject;

	private String keywords;

	private String feedSources;

	private String relatedSentimentName;
	

	private String state;
	private boolean enabled;
	private Timestamp endTime;
	
	public Timestamp getEndTime() {
		return endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	private boolean active;

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	private List<RelatedSentimentOutPutModal> relatedSentiments;

	private String timeLine;

	private String imageUrl;

	byte[] image;

	private OpinionAggregation opinionAggregation;
	
	private String description;

	public int getSentimentId() {
		return sentimentId;
	}

	public void setSentimentId(int sentimentId) {
		this.sentimentId = sentimentId;
	}

	public String getRelatedSentimentName() {
		return relatedSentimentName;
	}

	public void setRelatedSentimentName(String relatedSentimentName) {
		this.relatedSentimentName = relatedSentimentName;
	}

	private List<CategoryOutputModel> categories = new ArrayList<CategoryOutputModel>();
	private List<CategoryOutputModel> categoryOutputModels = new ArrayList<CategoryOutputModel>();
	private Set<String> bingImages = new HashSet<>();

	public Set<String> getBingImages() {
		return bingImages;
	}

	public void setBingImages(Set<String> bingImages) {
		this.bingImages = bingImages;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getSentimentType() {
		return sentimentType;
	}

	public void setSentimentType(String sentimentType) {
		this.sentimentType = sentimentType;
	}

	@Override
	public String toString() {
		return "SentimentModel [image=" + Arrays.toString(image) + ", toString()=" + super.toString() + "]";
	}

	public SentimentOutputModel() {
		super();
	}

	public SentimentOutputModel(Long id, String title, String type, UIMetrics uiMetrics, List<Opinion> otherAttributes,
			String fromattedAge) {
		super(id, title, type, uiMetrics, otherAttributes, fromattedAge);
		// TODO Auto-generated constructor stub
	}

	private static String getPreviewImageUrl(String sentimentName, String imgType) {
		StringBuilder sb = new StringBuilder();
		sb.append(FileLoadingUtils.BASE_IMAGE_PATH_SENTIMENT_LIST);
		sb.append(File.separator);
		sb.append(sentimentName);
		sb.append("."+imgType);
		return sb.toString();
	}

	public static final SentimentOutputModel getModel(Sentiment entity) {
		SentimentOutputModel model = new SentimentOutputModel(entity.getId(), null, entity.getType(), null, null, null);
//		model.setImageUrl(getPreviewImageUrl(StringUtils.trimAllWhitespace(entity.getSubject()),entity.getImageType()));
//		String fullFilePath = FileLoadingUtils.BASE_IMAGE_PATH + model.getImageUrl();
//		LOGGER.info("fullFilePath : " + fullFilePath);
//		File f = new File(fullFilePath);
//		if (!f.exists()) {
//			LOGGER.info("Image not found : " + fullFilePath);
//			model.setImageUrl(null);
//			model.setImage(ImageUtil.cropImage(entity.getImage(), 130, 150));
//		}
		model.setImage(entity.getImage());
		model.setImageFileType(entity.getImageType());
		model.setType(entity.getType());
		model.setSentimentType(entity.getType());
		model.setBingImages(entity.getBingImages());
		model.setSubject(entity.getSubject());
		model.setFormattedAge(entity.getFormattedAge());
		model.setTimeLine(DateUtils.getTimeLine(entity.getCreateTime()));
		model.setKeywords(entity.getKeywords());
		model.setFeedSources(entity.getFeedSources());
		model.setEndTime(entity.getEndTime());
		//model.setState(entity.getState());
		model.setState(entity.getState());
		model.setActive(entity.isActive());
		model.setEnabled(entity.isEnabled());
		model.setSentimentType(entity.getType());
		model.setDescription(entity.getDescription());

		for (Category category : entity.getCategories()) {
			model.getCategories().add(CategoryOutputModel.getModel(category));
			model.getCategoryOutputModels().add(CategoryOutputModel.getModel(category));
		}
		Set<Sentiment> retlSentiments = entity.getRelatedSentiments();
		if (retlSentiments != null) {
			List<RelatedSentimentOutPutModal> relatedSentiments = new ArrayList<RelatedSentimentOutPutModal>();
			for (Sentiment s : retlSentiments) {
				RelatedSentimentOutPutModal relatedSentimentOutPutModal = new RelatedSentimentOutPutModal();
				relatedSentimentOutPutModal.setId(s.getId());
				relatedSentimentOutPutModal.setName(s.getSubject());
				relatedSentiments.add(relatedSentimentOutPutModal);
				model.setRelatedSentimentName(s.getSubject());
				break;
			}
			model.setRelatedSentiments(relatedSentiments);
		}
		return model;
	}

	public static final SentimentOutputModel getIdAndNameModel(Sentiment entity) {
		SentimentOutputModel model = new SentimentOutputModel(entity.getId(), null, entity.getType(), null, null, null);
		model.setType(entity.getType());
		model.setSentimentType(entity.getType());
		model.setSubject(entity.getSubject());
		model.setFormattedAge(entity.getFormattedAge());
		model.setTimeLine(DateUtils.getTimeLine(entity.getCreateTime()));
		model.setKeywords(entity.getKeywords());
		model.setFeedSources(entity.getFeedSources());
		return model;
	}

	/**
	 * @return the opinionAggregation
	 */
	public OpinionAggregation getOpinionAggregation() {
		return opinionAggregation;
	}

	/**
	 * @param opinionAggregation
	 *            the opinionAggregation to set
	 */
	public void setOpinionAggregation(OpinionAggregation opinionAggregation) {
		this.opinionAggregation = opinionAggregation;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject
	 *            the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
		/**
	 * @return the categoryOutputModels
	 */
	public List<CategoryOutputModel> getCategoryOutputModels() {
		return categoryOutputModels;
	}

	/**
	 * @param categoryOutputModels
	 *            the categoryOutputModels to set
	 */
	public void setCategoryOutputModels(List<CategoryOutputModel> categoryOutputModels) {
		this.categoryOutputModels = categoryOutputModels;
	}
	/**
	 * @return the categoryOutputModels
	 */
	public List<CategoryOutputModel> getCategories() {
		return categories;
	}

	/**
	 * @param categoryOutputModels
	 *            the categoryOutputModels to set
	 */
	public void setCategories(List<CategoryOutputModel> categories) {
		this.categories = categories;
	}

	/**
	 * @return the relatedSentiments
	 */
	public List<RelatedSentimentOutPutModal> getRelatedSentiments() {
		return relatedSentiments;
	}

	/**
	 * @param relatedSentiments
	 *            the relatedSentiments to set
	 */
	public void setRelatedSentiments(List<RelatedSentimentOutPutModal> relatedSentiments) {
		this.relatedSentiments = relatedSentiments;
	}

	/**
	 * @return the timeLine
	 */
	public String getTimeLine() {
		return timeLine;
	}

	/**
	 * @param timeLine
	 *            the timeLine to set
	 */
	public void setTimeLine(String timeLine) {
		this.timeLine = timeLine;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getFeedSources() {
		return feedSources;
	}

	public void setFeedSources(String feedSources) {
		this.feedSources = feedSources;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
