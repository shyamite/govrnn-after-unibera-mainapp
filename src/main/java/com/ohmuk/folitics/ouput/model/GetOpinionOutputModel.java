/**
 * 
 */
package com.ohmuk.folitics.ouput.model;

import java.io.Serializable;
import java.util.List;

import com.ohmuk.folitics.hibernate.entity.Link;
import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.attachment.Attachment;

/**
 * @author Kalpana
 *
 */
public class GetOpinionOutputModel extends BaseOutputModel<String> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String subject;
	private String state;
	private String text;
	private Attachment attachment;
	private Link link;
	private SentimentOutputModel sentimentOutputModel;
	private UserOutputModel userOutputModel;
	private PollOutputModel agreeDisagreePoll;
	private String imageUrl;
	
	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the attachment
	 */
	public Attachment getAttachment() {
		return attachment;
	}

	/**
	 * @param attachment the attachment to set
	 */
	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}

	/**
	 * @return the sentimentOutputModel
	 */
	public SentimentOutputModel getSentimentOutputModel() {
		return sentimentOutputModel;
	}

	/**
	 * @param sentimentOutputModel the sentimentOutputModel to set
	 */
	public void setSentimentOutputModel(SentimentOutputModel sentimentOutputModel) {
		this.sentimentOutputModel = sentimentOutputModel;
	}

	/**
	 * @return the userOutputModel
	 */
	public UserOutputModel getUserOutputModel() {
		return userOutputModel;
	}

	/**
	 * @param userOutputModel the userOutputModel to set
	 */
	public void setUserOutputModel(UserOutputModel userOutputModel) {
		this.userOutputModel = userOutputModel;
	}

	/**
	 * @return the agreeDisagreePoll
	 */
	public PollOutputModel getAgreeDisagreePoll() {
		return agreeDisagreePoll;
	}

	/**
	 * @param agreeDisagreePoll the agreeDisagreePoll to set
	 */
	public void setAgreeDisagreePoll(PollOutputModel agreeDisagreePoll) {
		this.agreeDisagreePoll = agreeDisagreePoll;
	}
	
	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public GetOpinionOutputModel(Long id, String title, String type,
			UIMetrics uiMetrics, List<String> otherAttributes,
			String fromattedAge) {
		super(id, title, type, uiMetrics, otherAttributes, fromattedAge);
	}
	
	public static final GetOpinionOutputModel getModel(Opinion entity) {
		GetOpinionOutputModel model = new GetOpinionOutputModel(entity.getId(),
				null, entity.getType(), null, null, entity.getFormattedAge());
		model.setSubject(entity.getSubject());
		model.setState(entity.getState());
		model.setText(entity.getText());
		model.setAttachment(entity.getAttachment());
		model.setLink(entity.getLink());
		if(entity.getViews()!=null)
		model.setViews(entity.getViews()+1);
		else
			model.setViews(1L);
		if(entity.getSentiment() != null)
			model.setSentimentOutputModel(SentimentOutputModel.getModel(entity.getSentiment()));
		if(entity.getUser() != null)
			model.setUserOutputModel(UserOutputModel.getModel(entity.getUser()));
		if(entity.getAgreeDisagrePoll() != null)
			model.setAgreeDisagreePoll(PollOutputModel.getModel(entity.getAgreeDisagrePoll()));
		
		model.setImageUrl(entity.getImageUrl());
		return model;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "OpinionBeanOutputModel [subject=" + subject + ", state="
				+ state + ", text=" + text + ", attachment=" + attachment
				+ ", sentimentOutputModel=" + sentimentOutputModel
				+ ", userOutputModel=" + userOutputModel
				+ ", agreeDisagreePoll=" + agreeDisagreePoll + "]";
	}

	/**
	 * @return the link
	 */
	public Link getLink() {
		return link;
	}

	/**
	 * @param link the link to set
	 */
	public void setLink(Link link) {
		this.link = link;
	}

	
	
}
