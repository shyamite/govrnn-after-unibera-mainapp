package com.ohmuk.folitics.ouput.model;

import java.io.Serializable;

import com.ohmuk.folitics.hibernate.entity.Opinion;

public class OpinionNotificationOutputModel extends BaseOutputModel<Opinion> implements Serializable {
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    private String category;

    private String notificationId;

    private String status;

    private UserOutputModel user;

    private String url;

    private String imageUrl;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public UserOutputModel getUser() {
        return user;
    }

    public void setUser(UserOutputModel user) {
        this.user = user;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
