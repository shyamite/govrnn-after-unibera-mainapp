package com.ohmuk.folitics.ouput.model;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.ohmuk.folitics.charting.beans.OpinionAggregation;
import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.BreakingNews;
import com.ohmuk.folitics.util.DateUtils;
import com.ohmuk.folitics.util.FileLoadingUtils;
import com.ohmuk.folitics.util.ImageUtil;

/**
 * @author Asif
 *
 */
public class BreakingNewsOutputModel extends BaseOutputModel<Opinion> implements Serializable {
	protected static final Logger LOGGER = LoggerFactory.getLogger(BreakingNewsOutputModel.class);

	private static final long serialVersionUID = 1L;

	private int breakingnewsId;

	private String breakingnewsType;

	private String subject;

	private String keywords;

	private String feedSources;

	private String relatedBreakingNewsName;

	private String state;


	private String timeLine;

	private String imageUrl;

	byte[] image;

	private OpinionAggregation opinionAggregation;
	
	private String description;

	public int getBreakingNewsId() {
		return breakingnewsId;
	}

	public void setBreakingNewsId(int breakingnewsId) {
		this.breakingnewsId = breakingnewsId;
	}

	public String getRelatedBreakingNewsName() {
		return relatedBreakingNewsName;
	}

	public void setRelatedBreakingNewsName(String relatedBreakingNewsName) {
		this.relatedBreakingNewsName = relatedBreakingNewsName;
	}

	private List<CategoryOutputModel> categoryOutputModels = new ArrayList<CategoryOutputModel>();

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getBreakingNewsType() {
		return breakingnewsType;
	}

	public void setBreakingNewsType(String breakingnewsType) {
		this.breakingnewsType = breakingnewsType;
	}

	@Override
	public String toString() {
		return "BreakingNewsModel [image=" + Arrays.toString(image) + ", toString()=" + super.toString() + "]";
	}

	public BreakingNewsOutputModel() {
		super();
	}

	public BreakingNewsOutputModel(Long id, String title, String type, UIMetrics uiMetrics, List<Opinion> otherAttributes,
			String fromattedAge) {
		super(id, title, type, uiMetrics, otherAttributes, fromattedAge);
		// TODO Auto-generated constructor stub
	}

	private static String getPreviewImageUrl(String breakingnewsName, String imgType) {
		StringBuilder sb = new StringBuilder();
		sb.append(FileLoadingUtils.BASE_IMAGE_PATH_SENTIMENT_LIST);
		sb.append(File.separator);
		sb.append(breakingnewsName);
		sb.append("."+imgType);
		return sb.toString();
	}

	public static final BreakingNewsOutputModel getModel(BreakingNews entity) {
		BreakingNewsOutputModel model = new BreakingNewsOutputModel(entity.getId(), null, entity.getType(), null, null, null);
		model.setImageUrl(getPreviewImageUrl(StringUtils.trimAllWhitespace(entity.getSubject()),entity.getImageType()));
		String fullFilePath = FileLoadingUtils.BASE_IMAGE_PATH + model.getImageUrl();
		LOGGER.info("fullFilePath : " + fullFilePath);
		File f = new File(fullFilePath);
		if (!f.exists()) {
			LOGGER.info("Image not found : " + fullFilePath);
			model.setImageUrl(null);
			model.setImage(ImageUtil.cropImage(entity.getImage(), 130, 150));
		}
		model.setImageFileType(entity.getImageType());
		model.setBreakingNewsType(entity.getType());
		model.setSubject(entity.getSubject());
		model.setFormattedAge(entity.getFormattedAge());
		model.setTimeLine(DateUtils.getTimeLine(entity.getCreateTime()));
		model.setKeywords(entity.getKeywords());
		model.setFeedSources(entity.getFeedSources());
		model.setState(entity.getState());
		model.setDescription(entity.getDescription());

		for (Category category : entity.getCategories()) {
			model.getCategoryOutputModels().add(CategoryOutputModel.getModel(category));
		}
		
		return model;
	}

	public static final BreakingNewsOutputModel getIdAndNameModel(BreakingNews entity) {
		BreakingNewsOutputModel model = new BreakingNewsOutputModel(entity.getId(), null, entity.getType(), null, null, null);
		model.setBreakingNewsType(entity.getType());
		model.setSubject(entity.getSubject());
		model.setFormattedAge(entity.getFormattedAge());
		model.setTimeLine(DateUtils.getTimeLine(entity.getCreateTime()));
		model.setKeywords(entity.getKeywords());
		model.setFeedSources(entity.getFeedSources());
		return model;
	}

	/**
	 * @return the opinionAggregation
	 */
	public OpinionAggregation getOpinionAggregation() {
		return opinionAggregation;
	}

	/**
	 * @param opinionAggregation
	 *            the opinionAggregation to set
	 */
	public void setOpinionAggregation(OpinionAggregation opinionAggregation) {
		this.opinionAggregation = opinionAggregation;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject
	 *            the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the categoryOutputModels
	 */
	public List<CategoryOutputModel> getCategoryOutputModels() {
		return categoryOutputModels;
	}

	/**
	 * @param categoryOutputModels
	 *            the categoryOutputModels to set
	 */
	public void setCategoryOutputModels(List<CategoryOutputModel> categoryOutputModels) {
		this.categoryOutputModels = categoryOutputModels;
	}

	/**
	 * @return the timeLine
	 */
	public String getTimeLine() {
		return timeLine;
	}

	/**
	 * @param timeLine
	 *            the timeLine to set
	 */
	public void setTimeLine(String timeLine) {
		this.timeLine = timeLine;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getFeedSources() {
		return feedSources;
	}

	public void setFeedSources(String feedSources) {
		this.feedSources = feedSources;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
