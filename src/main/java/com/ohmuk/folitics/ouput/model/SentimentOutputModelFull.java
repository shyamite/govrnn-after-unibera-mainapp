package com.ohmuk.folitics.ouput.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.util.StringUtils;

import com.google.common.collect.Sets;
import com.ohmuk.folitics.cache.SingletonCache;
import com.ohmuk.folitics.charting.beans.OpinionAggregation;
import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.Sentiment;
import com.ohmuk.folitics.hibernate.entity.SentimentNews;
import com.ohmuk.folitics.hibernate.entity.attachment.Attachment;
import com.ohmuk.folitics.hibernate.entity.poll.Poll;
import com.ohmuk.folitics.util.ImageUtil;
import com.ohmuk.folitics.util.NumberUtil;

public class SentimentOutputModelFull extends BaseOutputModel<Opinion>
		implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String state;

	private String imageType;

	private String subject;

	private String description;

	private List<CategoryOutputModel> categoryOutputModels = new ArrayList<CategoryOutputModel>();

	byte[] image;

	private OpinionAggregation opinionAggregation;

	private List<AttachmentOutputModel> references;

	private Set<SentimentOutputModelFull> relatedSentiments;

	private Set<SentimentOutputModelFull> relatedToSentiments;

	private Set<SentimentOutputModelFull> permanentSentiments;

	private Set<SentimentOutputModelFull> temporarySentiments;

	private List<SentimentNews> sentimentNews;

	private List<Poll> polls;

	private List<String> imageUrls;

	private String relatedSentimentName;
	
	public String getRelatedSentimentName() {
		return relatedSentimentName;
	}

	public void setRelatedSentimentName(String relatedSentimentName) {
		this.relatedSentimentName = relatedSentimentName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<AttachmentOutputModel> getReferences() {
		if(references!=null){
			Collections.sort(references);
		}
		return references;
	}

	public void setReferences(List<AttachmentOutputModel> references) {
		this.references = references;
	}

	public void prepareReferences(List<Attachment> attachments) {
		this.references = new ArrayList<AttachmentOutputModel>();
		if (attachments != null) {
			for (Attachment entity : attachments) {
				references.add(AttachmentOutputModel.getModel(entity, "Admin",
						0));
			}
		}
	}

	public void addReferences(Attachment entity, String provider, Long id) {
		if (references == null)
			this.references = new ArrayList<AttachmentOutputModel>();
		references.add(AttachmentOutputModel.getModel(entity, provider, id));

	}

	public void addReferences(Opinion entity) {
		if (this.references == null)
			this.references = new ArrayList<AttachmentOutputModel>();
		AttachmentOutputModel model = AttachmentOutputModel.getModel(entity);
		if (model != null) {
			references.add(model);
		}
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "SentimentModel [image=" + Arrays.toString(image)
				+ ", toString()=" + super.toString() + "]";
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getImageType() {
		return imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public Set<SentimentOutputModelFull> getRelatedSentiments() {
		return relatedSentiments;
	}

	public void setRelatedSentiments(
			Set<SentimentOutputModelFull> relatedSentiments) {
		this.relatedSentiments = relatedSentiments;
	}

	public Set<SentimentOutputModelFull> getRelatedToSentiments() {
		return relatedToSentiments;
	}

	public void setRelatedToSentiments(
			Set<SentimentOutputModelFull> relatedToSentiments) {
		this.relatedToSentiments = relatedToSentiments;
	}

	public Set<SentimentOutputModelFull> getPermanentSentiments() {
		return permanentSentiments;
	}

	public void setPermanentSentiments(
			Set<SentimentOutputModelFull> permanentSentiments) {
		this.permanentSentiments = permanentSentiments;
	}

	public Set<SentimentOutputModelFull> getTemporarySentiments() {
		return temporarySentiments;
	}

	public void setTemporarySentiments(
			Set<SentimentOutputModelFull> temporarySentiments) {
		this.temporarySentiments = temporarySentiments;
	}

	public List<Poll> getPolls() {
		return polls;
	}

	public void setPolls(List<Poll> polls) {
		this.polls = polls;
	}

	public List<SentimentNews> getSentimentNews() {
		return sentimentNews;
	}

	public void setSentimentNews(List<SentimentNews> sentimentNews) {
		for(SentimentNews sentimentNew:sentimentNews){
			sentimentNew.setSentiment(null);
		}
		this.sentimentNews = sentimentNews;
	}

	public SentimentOutputModelFull() {
		super();
	}

	public SentimentOutputModelFull(Long id, String title, String type,
			UIMetrics uiMetrics, List<Opinion> otherAttributes,
			String fromattedAge) {
		super(id, title, type, uiMetrics, otherAttributes, fromattedAge);
		// TODO Auto-generated constructor stub
	}

	public static final SentimentOutputModelFull getModel(Sentiment entity) {
		SentimentOutputModelFull model = new SentimentOutputModelFull(
				entity.getId(), null, entity.getType(), null, null, null);
//		try {
//			model.setImage(ThumbnailUtil.getSentimentImageThumbnail(
//					entity.getImage(), entity.getImageType()));
//		} catch (IOException e) {
//			model.setImage(entity.getImage());
//			e.printStackTrace();
//		}
		model.setImage(entity.getImage());
		model.setImageFileType(entity.getImageType());
		model.setType(entity.getType());
		model.setSubject(entity.getSubject());
		model.setDescription(entity.getDescription());
		model.setFormattedAge(entity.getFormattedAge());
		for (Category category : entity.getCategories()) {
			model.getCategoryOutputModels().add(
					CategoryOutputModel.getModel(category));
		}
		List<Poll> polls = new ArrayList<>();
		polls = entity.getPolls();
		Collections.sort(polls, Collections.reverseOrder());
		model.setPolls(entity.getPolls());
		List<SentimentNews> sentNews = new ArrayList<>();
		sentNews = entity.getSentimentNews();
		Collections.sort(sentNews, Collections.reverseOrder());
		model.setSentimentNews(sentNews);
		List<SentimentOutputModelFull> relatedSentiments = new ArrayList<SentimentOutputModelFull>();
		for(Sentiment s: entity.getRelatedSentiments()){
			relatedSentiments.add(SentimentOutputModelFull.getModel(s));
		}
		
		model.setRelatedSentiments(Sets.newHashSet(relatedSentiments));
		Set<SentimentOutputModelFull> retlSentiments =  model.getRelatedSentiments();
		if(retlSentiments!=null){
			for(SentimentOutputModelFull s:retlSentiments){
				model.setRelatedSentimentName(s.getSubject());
				break;
			}
		}
		//load the image urls after setting the related Sentiment name if its existing
		model.loadImageUrls();
		return model;
	}

	/**
	 * @return the opinionAggregation
	 */
	public OpinionAggregation getOpinionAggregation() {
		return opinionAggregation;
	}

	/**
	 * @param opinionAggregation
	 *            the opinionAggregation to set
	 */
	public void setOpinionAggregation(OpinionAggregation opinionAggregation) {
		this.opinionAggregation = opinionAggregation;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject
	 *            the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the categoryOutputModels
	 */
	public List<CategoryOutputModel> getCategoryOutputModels() {
		return categoryOutputModels;
	}

	/**
	 * @param categoryOutputModels
	 *            the categoryOutputModels to set
	 */
	public void setCategoryOutputModels(
			List<CategoryOutputModel> categoryOutputModels) {
		this.categoryOutputModels = categoryOutputModels;
	}

	private void loadImageUrls(){
		this.imageUrls = new ArrayList<String>();
		Map<Integer, String> imgIndexMap= new HashMap<Integer, String>(); 
		String sentimentNameToLoadImageUrls = this.relatedSentimentName;
		if(StringUtils.isEmpty(sentimentNameToLoadImageUrls )){
			sentimentNameToLoadImageUrls = this.subject; 
		}
		if (!StringUtils.isEmpty(sentimentNameToLoadImageUrls)) {
			List<String> imageUrlsAll = (List<String>) SingletonCache
					.getInstance().get(sentimentNameToLoadImageUrls);
			if (imageUrlsAll != null && !imageUrlsAll.isEmpty()) {
				int totalNumberOfImages = imageUrlsAll.size();
				if (imageUrlsAll.size() <= ImageUtil.MAX_SENTIMENT_IMAGES) {
					imageUrls.addAll(imageUrlsAll);
				} else {
					for (int i = 0; i < ImageUtil.MAX_SENTIMENT_IMAGES; i++) {
						if(i==ImageUtil.MAX_SENTIMENT_IMAGES) break;
						Integer index=NumberUtil
								.getRandomNumber(totalNumberOfImages);
						String url = imgIndexMap.get(index);
						if(url==null){
							imgIndexMap.put(index,imageUrlsAll.get(index));
							imageUrls.add(imageUrlsAll.get(index));
						}
						else{
							i--;
						}
					}
				}
			}
		}
	}
	public List<String> getImageUrls() {
//		if(imageUrls==null){
//			imageUrls = new ArrayList<String>();
//			imageUrls.add("/images/sentiments/Communalism/img1.jpg");
//			imageUrls.add("/images/sentiments/Communalism/img2.jpg");
//			imageUrls.add("/images/sentiments/Communalism/img3.jpg");
//			imageUrls.add("/images/sentiments/Communalism/img4.jpg");
//			imageUrls.add("/images/sentiments/Communalism/img5.jpg");
//			imageUrls.add("/images/sentiments/Communalism/img6.jpg");
//			imageUrls.add("/images/sentiments/Communalism/img7.jpg");
//			imageUrls.add("/images/sentiments/Communalism/img8.jpg");
//			imageUrls.add("/images/sentiments/Communalism/img9.jpg");
//			imageUrls.add("/images/sentiments/Communalism/img10.jpg");
//			imageUrls.add("/images/sentiments/Communalism/img11.jpg");
//			imageUrls.add("/images/sentiments/Communalism/img12.jpg");
//			imageUrls.add("/images/sentiments/Communalism/img13.jpg");
//			imageUrls.add("/images/sentiments/Communalism/img14.jpg");
//			imageUrls.add("/images/sentiments/Communalism/img15.jpg");
//		}
		return imageUrls;
	}

	public void setImageUrls(List<String> imageUrls) {
		this.imageUrls = imageUrls;
	}

}
