'use strict';

/**
 * @ngdoc directive
 * @name govrnnApp.directive:myDirective
 * @description
 * # myDirective
 */
angular.module('govrnnApp')
  .directive('newsModal', function () {
    return {
      templateUrl: 'templates/modal_news.html',
	  restrict: 'E',
      link: function postLink(scope, element, attrs) {},
	  controller: ['$scope','$http','$rootScope','$location', function ($scope, $http, $rootScope,$location) {
		  
		 $scope.newsSource = "";
		 $scope.isAggregationPresent=false;
		 $scope.rateNews= {};
		 $scope.newsAggregation="";
		 
		 
		 $scope.showNewsModal = function(newsSource,componentId,componentType) {								    		
			  $scope.newsSource =  newsSource;
			 var  sourceId =  newsSource.replace(/\s/g, '');
			   $('#'+sourceId).addClass("active");
			   	if(componentType=="sentimentNews"){
				   $scope.rateNews.componentType= "sentimentNews";
				   $scope.rateNews.componentId = componentId;
			   }
			   	$http.get('/rating/getRatingAggregation?componentId='+componentId+'&componentType='+componentType).then(
						function(response){
							$scope.newsAggregation = response.data.messages[0];
							if($scope.newsAggregation != null){
								
								if($scope.newsAggregation.rateAggregaionMap.negative != undefined || $scope.newsAggregation.rateAggregaionMap.positive != undefined){


									$scope.isAggregationPresent=true;
							
									$scope.myJson = {
											globals: {
												shadow: false,
												fontFamily: "Verdana",
												fontWeight: "100"
											},
											type: "pie",
											title:{
												    text:"Negative Feel"
												  },
											 
											backgroundColor: "#fff",

											
											legend: {
												layout: "x5",
												borderColor: "transparent",
												marker: {
													borderRadius: 10,
													borderColor: "transparent"
												}									
											},
											tooltip: {
												text: "%v"+"%"+ "%t"
											},
											plot: {
												refAngle: "-90",
												borderWidth: "5px",
												valueBox: {
													placement: "in",
													text: "",
													fontSize: "15px",
													textAlpha: 1,
												} 	
											},	
											series:[
												 {
													text: "Sad(" +((($scope.newsAggregation.rateTotalMap[0]) != undefined) ? ($scope.newsAggregation.rateTotalMap[0]) : 0) +")",
													values: [(($scope.newsAggregation.rateTotalMap[0]) != undefined) ? ($scope.newsAggregation.rateTotalMap[0]) : 0],
													backgroundColor: "#128807"
												},
												{
													text: "Baised(" +((($scope.newsAggregation.rateTotalMap[1]) != undefined) ? ($scope.newsAggregation.rateTotalMap[1]) : 0) +")",
													values: [(($scope.newsAggregation.rateTotalMap[1]) != undefined) ? ($scope.newsAggregation.rateTotalMap[1]) : 0],
													backgroundColor: "#ff7305",
												},
												{
													text: "Advert(" +((($scope.newsAggregation.rateTotalMap[2]) != undefined) ? ($scope.newsAggregation.rateTotalMap[2]) : 0) +")",
													values: [(($scope.newsAggregation.rateTotalMap[2]) != undefined) ? ($scope.newsAggregation.rateTotalMap[2]) : 0],
													backgroundColor: "yellow",
												},
												{
													text: "Plagrism(" +((($scope.newsAggregation.rateTotalMap[3]) != undefined) ? ($scope.newsAggregation.rateTotalMap[3]) : 0) +")",
													values: [(($scope.newsAggregation.rateTotalMap[3]) != undefined) ? ($scope.newsAggregation.rateTotalMap[3]) : 0],
													backgroundColor: "Red",
												},
												{
													text: "Facts(" +((($scope.newsAggregation.rateTotalMap[4]) != undefined) ? ($scope.newsAggregation.rateTotalMap[4]) : 0) +")",
													values: [(($scope.newsAggregation.rateTotalMap[4]) != undefined) ? ($scope.newsAggregation.rateTotalMap[4]) : 0],
													backgroundColor: "Blue",
												}
												]
										};

									if($scope.newsAggregation.rateAggregaionMap.positive != undefined){

										$scope.myJson1 = {
												globals: {
													shadow: false,
													fontFamily: "Verdana",
													fontWeight: "100"
												},
												type: "pie",

												title:{
													    text:"Positive Feel"
													  },
												backgroundColor: "#fff",

												legend: {
													layout: "x5",
													borderColor: "transparent",
													marker: {
														borderRadius: 10,
														borderColor: "transparent"
													}									
												},
												tooltip: {
													text: "%v"+"%"+ "%t"
												},
												plot: {
													refAngle: "-90",
													borderWidth: "5px",
													valueBox: {
														placement: "in",
														text: "",
														fontSize: "15px",
														textAlpha: 1,
													} 	
												},	
												series:[
													 {
														text: "Format(" +((($scope.newsAggregation.rateTotalMap[5]) != undefined) ? ($scope.newsAggregation.rateTotalMap[5]) : 0) +")",
														values: [(($scope.newsAggregation.rateTotalMap[5]) != undefined) ? ($scope.newsAggregation.rateTotalMap[5]) : 0],
														backgroundColor: "#128807"
													},
													{
														text: "Interesting(" +((($scope.newsAggregation.rateTotalMap[6]) != undefined) ? ($scope.newsAggregation.rateTotalMap[6]) : 0) +")",
														values: [(($scope.newsAggregation.rateTotalMap[6]) != undefined) ? ($scope.newsAggregation.rateTotalMap[6]) : 0],
														backgroundColor: "#ff7305",
													},
													{
														text: "Informative(" +((($scope.newsAggregation.rateTotalMap[7]) != undefined) ? ($scope.newsAggregation.rateTotalMap[7]) : 0) +")",
														values: [(($scope.newsAggregation.rateTotalMap[7]) != undefined) ? ($scope.newsAggregation.rateTotalMap[7]) : 0],
														backgroundColor: "yellow",
													},
													{
														text: "Attractive(" +((($scope.newsAggregation.rateTotalMap[8]) != undefined) ? ($scope.newsAggregation.rateTotalMap[8]) : 0) +")",
														values: [(($scope.newsAggregation.rateTotalMap[8]) != undefined) ? ($scope.newsAggregation.rateTotalMap[8]) : 0],
														backgroundColor: "Red",
													},
													{
														text: "Ethical(" +((($scope.newsAggregation.rateTotalMap[9]) != undefined) ? ($scope.newsAggregation.rateTotalMap[9]) : 0) +")",
														values: [(($scope.newsAggregation.rateTotalMap[9]) != undefined) ? ($scope.newsAggregation.rateTotalMap[9]) : 0],
														backgroundColor: "Blue",
													}
													]
											};



									}

									$scope.myJson2 = {
											  type: "line",
											  scaleX:{
												    labels:["Sun","Mon","Tue","Wed","Thr","Fri","Sat"],
												 },
												 scaleY:{
													    values:"-10:10:2",
													    "label":{
													      text:"Positive |   Negative",
													      fontSize:14,
													      offsetX:-10
													    },
												 },
												 plotarea: {
										                margin: "dynamic 45 60 dynamic",
										            },
										            plot: {
										                aspect : "spline"
										            },
											  series: [
											    {
											      values: [
											               (($scope.newsAggregation.feelAggregation[1] != undefined)? $scope.newsAggregation.feelAggregation[1]: 0),
											               (($scope.newsAggregation.feelAggregation[2] != undefined)? $scope.newsAggregation.feelAggregation[2]: 0),
											               (($scope.newsAggregation.feelAggregation[3] != undefined)? $scope.newsAggregation.feelAggregation[3]: 0),
											               (($scope.newsAggregation.feelAggregation[4] != undefined)? $scope.newsAggregation.feelAggregation[4]: 0),
											               (($scope.newsAggregation.feelAggregation[5] != undefined)? $scope.newsAggregation.feelAggregation[5]: 0),
											               (($scope.newsAggregation.feelAggregation[6] != undefined)? $scope.newsAggregation.feelAggregation[6]: 0),
											               (($scope.newsAggregation.feelAggregation[7] != undefined)? $scope.newsAggregation.feelAggregation[7]: 0),
											               ]
											    },
											  ]
										};

								
								}else{
									$scope.isAggregationPresent = false;
									$scope.myJson  ="";
									$scope.myJson1  ="";
									$scope.myJson2  ="";
								}
								
							}else{
								$scope.isAggregationPresent = false;
								$scope.myJson  ="";
								$scope.myJson1  ="";
								$scope.myJson2  ="";
							}
							
					});
			   	$('#news_pop').modal("show");
       	 }
   	 
	  }]
    };	
  });
