var myapp = angular.module('govrnnApp');
myapp
.directive(
'myelem',
[
		'$http',
		'$q',
		function($http, $q) {

			var directiveData, dataPromise;
			function initChart($scope) {

				data1 = $scope.data.messages[0].data;
				// alert('chart');
				// alert(JSON.stringify(data1));

				var chart = false;
				if (chart)
					chart.destroy();
				var config = $scope.config || {};
				chart = AmCharts
				.makeChart(
				"gpidiv",
				{

					type : "stock",
					pathToImages: "",
					"theme" : "none",
					"dataDateFormat" : "YYYY-MM-DD",
					addClassNames : true,
					startDuration : 1,
					"precision" : 2,
					categoryAxesSettings : {
						minPeriod : "hh"
					},

					dataSets : [ {
						color : "#b0de09",
						fieldMappings : [ 
						{
							fromField : "score",
							toField : "score"
						}, 
						{
							fromField : "timestamp",
							toField : "timestamp"
						} ],
						dataProvider : $scope.data.messages[0].data,
						categoryField : "timestamp"
					} ],

					panels : [ {
						showCategoryAxis : true,
						title : "Govt Popularity Perception",
						eraseAll : false,

						"export" : {
							"enabled" : false
						},

						stockGraphs : [ {
							id : "g1",
							valueAxis : "v12",
							valueField : "score",
							"bullet" : "round",
							"bulletBorderAlpha" : 0,
							"bulletColor" : "#EF2D27",
							"bulletSizeField" : "milestone",
							"customBulletField" : "url",
							"hideBulletsCount" : 50,
							"lineThickness" : 1,
							"lineColor" : "#EF2D27",
							"type" : "smoothedLine",
							"title" : "",
							"useLineColorForBulletBorder" : true,
							"valueField" : "score",
							"urlField" : "url",
							//"balloonText" : "<div class='col-md-4'><img style='width: 30px; height: 40px;' src='data:image/JPEG;base64,[[image]]' alt='milestone image'> </div> <div class='col-md-8'><b style='font-size: 130%'>[[milestoneType]]</b></br>[[description]]</div>",

							useDataSetColors : false
						} ],

						stockLegend : {
							valueTextRegular : " ",
							markerType : "none"
						},

					} ],

					chartScrollbarSettings : {
						graph : "g1"
					},
					chartCursorSettings : {
						valueBalloonsEnabled : true
					},
					periodSelector : {
						position : "top",
						periods : [ {
							period : "DD",
							count : 1,
							label : "1D"
						}, {
							period : "DD",
							count : 5,
							label : "5D"
						},{
							period : "MM",
							count : 1,
							label : "1M"
						},{
							period : "MM",
							count : 3,
							label : "3M"
						},{
							period : "MM",
							count : 6,
							label : "6M"
						},  {
							period : "YTD",
							label : "YTD"
						},{
							period : "YYYY",
							count : 1,
							label : "1Y"
						}, {
							period : "YYYY",
							count : 2,
							label : "2Y"
						},{
							period : "YYYY",
							count : 5,
							label : "5Y"
						},{
							period : "MAX",
							label : "MAX"
						} ]
					}
				});
				
				// chart.addListener('rendered' ,function (event){

			          //  $( ".amChartsPeriodSelector .amChartsInputField" ).datepicker({
			              //  dateFormat: "dd-mm-yy",
			                //minDate: newStartDate,
			                //maxDate: newEndDate,
			              //  onClose: function() {
			                //    $(".amChartsPeriodSelector .amChartsInputField" ).trigger('blur');
			              //  }
			            //});
			       // });
			}
			
			function loadData() {

				// alert('das');

				var deferred = $q.defer();
				dataPromise = deferred.promise;
				return $http
				.get(
				'/chart/getChart/?ChartID=GPIChartService&ChartSecondaryID=GPI&id=1')
				.then(function(response) {

					return response;
				}, function(response) {

				});

			}

			return {
				restrict : 'EA',
				scope : {
					data : '=?',
					title : '@'
				},
				template : '<div id="gpidiv" style="min-width: 100%; height: 380px; margin: 0 auto"></div>',
				link : function($scope, element, attrs) {

					var chart = false;

					i = 0;

					$scope.data1 = loadData().then(
					function(data) {

						// alert(JSON.stringify(data.data.messages[0].chartMeta));
						$scope.data = data.data;
						// alert('inside
						// '+JSON.stringify($scope.data));
						initChart($scope);
						$scope.$watch($scope.data, function(newValue,
						oldValue) {

							// alert('inside
							// '+JSON.stringify($scope.data));

							initChart($scope);

						}, true);

					}, function() {

					})

				}
			}
		} ]);


