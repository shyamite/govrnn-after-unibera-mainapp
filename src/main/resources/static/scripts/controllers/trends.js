'use strict';

/**
 * @ngdoc function
 * @name govrnnApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the govrnnApp
 */
angular.module('govrnnApp')
	.controller('TrendsCtrl', ['$http', '$scope','$localStorage', '$rootScope','$location','CreateTrendService','$sce',
		function ($http, $scope,$localStorage, $rootScope,$location,CreateTrendService,$sce) {

			var trendId = $location.search().id;
			$scope.id = trendId
			//$scope.name = trendName;
			$scope.trendMappings = [];
			$scope.trendMappingsOpinion = [];
			$scope.trendMappingsResponses = [];			
			$scope.trendMappingComments = {};
			$scope.trendCommentCount = {};
			$scope.trendMappingOpinionComments = {};
			$scope.trendMappingResponseComments = {};
			$scope.user ={};
			$scope.trendNames =[];
			$scope.commentsToshow="";

			
			$scope.initFirst=function()
			{
			
				
				/* $http.get('/user/find?id='+$scope.loggedInUser).then(function(response) {
						if (response.data.messages != null) {
							$scope.user = response.data.messages[0];	
						}
				 });*/
				$http.get('/trend/getTrendById?id='+$scope.id).then(function(response) {
					if (response.data.messages != null) {
						$scope.trend = response.data.messages[0];					}
				});

				// opinion mappings
				$http.get('/trend/getOpinionsByTrendId?trendId='+$scope.id+'&count=3').then(function(response) {
					if (response.data.messages != null) {
						$scope.trendMappingsOpinion = response.data.messages[0];
						for(var i=0;i<$scope.trendMappingsOpinion.length ;i++){
							//$scope.trendMappingsOpinion[i].text=$sce.trustAsHtml($scope.trendMappingsOpinion[i].text);
							$http.get('/comment/getAllCommentsForComponentId?componentType=trend&componentId='+$scope.trendMappingsOpinion[i].trendmappingId).then(function(response) {
								if (response.data.messages != null) {
									if(response.data.messages.length > 0){
										$scope.trendMappingOpinionComments[(response.data.messages[0]).trendMappingId] =   response.data.messages;
									}
								}
							});
						}
					}
				});
				
				//respons mappings
				$http.get('/trend/getResponsesByTrendId?trendId='+$scope.id+'&count=3').then(function(response) {
					if (response.data.messages != null) {
						$scope.trendMappingsResponses = response.data.messages[0];
						for(var i=0;i<$scope.trendMappingsResponses.length ;i++){
						//	$scope.trendMappingsResponses[i].text=$sce.trustAsHtml($scope.trendMappingsResponses[i].text);
							$http.get('/comment/getAllCommentsForComponentId?componentType=trend&componentId='+$scope.trendMappingsResponses[i].trendmappingId).then(function(response) {
								if (response.data.messages != null) {
									if(response.data.messages.length > 0){
										$scope.trendMappingResponseComments[(response.data.messages[0]).trendMappingId] =   response.data.messages;	
									}
								}
							});
						}
					}
				});
				
				$http.get('/trend/getTopTrendUsers?count=3').then(function(response) {
					if (response.data.messages != null) {
						$scope.topTrendUser = response.data.messages[0];

					}
				});

				$http.get('/trend/getTopTrends?count=3').then(function(response) {
					if (response.data.messages != null) {
						$scope.topTrends = response.data.messages[0];

						for(var i=0;i<$scope.topTrends.length;i++){

							$http.get('/trend/getTrendCommentCount?id='+$scope.topTrends[i].id).then(function(response) {
								if (response.data.messages != null) {
									setComentsCount(response.data.messages[0])
								}
							});
						}
					}
				});
			};
			
			function setComentsCount(comemntCount){
				
				for(var key in comemntCount){
					$scope.trendCommentCount[key] = comemntCount[key];
				}
			}
			
			$scope.comment = {
				componentType : "",
				componentId: "",
				userId : "", 
				comment : "",
				trendMappingId : ""
			}

			$scope.saveOpinionCommnet = function(trendMappingId, opinionId, usId, componentType){
			  if($scope.isUserLoggedIn() == true){
				var content= $("#Opinion_"+opinionId).val();

				$scope.comment.componentType = componentType;
				$scope.comment.componentId = trendMappingId;
				$scope.comment.userId = $scope.getLoggedInUserId();;
				$scope.comment.comment = content;
				$scope.comment.trendMappingId = trendMappingId;
				
				CreateTrendService.data1($scope.comment);
				$scope.initFirst();
				$scope.commentsToshow = trendMappingId;
				$("#ViewCommnet_"+trendMappingId).text("Hide All Comments");
			}else{
				$scope.loginNow();
			}

			};
			$scope.saveResponseCommnet = function(trendMappingId, responseId, userId, componentType){
				if($scope.isUserLoggedIn() == true){
				var content= $("#Response_"+responseId).val();

				$scope.comment.componentType = componentType;
				$scope.comment.componentId = trendMappingId;
				$scope.comment.userId = $scope.getLoggedInUserId();
				$scope.comment.comment = content;
				$scope.comment.trendMappingId = trendMappingId;
				
				CreateTrendService.data1($scope.comment);
				$scope.initFirst();
				$("#ViewCommnetResponse_"+trendMappingId).text("Hide All Comments");
				$scope.commentsToshow = trendMappingId;
				}else{
					$scope.loginNow();
				}

			};

			$scope.viewOpinion = function(opinionId){
				$location.url('/viewOpinion?id='+opinionId);
			};
			$scope.viewResponse = function(opinionId,responseId){
				$location.url('/viewOpinion?id='+opinionId+'&responseId='+responseId);
			};
			$scope.viewPersonality = function(userId){
				$location.url('/personality?id='+userId);
			};
			
			$scope.showHideComments = function(id,option){
				if(option == "view"){
					$scope.commentsToshow = id;
				}
				else{
					$scope.commentsToshow = "";
				}
			}
			
			
			$scope.selectedItem ="";
			$scope.searchText = "";
	     
			$scope.getMatchingTrends = function (id) {
				 return $http.get('/trend/matchingTrends?name='+id).then(function(response) {
						if (response.data.messages != null) {
							//$scope.trendNames = response.data.messages[0];
							return response.data.messages[0];
						}
						
				 });
   	   
			};
/// Added by Manoj Gupta for searching trend
			
			
			$scope.filterTrend=function(){
				

				$http.get('/trend/matchingTrends?name='+$scope.keyword).then(function(response) {
					if (response.data.messages != null) {
						//$('#searchResult').
						
					}
				});
			};
			
			///
			$scope.showEditDelete ="";
			if($scope.isUserLoggedIn() == true){
				$scope.loggedInUser = $scope.getLoggedInUserId();
			    $scope.showEditDelete = $scope.loggedInUser; 
			}											
		
			
			$scope.commentEditable = "";
			
			$scope.enableEditComment = function(id,ownerId) {
				
				if($scope.isUserLoggedIn() == true){ 
					if(ownerId == $scope.getLoggedInUserId()){
						$scope.commentEditable = "comment_"+id;
						$scope.showEditDelete ="";
					}
				}
				else{
					$scope.loginNow();
				}
				
    		  };
    		  $scope.disableEditComment = function(id) {
    			  if($scope.isUserLoggedIn() == true){
    					$scope.loggedInUser = $scope.getLoggedInUserId();
    				    $scope.showEditDelete = $scope.loggedInUser; 
    				}
      		    $scope.commentEditable = "";
      		  };
      		  
    		  
    		  $scope.saveEditComment = function(id,type) {
   				if($scope.isUserLoggedIn() == true){
 				 var commentText = $("#comment_"+id).val();
 				 $http.post('/comment/update?id='+id+
 						 '&comment='+commentText+"&type=trend").then(
								function(response){	
								if(response.data.messages[0] != null);{
									var updatedCommentText = response.data.messages[0].comment;
									if(type =="opinion"){
										// opinion mappings
										$http.get('/trend/getOpinionsByTrendId?trendId='+$scope.id+'&count=3').then(function(response) {
											if (response.data.messages != null) {
												$scope.trendMappingsOpinion = response.data.messages[0];
												for(var i=0;i<$scope.trendMappingsOpinion.length ;i++){
													//trendMappings[i].text=$sce.trustAsHtml(trendMappings[i].text);
													$http.get('/comment/getAllCommentsForComponentId?componentType=trend&componentId='+$scope.trendMappingsOpinion[i].trendmappingId).then(function(response) {
														if (response.data.messages != null) {
															if(response.data.messages.length > 0){
																$scope.trendMappingOpinionComments[(response.data.messages[0]).trendMappingId] =   response.data.messages;
															}
														}
													});
												}
											}
										});
									}else{
										//respons mappings
										$http.get('/trend/getResponsesByTrendId?trendId='+$scope.id+'&count=3').then(function(response) {
											if (response.data.messages != null) {
												$scope.trendMappingsResponses = response.data.messages[0];
												for(var i=0;i<$scope.trendMappingsResponses.length ;i++){
													//trendMappings[i].text=$sce.trustAsHtml(trendMappings[i].text);
													$http.get('/comment/getAllCommentsForComponentId?componentType=trend&componentId='+$scope.trendMappingsResponses[i].trendmappingId).then(function(response) {
														if (response.data.messages != null) {
															if(response.data.messages.length > 0){
																$scope.trendMappingResponseComments[(response.data.messages[0]).trendMappingId] =   response.data.messages;	
															}
														}
													});
												}
											}
										});
										
									}
								}
							});
 				 $scope.commentEditable = "";
 				if($scope.isUserLoggedIn() == true){
 					$scope.loggedInUser = $scope.getLoggedInUserId();
 				    $scope.showEditDelete = $scope.loggedInUser; 
 				}
  			}else{
				$scope.loginNow();
			}

    		  };
    		  

    		  $rootScope.$on("CallParentDeleteComment", function(evt,id){
	    		  $scope.deleteResponse(id[0],id[1]);
	           });
    		  
    		  $scope.deleteResponse = function(id,type) {								    		
		    		 $('#modal_delete_comment').modal("hide");
		    		 $http.post('/comment/delete?componentType=trend&id='+id).then(
								function(response){	
								if(response.data.success){
								//	$location.path("/viewAllOpinion");
									if(type == "Opinion"){

										// opinion mappings
										$http.get('/trend/getOpinionsByTrendId?trendId='+$scope.id+'&count=3').then(function(response) {
											if (response.data.messages != null) {
												$scope.trendMappingsOpinion = response.data.messages[0];
												for(var i=0;i<$scope.trendMappingsOpinion.length ;i++){
													//trendMappings[i].text=$sce.trustAsHtml(trendMappings[i].text);
													$http.get('/comment/getAllCommentsForComponentId?componentType=trend&componentId='+$scope.trendMappingsOpinion[i].trendmappingId).then(function(response) {
														if (response.data.messages != null) {
															if(response.data.messages.length > 0){
																$scope.trendMappingOpinionComments[(response.data.messages[0]).trendMappingId] =   response.data.messages;
															}
														}
													});
												}
											}
										});
									
									}else{

										//response mappings
										$http.get('/trend/getResponsesByTrendId?trendId='+$scope.id+'&count=3').then(function(response) {
											if (response.data.messages != null) {
												$scope.trendMappingsResponses = response.data.messages[0];
												for(var i=0;i<$scope.trendMappingsResponses.length ;i++){
													//trendMappings[i].text=$sce.trustAsHtml(trendMappings[i].text);
													$http.get('/comment/getAllCommentsForComponentId?componentType=trend&componentId='+$scope.trendMappingsResponses[i].trendmappingId).then(function(response) {
														if (response.data.messages != null) {
															if(response.data.messages.length > 0){
																$scope.trendMappingResponseComments[(response.data.messages[0]).trendMappingId] =   response.data.messages;	
															}
														}
													});
												}
											}
										});
										
									
									}
								}
							});
		    		 

		    	 };	
		    	 
		    	 //added by manoj gupta for trend search
		    	 
		    	 $scope.searchclick=function(){
		    		
		    		 $("#hola").show();
		    		 $http.get('/trend/getTrendById?id='+$scope.selectedTrends.originalObject.id).then(function(response) {
							if (response.data.messages != null) {
								$scope.trend = response.data.messages[0];
								}
						});
		    			$http.get('/trend/getOpinionsByTrendId?trendId='+$scope.selectedTrends.originalObject.id+'&count=3').then(function(response) {
							if (response.data.messages != null) {
								 $("#hola").hide();
								$scope.trendMappingsOpinion = response.data.messages[0];
								for(var i=0;i<$scope.trendMappingsOpinion.length ;i++){
									//trendMappings[i].text=$sce.trustAsHtml(trendMappings[i].text);
									$http.get('/comment/getAllCommentsForComponentId?componentType=trend&componentId='+$scope.trendMappingsOpinion[i].trendmappingId).then(function(response) {
										if (response.data.messages != null) {
											if(response.data.messages.length > 0){
												$scope.trendMappingOpinionComments[(response.data.messages[0]).trendMappingId] =   response.data.messages;
											}
										}
									});
								}
							}
							else{
								 $("#hola").hide();
							}
						});
		    		 
		    			
		    		
		    	 };
		    	 
		    	 
		    	 ///added by manoj trends for usercount
		    	 $scope.divhide=function(){
		    		 $('.popup_smiley').hide();
		    	 }
		    	 $scope.userTrends=function(id,classid)
		    	 {
		    		
		    		 $('#hola').show();
		    		// $('.popup_smiley').hide();
		    		 $('#popupSmilee_'+classid).hide();
		    			$http.get('/trend/getAllTrendsByUser?userId='+id).then(function(response) {
							if (response.data.messages != null) {
								if(response.data.messages.length > 0){
									$scope.userTrendArray=response.data.messages[0];
									 $('#hola').hide();
									$('.popup_smiley').show();
									$('#popupSmilee_'+classid).show();
								}
							}
						});
		    	 }

		    	 $('#hola').hide();
		}]);
