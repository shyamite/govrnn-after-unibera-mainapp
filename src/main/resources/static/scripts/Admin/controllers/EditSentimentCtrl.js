'use strict';

var app = angular.module("govrnnadminApp");
window.govrnnApp=app;
app.controller("EditCtrl", function ($scope, $http,$location) {
	$scope.categories = [
	    {id: 1, name: 'Agriculture & allied activities',checked:false},
	    {id: 2, name: 'Labour & Employment',checked:false},
	    {id: 3, name: 'Commerce & Trade',checked:false},
	    {id: 105, name: 'FDI',checked:false},
	    {id: 5, name: 'Taxation',checked:false},
	    {id: 6, name: 'Banking',checked:false},
	    {id: 104, name: 'Inflation',checked:false}
	    
	  ];
	$scope.selectedCategories=[];
	$scope.findByType=function(){
		$scope.sentimentData=[];
			$('#hola').show();
			$http({
	            url: '/admin/sentiment/getAllSentiments?sentimentType='+$scope.sentiment.type
	            ,
	            method: "GET",
	            headers: {'Content-Type': 'application/json'}
	        }).then(function(res){
	        	
	        	$scope.sentimentData=res.data.messages;
	        		
	        	$('#hola').hide();
	        	
	        },function(err){
	        	console.log('err',err);
	        	$('#hola').hide();
	        })
	};
	$scope.clickRelatedSentiment=function(){
		angular.forEach($scope.sentiment.relatedSentiments,function(i,j){
			
			$scope.addedSentiment.push(i.id)
			$('#sentbtn_'+i.id).val('Remove');
	
	})
	}
	$scope.editSentimentInIT=function(){
		$('#hola').show();
		var id=$location.search().sentimentId;
		 $http({
            url: '/sentiment/findSentiment?id='+id,
            method: "GET",
            
            headers: {'Content-Type': 'application/json'}
        }).then(function(res){
        	$('#hola').hide();
        	
        	if(res.data.messages!=null){
        		$scope.sentiment=res.data.messages[0];
        		if($scope.sentiment.keywords==undefined)
        			$scope.sentiment.keywords="";
        		if($scope.sentiment.categories==undefined)
        		$scope.sentiment.categories=[];
        		$scope.questionList=$scope.sentiment.polls;
        		$scope.linkList=$scope.sentiment.references;
        		$scope.findByType();
        		
        		
        		angular.forEach($scope.sentiment.categoryOutputModels,function(a,b){
        			angular.forEach($scope.categories,function(e,f){
        				if(a.name==e.name)
        					{
        					$scope.categories[f].checked=true;
        					}
        			})
        		})
        			
        	}
        	else{
        	alert("no data found.");
        	}
        	
        },function(err){
        	alert("Failed:unable to fetch data");
        })
   };
	$scope.editSentimentInIT();
	$scope.convertMainFile=function(ele) {
		
	var f=document.getElementById(ele.id).files[0],
	  reader = new FileReader();
	  reader.onloadend = function(e) {
		  var image = e.target.result;
			var arr = image.split(",");
			var arr1 = arr[0].split(";");
			var arr2 = arr1[0].split(":"); 
			$scope.sentiment.image=arr[1];
			$scope.sentiment.imageFileType=arr2[1];
			$scope.$apply();
	  }
	  reader.readAsDataURL(f);
}
	
	$scope.convertFile=function(ele) {
		var f=document.getElementById(ele.id).files[0];
		  var reader = new FileReader();
		  reader.onloadend = function(e) {
			  var image = e.target.result;
				var arr = image.split(",");
				var arr1 = arr[0].split(";");
				var arr2 = arr1[0].split(":"); 
				 $scope.image=image;
				 $scope.questionList[ele.id.slice(6,7)].image=arr[1];
				 $scope.imageType=arr2[1];
				$scope.$apply();
		  }
		  reader.readAsDataURL(f);
		};
	
	
	$scope.updateSentiment = function() {
		$('#hola').show();
		$scope.relatedSents=[];
		angular.forEach($scope.addedSentiment,function(a,b){
			var o={"id":a}
			$scope.relatedSents.push(o);
		})
		angular.forEach($scope.categories,function(a,b){
			if($scope.categories[b].checked==true){
				var o={"id":$scope.categories[b].id,"name":$scope.categories[b].name}
				$scope.selectedCategories.push(o);
			}
			});
			$scope.submitToUpdate({'id':$scope.sentiment.id,'imageByte':$scope.sentiment.image,'subject':$scope.sentiment.subject,"description": $scope.sentiment.description,
				"keywords": $scope.sentiment.keywords,"imageType":$scope.sentiment.imageFileType,"type": $scope.sentiment.type, "categories":$scope.sentiment.categories,"relatedSentiments":$scope.relatedSents
				});
			
		};
	$scope.submitToUpdate=function(data){
		 $http({
            url: '/admin/sentiment/edit',
            method: "PUT",
            data: data,
            headers: {'Content-Type': 'application/json'}
        }).then(function(res){
        	
        	$('#hola').hide();
        	alert("Updated Sucessfully.");
        	
        	console.log(res);
        },function(err){
        	
        	$('#hola').hide();
        	alert("Update Failed.");
        })
   };
	
	$scope.questionList=[];
	$scope.linkList=[];
	$scope.addedSentiment=[];
	var linkBase={id:"",title:"",link:""};
	
	var questionBase={id:"",question:"",options:[{pollOption:""},{pollOption:""},{pollOption:""}],image:""};
	$scope.addQuestion=function(){
		questionBase.id=$scope.sentiment.id;
		$scope.questionList.push(angular.copy(questionBase));
	}
	$scope.addLinks=function(){
		linkBase.id=$scope.sentiment.id;
		$scope.linkList.push(angular.copy(linkBase));
	}
	$scope.removeLinks=function(){
		if($scope.linkList.length>1)
		$scope.linkList.length=$scope.linkList.length-1;
	}
	$scope.removeQuestion=function(){
		if($scope.questionList.length>1)
		$scope.questionList.length=$scope.questionList.length-1;
	}
	$scope.submitPoll=function(){
		var Questiondata = angular.toJson($scope.questionList);
		var Linkdata = angular.toJson($scope.linkList);
		var quesLinkObj={
				"polls":[Questiondata],
				"links":[Linkdata]
				
		};
		addPollLink(quesLinkObj);
	}
	function init(){
		$scope.questionList.push(angular.copy(questionBase));
		$scope.linkList.push(angular.copy(linkBase));
	}
	
	function addPollLink(questionLink){
		
		var data = angular.toJson(questionLink);
		var questionList=JSON.parse(data);
		 $http({
	            url: '/admin/sentiment/editPoll',
	            method: "PUT",
	            data: questionList,
	            headers: {'Content-Type': 'application/json'}
       }).then(function(res){
       	console.log(res)
       },function(err){
       	console.log('err',err);
       })
  }
$scope.SelectedSentiment=function(id){
		if($scope.addedSentiment.indexOf(id) == -1) {
			$scope.addedSentiment.push(id);
			$('#sentbtn_'+id).val('Remove');
			}
		else{
			$scope.addedSentiment.pop(id);
			$('#sentbtn_'+id).val('Add');
	}	
	};
});
