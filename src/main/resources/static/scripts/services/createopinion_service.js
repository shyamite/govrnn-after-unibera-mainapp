﻿'use strict';

/**
 * @ngdoc service
 * @name govrnnApp.myService
 * @description
 * # myService
 * Service in the govrnnApp.
 */
angular.module('govrnnApp')
  .service('CreateOpinionService',['$http','$rootScope', '$route',function ($http,$rootScope, $route) {
	  
		var opinion = {};

		this.opinion = {
			userId : [],
			links : []
		};

		this.saveOpinion = function(opinion, file) {
			$http(
					{
						method : 'POST',
						url : '/opinion/addOpinion',
						headers : {
							'Content-Type' : undefined
						},
						transformRequest : function() {
							var formData = new FormData();

							formData.append('opinion', new Blob([ angular
									.toJson(opinion) ], {
								type : "application/json"
							}));
							formData.append("file", file);
							return formData;
						},
						data : {
							opinion : opinion,
							file : file
						}
					}).success(function(data, status, headers, config) {
						if(data.success){							
						alert("Successfully created opinion");	
						$window.location.href = '/#/sentiment/:sentimentId/';
							
						}
			

			})
		};
	/*........................	code to submit response..................................*/
		 this.data1 = function(responseObject) {
				console.log("inside response service");
				console.log("inside "+responseObject);
				
			$http.post('/response/add/', JSON.stringify(responseObject)).then(
						function(resp){
							//alert("json inside service"+JSON.stringify(responseObject));
							//alert("successfully submit response");
							$route.reload();
						});
			}

		/* .......................to submit response..............................*/
			
		/*	this.data1 = function(response1) {
				console.log("response at service"
						+JSOG.stringify(response1)); 
				console.log("inside response service");
	          
	           $http({
	               method: 'POST',
	              // url: $rootScope.liveHost+"/response/add",
	                 url : '/response/add',
	                 headers: {'Content-Type': 'application/json'},
	               data:{
	               response2:response1
	                   }
	                 }).then(function successCallback(response) {
	                  if(response.data.success === true){  
	      alert('response created');
	     console.log("success: created new response");
	     }
	     else
	     {
	       alert('unable to create response');
	       console.log("succes : unable to create response"); 
	     }
	     }, function errorCallback(response) {
	     console.log("unable to create response");
	             });
			}*/
	
			
}]);