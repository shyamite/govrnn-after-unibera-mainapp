 'use strict';

/**
 * @ngdoc overview
 * @name govrnnApp
 * @description
 * # govrnnApp
 *
 * Main module of the application.
 */
angular
  .module('govrnnApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
	'ngStorage',
	'zingchart-angularjs',
	'angucomplete'
  ])

  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl',
        controllerAs: 'home'
      })
      .when('/viewAllOpinionNotifications', {
        templateUrl: 'views/viewAllNotifications.html',
        controller: 'opinionNotificationController',
        controllerAs: 'vm'
      })
      .when('/viewGeneralNotification', {
        templateUrl: 'views/viewGeneralNotification.html',
        controller: 'generalNotificationController',
        controllerAs: 'vm'
      })
      .when('/viewAllOpinion', {
        templateUrl: 'views/viewAllOpinion.html',
        controller: 'ViewAllOpinionCtrl',
        controllerAs: 'viewallopinion'
      })
      .when('/viewOpinion', {
        templateUrl: 'views/viewOpinion.html',
        controller: 'ViewOpinionCtrl',
        controllerAs: 'viewopinion'
      })
      .when('/trends', {
        templateUrl: 'views/trends.html',
        controller: 'TrendsCtrl',
        controllerAs: 'trends'
      })
      .when('/userSetting/id=:userId', {
        templateUrl: 'views/user-setting.html',
        controller:   'UserSettingCtrl',
        controllerAs: 'usersetting'
      })
      .when('/?sentimentId=:sentimentId', {
        templateUrl: 'views/home.html',
        controller:   'HomeCtrl',
        controllerAs: 'home'
      })
      .when('/personality', {
        templateUrl: 'views/personality.html',
        controller: 'PersonalityCtrl',
        controllerAs: 'personality'
      })
      .when('/searchuser', {
        templateUrl: 'views/search_user.html',
        controller: 'SearchUserCtrl',
        controllerAs: 'searchuser'
      })
      .when('/userConnection', {
          templateUrl: 'views/userConnection.html',
          controller: 'UserConnectionCtrl',
          controllerAs: 'userConnection'
        })
      .when('/userAgreement', {
          templateUrl: 'views/userAgreement.html'
        })
      .when('/privacyPolicy', {
          templateUrl: 'views/privacyPolicy.html'
        })
      .when('/communityGuidelines', {
          templateUrl: 'views/communityGuidelines.html'
        })
      .when('/cookiePolicy', {
          templateUrl: 'views/cookiePolicy.html'
        })
      .when('/copyrightPolicy', {
          templateUrl: 'views/copyrightPolicy.html'
        })
      .when('/aboutUs', {
          templateUrl: 'views/about.html',
		  controller: 'AboutCtrl',
		  controllerAs: 'about'
        })
      .when('/viewAllsearchuser', {
			templateUrl: 'views/viewAllsearch_user.html',
			controller: 'ViewAllSearchUserCtrl',
			controllerAs: 'viewAllsearchuser'
      })
      .when('/admin/uploadFile', {
        templateUrl: 'views/uploadFile.html',
        controller: 'UploadFileCtrl',
        controllerAs: 'fileUploader'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  
  .run(function ($rootScope) {
  $rootScope.liveHost = 'http://139.59.69.162:8080';
 //   $rootScope.liveHost = 'http://localhost:8080';
})
.config(['$locationProvider', function($locationProvider) {
  $locationProvider.hashPrefix('');
}]);
