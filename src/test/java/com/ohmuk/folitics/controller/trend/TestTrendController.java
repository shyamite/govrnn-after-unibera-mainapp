package com.ohmuk.folitics.controller.trend;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.hibernate.entity.trend.Trend;
import com.ohmuk.folitics.hibernate.entity.trend.TrendMapping;
import com.ohmuk.folitics.ouput.model.TrendOutputModel;
import com.ohmuk.folitics.utils.TestUtils;

/**
 * @author soumya
 *
 */
@RunWith(BlockJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestTrendController {
	// Test RestTemplate to invoke the APIs.
	protected TestRestTemplate restTemplate = new TestRestTemplate();

	@Test
	public void TestController() throws JsonProcessingException, IOException {

		
		Trend trend = testCreateTrendApi("# Trend "+Math.random());
		testCreateTrendMappingApi(trend);
		testMatchTrendApi("Trend");
		//testdeleteTrendApi(trend.getId());
		
	}

	protected Trend testCreateTrendApi(String name) throws JsonProcessingException, IllegalArgumentException {

		Trend trend = new Trend();
		trend.setName(name);
		trend.setCreatedBy("admin");
		
		HttpEntity<String> httpEntity = TestUtils.getHttpEntity(new ObjectMapper().convertValue(trend, Map.class));

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.TREND_CONTROLLER_APIS.ADD_TREND);
		HttpEntity<ResponseDto<Trend>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.POST, httpEntity, new ParameterizedTypeReference<ResponseDto<Trend>>() {
				});
		assertNotNull(response);
		return response.getBody().getMessages().get(0);
	}

	protected void testCreateTrendMappingApi(Trend trend) throws JsonProcessingException, IllegalArgumentException {

		TrendMapping mapping = new TrendMapping();		
		mapping.setComponentId(1l);
		mapping.setComponentType("Opinion");
		mapping.setTrend(trend);
		Map<String,Object> trendMapping = new HashMap<>();
		trendMapping.put("componentId", 1l);
		trendMapping.put("componentType", "opinion");
		trendMapping.put("userId", 1l);
		trendMapping.put("trend", trend);
		
		HttpEntity<String> httpEntity = TestUtils.getHttpEntity(trendMapping);

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.TREND_CONTROLLER_APIS.ADD_TREND_MAPPING);
		HttpEntity<ResponseDto<Long>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.POST, httpEntity, new ParameterizedTypeReference<ResponseDto<Long>>() {
				});
		assertNotNull(response);
	}

	protected void testMatchTrendApi(String name) throws JsonProcessingException, IllegalArgumentException {

		HttpEntity httpEntity = TestUtils.getHttpEntityHttpHeaders();

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.TREND_CONTROLLER_APIS.GET_MATCHING_TRENDS).queryParam("name",name);
		HttpEntity<ResponseDto<Object>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.GET, httpEntity, new ParameterizedTypeReference<ResponseDto<Object>>() {
				});
		assertNotNull(response);
	}


	protected void testdeleteTrendApi(Long id) throws JsonProcessingException, IllegalArgumentException {

		HttpEntity httpEntity = TestUtils.getHttpEntityHttpHeaders();

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.TREND_CONTROLLER_APIS.DELETE_TREND).queryParam("id", id);
		HttpEntity<ResponseDto<String>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.POST, httpEntity, new ParameterizedTypeReference<ResponseDto<String>>() {
				});
		assertNotNull(response);
	}

}
